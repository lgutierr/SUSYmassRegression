# Kinematic event generator

Package containing a basic model independent generator for generation of large stats.

## Software

### Directly running ROOT macro
```
root -q 'generator.cxx+("process name", [options])'
```
See below for list of allowed process names.

### Shell script with various generation requests
```
./generateProcesses.sh
```

##### Options:

```nEvents```
* Number of events to be generated
* Default: 1e5

```maxResonanceM```
* Maximum resonance mass to be considered in GeV
* The total available energy is 13 TeV
* Default: 3 TeV

```fileType```: 
* Specifies if it is a combined train and test file, a specific one, or both.
* Allowed values: "", "train", "test", "both"
* Default: "both"

```visObjSortRule```
* Used to sort visible final state objects by a given variable.
* Allowed values: "truth", "pT"
* Default: "pT"

```outputSortRule``` 
* Used to sort outputs. 
* Added to avoid input degeneracy with 2D or higher mass splitting samples with symmetric decay cascades.
* Allowed values: "", "mag"
* Default: ""

```inputsType```
* Trigger the calculation of various sets of variables.
* Allowed values: 0,
* Default: 0
* 0: [pT, eta, phi, px, py, pz, E] of visible objects and MET, and event level [sphericity, transverse sphericity, aplanarity, momentum tensor eigenvalues].

```genType```
* Generation mode
* Allowed values: "randGen"
* Default: "randGen"


## Allowed processes

#### Process name: getKine_GG

* Pair production of gluinos (pp > GG)
* Each gluino decays to a pair of quarks and the LSP neutralino (G > N1 + q q)
* In the generation, A and B refers to the source gluino.

#### Process name: getKine_GG_XX_had

* Pair production of gluinos (pp > GG)
* Each gluino decays to a pair of quarks and a chargino (G > X + q q)
* Each chargino decays to a pair of quarks and the LSP neutralino (X > N1 + q q)
* In the generation, A and B refers to the source gluino.

#### Process name: getKine_GG_XX_semiLep

* Pair production of gluinos (pp > GG)
* Each gluino decays to a pair of quarks and a chargino (G > X + q q)
* One chargino decays leptonically to the LSP neutralino, lepton and neutrino (X > N1 + l v) and the other hadronically to the LSP neutralino and a pair of quarks (X > N1 + q q)
* In the generation, A and B refers to the source gluino.

#### Process name: getKine_GG_XX_WW_had

* Pair production of gluinos (pp > GG)
* Each gluino decays to a pair of quarks and a chargino (G > X + q q)
* Each chargino decays to a W boson and the LSP neutralino (X > N1 + W)
* Each W boson decays hadronically (W > q q)
* In the generation, A and B refers to the source gluino.

#### Process name: getKine_GG_XX_WW_semiLep

* Pair production of gluinos (pp > GG)
* Each gluino decays to a pair of quarks and a chargino (G > X + q q)
* Each chargino decays to a W boson and the LSP neutralino (X > N1 + W)
* One W boson decays hadronically and the other leptonically (W > q q and W > l v)
* In the generation, A and B refers to the source gluino.

## WARNINGs

* To avoid generation code confusion, do not delete the ```.log``` file which keeps track of every generation requested

## Development notes:

* Event generation only cares about conservation of:
  * 4-momentum
  * Lorentz invariance
* Currently only generating process objects.

* Improvement tasks:
  * Expand to genTypes such us equalGen, calibGen, calibRandGen. (Clean up old version code)
  * Pile-up
  * ISR and FSR jets
