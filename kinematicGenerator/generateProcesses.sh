# Gluino DD processes
root -q 'generator.cxx+("getKine_GG", nEvents=1e6)'
# Gluino CD1 with W bosons fully-hadronic
root -q 'generator.cxx+("getKine_GG_XX_WW_had", nEvents=1e6)'
# Gluino CD1 with W bosons semi-leptonic
root -q 'generator.cxx+("getKine_GG_XX_WW_semiLep", nEvents=1e6)'
