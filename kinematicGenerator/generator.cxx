#include "TFile.h"
#include "TTree.h"
#include "TRandom3.h"
#include "TMatrixD.h"
#include "TMatrixDEigen.h"

#include "myGenerator.h"

#include <iostream>
#include <string.h>
#include <string>
#include <sstream>
#include <fstream>
#include <ctime>
#include <algorithm>
#include <random>
#include <initializer_list>
#include <filesystem>
  
using namespace std;

///////////////////////////////////////////////////////
//
//  Process
//
///////////////////////////////////////////////////////

string process = "";

// Check process is allowed
bool checkProcess()
{
  cout << "----- Process -----" << endl;
  bool cond1 = process != "getKine_GG";
  bool cond2 = process != "getKine_GG_XX_had" && process != "getKine_GG_XX_semiLep";
  bool cond3 = process != "getKine_GG_XX_WW_had" && process != "getKine_GG_XX_WW_semiLep";
  bool cond = cond1 && cond2 && cond3;
  if (cond){
    cout << "Desired process=\"" << process << "\" not allowed..." << endl;
    cout << "process accepted values: \"getKine_GG\", \"getKine_GG_XX_had\", \"getKine_GG_XX_semiLep\", \"getKine_GG_XX_WW_had\", or \"getKine_GG_XX_WW_semiLep\"" << endl;
    cout << "Exiting program without generation..." << endl;
    return false;
  }
  else{
    cout << "Generate: " << process << endl;
    return true;
  }
}


///////////////////////////////////////////////////////
//
//  Options 
//
///////////////////////////////////////////////////////

int nEvents = 1e5;
float maxResonanceM = 3000;
string fileType = "both";
string visObjSortRule = "pT";
string outputSortRule = "";
int inputsType = 0;
string genType = "randGen";

void readOptions() 
{
  cout << "----- Event generation options -----" << endl;
  cout << "nEvents: " << nEvents << endl;
  cout << "maxResonanceM: " << maxResonanceM << endl;
  cout << "fileType: " << fileType << endl;
  cout << "visObjSortRule: " << visObjSortRule << endl;
  cout << "outputSortRule: " << outputSortRule << endl;
  cout << "inputsType: " << inputsType << endl;
  cout << "genType: " << genType << endl;
}

bool checkOptions()
{
  cout << "----- Event generation options check -----" << endl;
  bool accepted = true;

  // Check maximum resonance mass
  if (maxResonanceM > 6500){
    cout << "There might not be enough energy to generate resonance mass..." << endl;
  }
  // Check file type
  if (fileType != "" && fileType != "train" && fileType != "test" && fileType != "both"){
    cout << "Value of argument fileType not accepted..." << endl;
    cout << "fileType accepted values: \"\", \"train\", \"test\", or (default) \"both\"" << endl;
    accepted = false;
  }
  // Check visible objects sorting rule
  if (visObjSortRule != "truth" && visObjSortRule != "pT"){
    cout << "Value of argument visObjSortRule not accepted..." << endl;
    cout << "visObjSortRule accepted values: \"truth\", or (default) \"pT\"" << endl;
    accepted = false;
  }  
  
  // Check outputs sorting rule
  if (outputSortRule != "" && outputSortRule != "mag"){
    cout << "Value of argument outputSortRule not accepted..." << endl;
    cout << "outputSortRule accepted values: \"mag\", or (default) \"\"" << endl;
    accepted = false;
  } 
  
  // Check inputs type
  if (inputsType != 0){
    cout << "Value of argument inputsType not accepted..." << endl;
    cout << "inputsType accepted values: 0" << endl;
    accepted = false;
  }
  
  // Check gen. type
  if (genType != "randGen"){
    cout << "Value of argument genType not accepted..." << endl;
    cout << "genType accepted values: (default) \"randGen\"" << endl;
    accepted = false;
  }

  // Acceptable value
  if (accepted) cout << "All options are OK... Proceeding with next steps" << endl;
  else{ 
    cout << "Critical options not acceptable... " << endl;
    cout << "Exiting program without generation..." << endl;
  }
  return accepted;

}

void resetOptions()
{
  nEvents = 1e5;
  maxResonanceM = 3000;
  fileType = "both";
  visObjSortRule = "pT";
  outputSortRule = "";
  inputsType = 0;
  genType = "randGen";
}


///////////////////////////////////////////////////////
//
// Log
//
///////////////////////////////////////////////////////

void startLog(){
  cout << "----- Start log file -----" << endl;

  // If log file does not exist, create one
  ifstream f("./generator.log");
  if (f.good() ){ 
    f.close(); 
    cout << "Using existing log file..." << endl;
  }
  else{
    fstream newF;
    newF.open("./generator.log", ios::out);
    newF.close();
    cout << "Log file does not exist. Creating new log file..." << endl;
  }
}

pair<int, bool> searchLog(){

  cout << "----- Searching log file -----" << endl;

  ifstream f("./generator.log");
  string line;
  int genCode = -1;
  bool processMatch = false;
  bool nEventsMatch = false;
  bool maxResonanceMMatch = false;
  bool fileTypeMatch = false;
  bool visObjSortRuleMatch = false;
  bool outputSortRuleMatch = false;
  bool inputsTypeMatch = false;
  bool genTypeMatch = false;
  bool match = false;
  // Check existing entries
  while (getline(f, line)){
    // Read line
    string code;
    string value;
    replace(line.begin(), line.end(), ':', ' ');
    stringstream ss(line);
    ss >> code;
    ss >> value;
    // Check genCode
    if (code == "genCode"){
      // First entry
      if (genCode == -1) genCode = stoi(value);
      // Following entries
      else {
        // Check if previous genCode matches options
        bool match1 = processMatch && nEventsMatch && maxResonanceMMatch;
        bool match2 = fileTypeMatch && visObjSortRuleMatch && outputSortRuleMatch;
        bool match3 = inputsTypeMatch && genTypeMatch;
        match = match1 && match2 && match3;
        if (match) break;
        // If previous genCode does not match options, update genCode and reset bools
        else{
          genCode = stoi(value);
          processMatch = false;
          nEventsMatch = false;
          maxResonanceMMatch = false;
          fileTypeMatch = false;
          visObjSortRuleMatch = false;
          outputSortRuleMatch = false;
          inputsTypeMatch = false;
          genTypeMatch = false;
          match = false;
        }
      }
    }
    // Check process
    else if (code == "process"){
      if (process == value) processMatch=true;
    }
    // Check nEvents
    else if (code == "nEvents"){ 
      if (nEvents == stoi(value)) nEventsMatch=true;
    }
    // Check maxResonanceM
    else if (code == "maxResonanceM"){
      if (maxResonanceM == stoi(value)) maxResonanceMMatch=true;
    }
    // Check fileType
    else if (code == "fileType"){ 
      if (fileType == value) fileTypeMatch=true;
    }
    // Check visObjSortRule
    else if (code == "visObjSortRule"){ 
      if (visObjSortRule == value) visObjSortRuleMatch=true;
    }
    // Check outputSortRule
    else if (code == "outputSortRule"){ 
      if (outputSortRule == value) outputSortRuleMatch=true;
    }
    // Check inputsType
    else if (code == "inputsType"){ 
      if (inputsType == stoi(value)) inputsTypeMatch=true;
    }
    // Check genType
    else if (code == "genType"){ 
      if (genType == value) genTypeMatch=true;
    }
  }
  // Update match in case it is the last entry
  bool match1 = processMatch && nEventsMatch && maxResonanceMMatch;
  bool match2 = fileTypeMatch && visObjSortRuleMatch && outputSortRuleMatch;
  bool match3 = inputsTypeMatch && genTypeMatch;
  match = match1 && match2 && match3;
  // Check the matching genCode, otherwise create a new one
  if (genCode == -1){
    // Create new entry
    genCode = 0;
    match = false;
  }
  else{
    // If it does not match, generate a new genCode
    if (!match) genCode += 1;
  }

  // Logging
  if (match){ 
    cout << "Found generation with exact same conditions..." << endl;
    cout << "Using genCode: " << genCode << endl;
  }
  else{
    cout << "No generation with exact same conditions found..." << endl;
    cout << "New genCode: " << genCode << endl;
  }
  
  // Return output
  pair<int, bool> searchOut;
  searchOut.first = genCode;
  searchOut.second = match;
  return searchOut;
}

void updateLog(int genCode)
{
  cout << "----- Update log file -----" << endl;

  fstream f;
  f.open("./generator.log", ios::app);
  f << "genCode: " << genCode << endl;
  f << "  process: " << process << endl;
  f << "  nEvents: " << nEvents << endl; 
  f << "  fileType: " << fileType << endl;
  f << "  maxResonanceM: " << maxResonanceM << endl; 
  f << "  visObjSortRule: " << visObjSortRule << endl; 
  f << "  outputSortRule: " << outputSortRule << endl; 
  f << "  inputsType: " << inputsType << endl; 
  f << "  genType: " << genType << endl; 
  f.close();

  cout << "Added entry for genCode=" << genCode << endl;
}


///////////////////////////////////////////////////////
//
// Kinematic generation
//
///////////////////////////////////////////////////////

bool comparePt(tlv v1, tlv v2){
  return v1.Pt() > v2.Pt();
}

bool compareOutputs(int dM1, int dM2){
  return dM1 > dM2;
}

bool compareDoubles(double a, double b){
  return a > b;
}

void getKine_GG_XX_had_run(int genCode, string ext){

  // Start file
  fstream txtout;
  stringstream fileName;
  std::filesystem::create_directory("./data");
  fileName << "./data/genCode" << genCode << ext << ".csv";
  txtout.open(fileName.str(), ios::out);
    
  // Define file header
  stringstream header;
  if (inputsType >= 0){
    // Visible objects info
    header << "q1APt,q1AEta,q1APhi,q1AE,q1APx,q1APy,q1APz" << ",";
    header << "q2APt,q2AEta,q2APhi,q2AE,q2APx,q2APy,q2APz" << ",";
    header << "q3APt,q3AEta,q3APhi,q3AE,q3APx,q3APy,q3APz" << ",";
    header << "q4APt,q4AEta,q4APhi,q4AE,q4APx,q4APy,q4APz" << ",";
    header << "q1BPt,q1BEta,q1BPhi,q1BE,q1BPx,q1BPy,q1BPz" << ",";
    header << "q2BPt,q2BEta,q2BPhi,q2BE,q2BPx,q2BPy,q2BPz" << ",";
    header << "q3BPt,q3BEta,q3BPhi,q3BE,q3BPx,q3BPy,q3BPz" << ",";
    header << "q4BPt,q4BEta,q4BPhi,q4BE,q4BPx,q4BPy,q4BPz" << ",";
    header << "metPt,metEta,metPhi,metE,metPx,metPy,metPz" << ",";
    // Arbitrary variables
    header << "alpha,alpha2" << ",";
    // Event shape variables
    header << "S,St,A,mTens1,mTens2,mTens3" << ",";
    // Outputs
    header << "mG,mX1,mN1,dM1_1,dM1_2,dM2_1,dM2_2";
  }
  if (inputsType >= 1){}
  txtout << header.str() << endl;
  
  // Start random generator
  auto rng = default_random_engine {};
  
  // Start final state particle variables
  float q1APt=0;      float q1AEta=0;      float q1APhi=0;      float q1AE=0;      float q1APx=0;      float q1APy=0;      float q1APz=0;
  float q2APt=0;      float q2AEta=0;      float q2APhi=0;      float q2AE=0;      float q2APx=0;      float q2APy=0;      float q2APz=0;
  float q3APt=0;      float q3AEta=0;      float q3APhi=0;      float q3AE=0;      float q3APx=0;      float q3APy=0;      float q3APz=0;
  float q4APt=0;      float q4AEta=0;      float q4APhi=0;      float q4AE=0;      float q4APx=0;      float q4APy=0;      float q4APz=0;
  float q1BPt=0;      float q1BEta=0;      float q1BPhi=0;      float q1BE=0;      float q1BPx=0;      float q1BPy=0;      float q1BPz=0;
  float q2BPt=0;      float q2BEta=0;      float q2BPhi=0;      float q2BE=0;      float q2BPx=0;      float q2BPy=0;      float q2BPz=0;
  float q3BPt=0;      float q3BEta=0;      float q3BPhi=0;      float q3BE=0;      float q3BPx=0;      float q3BPy=0;      float q3BPz=0;
  float q4BPt=0;      float q4BEta=0;      float q4BPhi=0;      float q4BE=0;      float q4BPx=0;      float q4BPy=0;      float q4BPz=0;
  float metPt=0;      float metEta=0;      float metPhi=0;      float metE=0;      float metPx=0;      float metPy=0;      float metPz=0;
  // Start event level variables 
  float alpha=0;       float alpha2=0; 
  float S=0;           float St = 0;         float A=0;            
  float mTens1=0;      float mTens2=0;       float mTens3=0; 
  // Start output variables info
  int mG=0;            int mX1=0;            int mN1=0;
  float dM1_1=0;       float dM1_2=0;
  float dM2_1=0;       float dM2_2=0;
  
  // Event generation loop
  for (int kk=0; kk<nEvents; kk++){

    while (true){
    
      if (genType == "randGen"){
        // Generate a random SUSY mass
        // Gluino mass (mG) from 1GeV to maxResonanceM
        mG = rand()%(int)(maxResonanceM-1)+1;
        mX1 = rand()%(int)(maxResonanceM-2)+1;
        mN1 = rand()%(int)(maxResonanceM-3);
        while (mG > maxResonanceM || mG <= mX1 || mX1 <= mN1){
          mG = rand()%(int)(maxResonanceM-1)+1;
          mX1 = rand()%(int)(maxResonanceM-2)+1;
          mN1 = rand()%(int)(maxResonanceM-3);
        }
      }
  
      // Print event status
      if (kk == 0) cout << "Event " << kk+1 << "/" << nEvents << ": mG=" << mG << "GeV, mX1=" << mX1 << "GeV, mN1=" << mN1 << "GeV" << endl;
      else if ((kk+1)%100000 == 0) cout << "Event " << kk+1 << "/" << nEvents << ": mG=" << mG << "GeV, mX1=" << mX1 << "GeV, mN1=" << mN1 << "GeV" << endl;
      else if ((kk+1)==nEvents) cout << "Event " << kk+1 << "/" << nEvents << ": mG=" << mG << "GeV, mX1=" << mX1 << "GeV, mN1=" << mN1 << "GeV" << endl;
   
      // Start TLorentzVectors 
      tlv pGA(0,0,0,0), pGB(0,0,0,0); 
      tlv pq1A(0,0,0,0), pq1B(0,0,0,0); 
      tlv pq2A(0,0,0,0), pq2B(0,0,0,0); 
      tlv pX1A(0,0,0,0), pX1B(0,0,0,0); 
      tlv pq3A(0,0,0,0), pq3B(0,0,0,0); 
      tlv pq4A(0,0,0,0), pq4B(0,0,0,0); 
      tlv pN1A(0,0,0,0), pN1B(0,0,0,0); 
      
      // Run generator  
      myGenerator::getKine_GG_XX_had(mG, mX1, mN1,
                                     pGA, pq1A, pq2A, pX1A,
                                     pGB, pq1B, pq2B, pX1B,
                                     pq3A, pq4A, pN1A,
                                     pq3B, pq4B, pN1B);

      // Store visible objects and sort them if necessary
      vector<tlv> vObjs = {pq1A, pq2A, pq3A, pq4A, pq1B, pq2B, pq3B, pq4B};
      if (visObjSortRule == "pT") sort(vObjs.begin(), vObjs.end(), comparePt);

      // Update final state visible objects info
      q1APt = vObjs[0].Pt();      q1AEta = vObjs[0].Eta();      q1APhi = vObjs[0].Phi();      q1AE = vObjs[0].E();    
      q1APx = vObjs[0].Px();      q1APy = vObjs[0].Py();        q1APz = vObjs[0].Pz();
      q2APt = vObjs[1].Pt();      q2AEta = vObjs[1].Eta();      q2APhi = vObjs[1].Phi();      q2AE = vObjs[1].E();
      q2APx = vObjs[1].Px();      q2APy = vObjs[1].Py();        q2APz = vObjs[1].Pz();
      q3APt = vObjs[2].Pt();      q3AEta = vObjs[2].Eta();      q3APhi = vObjs[2].Phi();      q3AE = vObjs[2].E();
      q3APx = vObjs[2].Px();      q3APy = vObjs[2].Py();        q3APz = vObjs[2].Pz();
      q4APt = vObjs[3].Pt();      q4AEta = vObjs[3].Eta();      q4APhi = vObjs[3].Phi();      q4AE = vObjs[3].E();    
      q4APx = vObjs[3].Px();      q4APy = vObjs[3].Py();        q4APz = vObjs[3].Pz();
      q1BPt = vObjs[4].Pt();      q1BEta = vObjs[4].Eta();      q1BPhi = vObjs[4].Phi();      q1BE = vObjs[4].E();    
      q1BPx = vObjs[4].Px();      q1BPy = vObjs[4].Py();        q1BPz = vObjs[4].Pz();
      q2BPt = vObjs[5].Pt();      q2BEta = vObjs[5].Eta();      q2BPhi = vObjs[5].Phi();      q2BE = vObjs[5].E();
      q2BPx = vObjs[5].Px();      q2BPy = vObjs[5].Py();        q2BPz = vObjs[5].Pz();
      q3BPt = vObjs[6].Pt();      q3BEta = vObjs[6].Eta();      q3BPhi = vObjs[6].Phi();      q3BE = vObjs[6].E();
      q3BPx = vObjs[6].Px();      q3BPy = vObjs[6].Py();        q3BPz = vObjs[6].Pz();
      q4BPt = vObjs[7].Pt();      q4BEta = vObjs[7].Eta();      q4BPhi = vObjs[7].Phi();      q4BE = vObjs[7].E();
      q4BPx = vObjs[7].Px();      q4BPy = vObjs[7].Py();        q4BPz = vObjs[7].Pz();

      // Create MET
      tlv pMis = pN1A+pN1B; pMis.SetXYZT(pMis.Px(), pMis.Py(), 0., 0.);

      // Update MET info
      metPt = pMis.Pt();      metPhi = pMis.Phi();      metE = pMis.E();      metPx = pMis.Px();      metPy = pMis.Py();      metPz = pMis.Pz();

      // Get event shape variables
      // Start by defining general momentum tensor
      TMatrixD matrix(3,3);
      float px2 = pow(q1APx,2)+pow(q2APx,2)+pow(q3APx,2)+pow(q4APx,2)+pow(q1BPx,2)+pow(q2BPx,2)+pow(q3BPx,2)+pow(q4BPx,2);
      float py2 = pow(q1APy,2)+pow(q2APy,2)+pow(q3APy,2)+pow(q4APy,2)+pow(q1BPy,2)+pow(q2BPy,2)+pow(q3BPy,2)+pow(q4BPy,2);
      float pz2 = pow(q1APz,2)+pow(q2APz,2)+pow(q3APz,2)+pow(q4APz,2)+pow(q1BPz,2)+pow(q2BPz,2)+pow(q3BPz,2)+pow(q4BPz,2);
      float pxpy = q1APx*q1APy+q2APx*q2APy+q3APx*q3APy+q4APx*q4APy+q1BPx*q1BPy+q2BPx*q2BPy+q3BPx*q3BPy+q4BPx*q4BPy;
      float pxpz = q1APx*q1APz+q2APx*q2APz+q3APx*q3APz+q4APx*q4APz+q1BPx*q1BPz+q2BPx*q2BPz+q3BPx*q3BPz+q4BPx*q4BPz;
      float pypz = q1APy*q1APz+q2APy*q2APz+q3APy*q3APz+q4APy*q4APz+q1BPy*q1BPz+q2BPy*q2BPz+q3BPy*q3BPz+q4BPy*q4BPz;
      TArrayD data(9);
      data[0] = px2;      data[3] = pxpy;      data[6] = pxpz;
      data[1] = pxpy;     data[4] = py2;       data[7] = pypz;
      data[2] = pxpz;     data[5] = pypz;      data[8] = pz2;
      matrix.SetMatrixArray(data.GetArray());
      // Get eigenvalues
      TMatrixDEigen eigen(matrix);
      TMatrixD diagMatrix = eigen.GetEigenValues();
      double lambda1 = TMatrixDRow(diagMatrix, 0)(0);
      double lambda2 = TMatrixDRow(diagMatrix, 1)(1);
      double lambda3 = TMatrixDRow(diagMatrix, 2)(2);
      vector<double> eigenVals = {lambda1, lambda2, lambda3};
      sort(eigenVals.begin(), eigenVals.end(), compareDoubles);
      // Normalize eigenvalues
      double eigenValNorm = lambda1+lambda2+lambda3;
      for (unsigned int i=0; i<eigenVals.size(); i++){
        eigenVals[i] = eigenVals[i]/eigenValNorm;
      }

      // Update event shape variables
      S = (3.0/2.0)*(eigenVals[1]+eigenVals[2]);
      St = (2.0*eigenVals[1])/(eigenVals[0]+eigenVals[1]);
      A = (3.0/2.0)*(eigenVals[2]);
      mTens1 = lambda1/eigenValNorm;
      mTens2 = lambda2/eigenValNorm;
      mTens3 = lambda3/eigenValNorm;
      
      // Set outputs
      int out1 = mG-mX1;
      int out2 = mX1-mN1;
      vector<int> outputs = {out1, out2};
      if (outputSortRule == "mag") sort(outputs.begin(), outputs.end(), compareOutputs);
      dM1_1 = outputs[0];      dM1_2 = outputs[0];
      dM2_1 = outputs[1];      dM2_2 = outputs[1];

      // Set alpha based on outputs
      alpha = (dM1_1-dM2_1)/(2*maxResonanceM)+0.5;
      if (dM1_1 > dM2_1) { alpha2 = 0; }
      else if (dM1_1 == dM2_1) { alpha2 = 0.5; }
      else {alpha2 = 1;}
    
      // Set combined particles input variables
      if (inputsType >= 1){}
    
      // Added to avoid nans
      bool q1Anans = isnan(q1APt) || isnan(q1AEta) || isnan(q1APhi);
      bool q2Anans = isnan(q2APt) || isnan(q2AEta) || isnan(q2APhi);
      bool q3Anans = isnan(q3APt) || isnan(q3AEta) || isnan(q3APhi);
      bool q4Anans = isnan(q4APt) || isnan(q4AEta) || isnan(q4APhi);
      bool q1Bnans = isnan(q1BPt) || isnan(q1BEta) || isnan(q1BPhi);
      bool q2Bnans = isnan(q2BPt) || isnan(q2BEta) || isnan(q2BPhi);
      bool q3Bnans = isnan(q3BPt) || isnan(q3BEta) || isnan(q3BPhi);
      bool q4Bnans = isnan(q4BPt) || isnan(q4BEta) || isnan(q4BPhi);
      bool metnans = isnan(metPt) || isnan(metEta) || isnan(metPhi);
      bool nans = q1Anans || q2Anans || q3Anans || q4Anans || q1Bnans || q2Bnans || q3Bnans || q4Bnans || metnans;

      if (nans) continue;
      else break;
    } 

    // Fill text file
    if (inputsType >=0){
      txtout << q1APt << "," << q1AEta << "," << q1APhi << "," << q1AE << "," << q1APx << "," << q1APy << "," << q1APz << ",";
      txtout << q2APt << "," << q2AEta << "," << q2APhi << "," << q2AE << "," << q2APx << "," << q2APy << "," << q2APz << ",";
      txtout << q3APt << "," << q3AEta << "," << q3APhi << "," << q3AE << "," << q3APx << "," << q3APy << "," << q3APz << ",";
      txtout << q4APt << "," << q4AEta << "," << q4APhi << "," << q4AE << "," << q4APx << "," << q4APy << "," << q4APz << ",";
      txtout << q1BPt << "," << q1BEta << "," << q1BPhi << "," << q1BE << "," << q1BPx << "," << q1BPy << "," << q1BPz << ",";
      txtout << q2BPt << "," << q2BEta << "," << q2BPhi << "," << q2BE << "," << q2BPx << "," << q2BPy << "," << q2BPz << ",";
      txtout << q3BPt << "," << q3BEta << "," << q3BPhi << "," << q3BE << "," << q3BPx << "," << q3BPy << "," << q3BPz << ",";
      txtout << q4BPt << "," << q4BEta << "," << q4BPhi << "," << q4BE << "," << q4BPx << "," << q4BPy << "," << q4BPz << ",";
      txtout << metPt << "," << metEta << "," << metPhi << "," << metE << "," << metPx << "," << metPy << "," << metPz << ",";
      txtout << alpha << "," << alpha2 << ",";
      txtout << S << "," << St << "," << A << "," << mTens1 << "," << mTens2 << "," << mTens3 << ",";
      txtout << mG << "," << mX1 << "," << mN1 << ",";
      txtout << dM1_1 << "," << dM1_2 << "," << dM2_1 << "," << dM2_2;
    }
    if (inputsType >=1){}
    txtout << endl;
  }

  // Close file
  txtout.close();  
}

void getKine_GG_XX_semiLep_run(int genCode, string ext){

  // Start file
  fstream txtout;
  stringstream fileName;
  std::filesystem::create_directory("./data");
  fileName << "./data/genCode" << genCode << ext << ".csv";
  txtout.open(fileName.str(), ios::out);
    
  // Define file header
  stringstream header;
  if (inputsType >= 0){
    // Visible objects info
    header << "q1APt,q1AEta,q1APhi,q1AE,q1APx,q1APy,q1APz" << ",";
    header << "q2APt,q2AEta,q2APhi,q2AE,q2APx,q2APy,q2APz" << ",";
    header << "q3APt,q3AEta,q3APhi,q3AE,q3APx,q3APy,q3APz" << ",";
    header << "q4APt,q4AEta,q4APhi,q4AE,q4APx,q4APy,q4APz" << ",";
    header << "q1BPt,q1BEta,q1BPhi,q1BE,q1BPx,q1BPy,q1BPz" << ",";
    header << "q2BPt,q2BEta,q2BPhi,q2BE,q2BPx,q2BPy,q2BPz" << ",";
    header << "l1BPt,l1BEta,l1BPhi,l1BE,l1BPx,l1BPy,l1BPz" << ",";
    header << "metPt,metEta,metPhi,metE,metPx,metPy,metPz" << ",";
    // Arbitrary variables
    header << "alpha,alpha2" << ",";
    // Event shape variables
    header << "S,St,A,mTens1,mTens2,mTens3" << ",";
    // Outputs
    header << "mG,mX1,mN1,dM1_1,dM1_2,dM2_1,dM2_2";
  }
  if (inputsType >= 1){}
  txtout << header.str() << endl;
  
  // Start random generator
  auto rng = default_random_engine {};
  
  // Start final state particle variables
  float q1APt=0;      float q1AEta=0;      float q1APhi=0;      float q1AE=0;      float q1APx=0;      float q1APy=0;      float q1APz=0;
  float q2APt=0;      float q2AEta=0;      float q2APhi=0;      float q2AE=0;      float q2APx=0;      float q2APy=0;      float q2APz=0;
  float q3APt=0;      float q3AEta=0;      float q3APhi=0;      float q3AE=0;      float q3APx=0;      float q3APy=0;      float q3APz=0;
  float q4APt=0;      float q4AEta=0;      float q4APhi=0;      float q4AE=0;      float q4APx=0;      float q4APy=0;      float q4APz=0;
  float q1BPt=0;      float q1BEta=0;      float q1BPhi=0;      float q1BE=0;      float q1BPx=0;      float q1BPy=0;      float q1BPz=0;
  float q2BPt=0;      float q2BEta=0;      float q2BPhi=0;      float q2BE=0;      float q2BPx=0;      float q2BPy=0;      float q2BPz=0;
  float l1BPt=0;      float l1BEta=0;      float l1BPhi=0;      float l1BE=0;      float l1BPx=0;      float l1BPy=0;      float l1BPz=0;
  float metPt=0;      float metEta=0;      float metPhi=0;      float metE=0;      float metPx=0;      float metPy=0;      float metPz=0;
  // Start event level variables 
  float alpha=0;       float alpha2=0; 
  float S=0;           float St = 0;         float A=0;            
  float mTens1=0;      float mTens2=0;       float mTens3=0; 
  // Start output variables info
  int mG=0;            int mX1=0;            int mN1=0;
  float dM1_1=0;       float dM1_2=0;
  float dM2_1=0;       float dM2_2=0;
  
  // Event generation loop
  for (int kk=0; kk<nEvents; kk++){

    while (true){
    
      if (genType == "randGen"){
        // Generate a random SUSY mass
        // Gluino mass (mG) from 1GeV to maxResonanceM
        mG = rand()%(int)(maxResonanceM-1)+1;
        mX1 = rand()%(int)(maxResonanceM-2)+1;
        mN1 = rand()%(int)(maxResonanceM-3);
        while (mG > maxResonanceM || mG <= mX1 || mX1 <= mN1){
          mG = rand()%(int)(maxResonanceM-1)+1;
          mX1 = rand()%(int)(maxResonanceM-2)+1;
          mN1 = rand()%(int)(maxResonanceM-3);
        }
      }
  
      // Print event status
      if (kk == 0) cout << "Event " << kk+1 << "/" << nEvents << ": mG=" << mG << "GeV, mX1=" << mX1 << "GeV, mN1=" << mN1 << "GeV" << endl;
      else if ((kk+1)%100000 == 0) cout << "Event " << kk+1 << "/" << nEvents << ": mG=" << mG << "GeV, mX1=" << mX1 << "GeV, mN1=" << mN1 << "GeV" << endl;
      else if ((kk+1)==nEvents) cout << "Event " << kk+1 << "/" << nEvents << ": mG=" << mG << "GeV, mX1=" << mX1 << "GeV, mN1=" << mN1 << "GeV" << endl;
   
      // Start TLorentzVectors 
      tlv pGA(0,0,0,0), pGB(0,0,0,0); 
      tlv pq1A(0,0,0,0), pq1B(0,0,0,0); 
      tlv pq2A(0,0,0,0), pq2B(0,0,0,0); 
      tlv pX1A(0,0,0,0), pX1B(0,0,0,0); 
      tlv pq3A(0,0,0,0), pl1B(0,0,0,0); 
      tlv pq4A(0,0,0,0), pv1B(0,0,0,0); 
      tlv pN1A(0,0,0,0), pN1B(0,0,0,0); 
      
      // Run generator  
      myGenerator::getKine_GG_XX_semiLep(mG, mX1, mN1,
                                            pGA, pq1A, pq2A, pX1A,
                                            pGB, pq1B, pq2B, pX1B,
                                            pq3A, pq4A, pN1A,
                                            pl1B, pv1B, pN1B);

      // Store visible objects and sort them if necessary
      vector<tlv> vJets = {pq1A, pq2A, pq3A, pq4A, pq1B, pq2B};
      vector<tlv> vLeps = {pl1B};
      if (visObjSortRule == "pT"){ 
        sort(vJets.begin(), vJets.end(), comparePt);
        sort(vLeps.begin(), vLeps.end(), comparePt);
      }

      // Update final state visible objects info
      q1APt = vJets[0].Pt();      q1AEta = vJets[0].Eta();      q1APhi = vJets[0].Phi();      q1AE = vJets[0].E();    
      q1APx = vJets[0].Px();      q1APy = vJets[0].Py();        q1APz = vJets[0].Pz();
      q2APt = vJets[1].Pt();      q2AEta = vJets[1].Eta();      q2APhi = vJets[1].Phi();      q2AE = vJets[1].E();
      q2APx = vJets[1].Px();      q2APy = vJets[1].Py();        q2APz = vJets[1].Pz();
      q3APt = vJets[2].Pt();      q3AEta = vJets[2].Eta();      q3APhi = vJets[2].Phi();      q3AE = vJets[2].E();
      q3APx = vJets[2].Px();      q3APy = vJets[2].Py();        q3APz = vJets[2].Pz();
      q4APt = vJets[3].Pt();      q4AEta = vJets[3].Eta();      q4APhi = vJets[3].Phi();      q4AE = vJets[3].E();    
      q4APx = vJets[3].Px();      q4APy = vJets[3].Py();        q4APz = vJets[3].Pz();
      q1BPt = vJets[4].Pt();      q1BEta = vJets[4].Eta();      q1BPhi = vJets[4].Phi();      q1BE = vJets[4].E();    
      q1BPx = vJets[4].Px();      q1BPy = vJets[4].Py();        q1BPz = vJets[4].Pz();
      q2BPt = vJets[5].Pt();      q2BEta = vJets[5].Eta();      q2BPhi = vJets[5].Phi();      q2BE = vJets[5].E();
      q2BPx = vJets[5].Px();      q2BPy = vJets[5].Py();        q2BPz = vJets[5].Pz();
      l1BPt = vLeps[0].Pt();      l1BEta = vLeps[0].Eta();      l1BPhi = vLeps[0].Phi();      l1BE = vLeps[0].E();
      l1BPx = vLeps[0].Px();      l1BPy = vLeps[0].Py();        l1BPz = vLeps[0].Pz();

      // Create MET
      tlv pMis = pN1A+pN1B+pv1B; pMis.SetXYZT(pMis.Px(), pMis.Py(), 0., 0.);

      // Update MET info
      metPt = pMis.Pt();      metPhi = pMis.Phi();      metE = pMis.E();      metPx = pMis.Px();      metPy = pMis.Py();      metPz = pMis.Pz();

      // Get event shape variables
      // Start by defining general momentum tensor
      TMatrixD matrix(3,3);
      float px2 = pow(q1APx,2)+pow(q2APx,2)+pow(q3APx,2)+pow(q4APx,2)+pow(q1BPx,2)+pow(q2BPx,2)+pow(l1BPx,2);
      float py2 = pow(q1APy,2)+pow(q2APy,2)+pow(q3APy,2)+pow(q4APy,2)+pow(q1BPy,2)+pow(q2BPy,2)+pow(l1BPy,2);
      float pz2 = pow(q1APz,2)+pow(q2APz,2)+pow(q3APz,2)+pow(q4APz,2)+pow(q1BPz,2)+pow(q2BPz,2)+pow(l1BPz,2);
      float pxpy = q1APx*q1APy+q2APx*q2APy+q3APx*q3APy+q4APx*q4APy+q1BPx*q1BPy+q2BPx*q2BPy+l1BPx*l1BPy;
      float pxpz = q1APx*q1APz+q2APx*q2APz+q3APx*q3APz+q4APx*q4APz+q1BPx*q1BPz+q2BPx*q2BPz+l1BPx*l1BPz;
      float pypz = q1APy*q1APz+q2APy*q2APz+q3APy*q3APz+q4APy*q4APz+q1BPy*q1BPz+q2BPy*q2BPz+l1BPy*l1BPz;
      TArrayD data(9);
      data[0] = px2;      data[3] = pxpy;      data[6] = pxpz;
      data[1] = pxpy;     data[4] = py2;       data[7] = pypz;
      data[2] = pxpz;     data[5] = pypz;      data[8] = pz2;
      matrix.SetMatrixArray(data.GetArray());
      // Get eigenvalues
      TMatrixDEigen eigen(matrix);
      TMatrixD diagMatrix = eigen.GetEigenValues();
      double lambda1 = TMatrixDRow(diagMatrix, 0)(0);
      double lambda2 = TMatrixDRow(diagMatrix, 1)(1);
      double lambda3 = TMatrixDRow(diagMatrix, 2)(2);
      vector<double> eigenVals = {lambda1, lambda2, lambda3};
      sort(eigenVals.begin(), eigenVals.end(), compareDoubles);
      // Normalize eigenvalues
      double eigenValNorm = lambda1+lambda2+lambda3;
      for (unsigned int i=0; i<eigenVals.size(); i++){
        eigenVals[i] = eigenVals[i]/eigenValNorm;
      }

      // Update event shape variables
      S = (3.0/2.0)*(eigenVals[1]+eigenVals[2]);
      St = (2.0*eigenVals[1])/(eigenVals[0]+eigenVals[1]);
      A = (3.0/2.0)*(eigenVals[2]);
      mTens1 = lambda1/eigenValNorm;
      mTens2 = lambda2/eigenValNorm;
      mTens3 = lambda3/eigenValNorm;
      
      // Set outputs
      int out1 = mG-mX1;
      int out2 = mX1-mN1;
      vector<int> outputs = {out1, out2};
      if (outputSortRule == "mag") sort(outputs.begin(), outputs.end(), compareOutputs);
      dM1_1 = outputs[0];      dM1_2 = outputs[0];
      dM2_1 = outputs[1];      dM2_2 = outputs[1];

      // Set alpha based on outputs
      alpha = (dM1_1-dM2_1)/(2*maxResonanceM)+0.5;
      if (dM1_1 > dM2_1) { alpha2 = 0; }
      else if (dM1_1 == dM2_1) { alpha2 = 0.5; }
      else {alpha2 = 1;}

      // Set more input variables
      if (inputsType >= 1){}
  
      // Added to avoid nans
      bool q1Anans = isnan(q1APt) || isnan(q1AEta) || isnan(q1APhi);
      bool q2Anans = isnan(q2APt) || isnan(q2AEta) || isnan(q2APhi);
      bool q3Anans = isnan(q3APt) || isnan(q3AEta) || isnan(q3APhi);
      bool q4Anans = isnan(q4APt) || isnan(q4AEta) || isnan(q4APhi);
      bool q1Bnans = isnan(q1BPt) || isnan(q1BEta) || isnan(q1BPhi);
      bool q2Bnans = isnan(q2BPt) || isnan(q2BEta) || isnan(q2BPhi);
      bool l1Bnans = isnan(l1BPt) || isnan(l1BEta) || isnan(l1BPhi);
      bool metnans = isnan(metPt) || isnan(metEta) || isnan(metPhi);
      bool nans = q1Anans || q2Anans || q3Anans || q4Anans || q1Bnans || q2Bnans || l1Bnans || metnans;

      if (nans) continue;
      else break;
    } 

    // Fill text file
    if (inputsType >=0){
      txtout << q1APt << "," << q1AEta << "," << q1APhi << "," << q1AE << "," << q1APx << "," << q1APy << "," << q1APz << ",";
      txtout << q2APt << "," << q2AEta << "," << q2APhi << "," << q2AE << "," << q2APx << "," << q2APy << "," << q2APz << ",";
      txtout << q3APt << "," << q3AEta << "," << q3APhi << "," << q3AE << "," << q3APx << "," << q3APy << "," << q3APz << ",";
      txtout << q4APt << "," << q4AEta << "," << q4APhi << "," << q4AE << "," << q4APx << "," << q4APy << "," << q4APz << ",";
      txtout << q1BPt << "," << q1BEta << "," << q1BPhi << "," << q1BE << "," << q1BPx << "," << q1BPy << "," << q1BPz << ",";
      txtout << q2BPt << "," << q2BEta << "," << q2BPhi << "," << q2BE << "," << q2BPx << "," << q2BPy << "," << q2BPz << ",";
      txtout << l1BPt << "," << l1BEta << "," << l1BPhi << "," << l1BE << "," << l1BPx << "," << l1BPy << "," << l1BPz << ",";
      txtout << metPt << "," << metEta << "," << metPhi << "," << metE << "," << metPx << "," << metPy << "," << metPz << ",";
      txtout << alpha << "," << alpha2 << ",";
      txtout << S << "," << St << "," << A << "," << mTens1 << "," << mTens2 << "," << mTens3 << ",";
      txtout << mG << "," << mX1 << "," << mN1 << ",";
      txtout << dM1_1 << "," << dM1_2 << "," << dM2_1 << "," << dM2_2;
    }
    if (inputsType >=1){}
    txtout << endl;
  }

  // Close file
  txtout.close();  
}

void getKine_GG_XX_WW_had_run(int genCode, string ext){

  // Start file
  fstream txtout;
  stringstream fileName;
  std::filesystem::create_directory("./data");
  fileName << "./data/genCode" << genCode << ext << ".csv";
  txtout.open(fileName.str(), ios::out);
    
  // Define file header
  stringstream header;
  if (inputsType >= 0){
    // Visible objects info
    header << "q1APt,q1AEta,q1APhi,q1AE,q1APx,q1APy,q1APz" << ",";
    header << "q2APt,q2AEta,q2APhi,q2AE,q2APx,q2APy,q2APz" << ",";
    header << "q3APt,q3AEta,q3APhi,q3AE,q3APx,q3APy,q3APz" << ",";
    header << "q4APt,q4AEta,q4APhi,q4AE,q4APx,q4APy,q4APz" << ",";
    header << "q1BPt,q1BEta,q1BPhi,q1BE,q1BPx,q1BPy,q1BPz" << ",";
    header << "q2BPt,q2BEta,q2BPhi,q2BE,q2BPx,q2BPy,q2BPz" << ",";
    header << "q3BPt,q3BEta,q3BPhi,q3BE,q3BPx,q3BPy,q3BPz" << ",";
    header << "q4BPt,q4BEta,q4BPhi,q4BE,q4BPx,q4BPy,q4BPz" << ",";
    header << "metPt,metEta,metPhi,metE,metPx,metPy,metPz" << ",";
    // Arbitrary variables
    header << "alpha,alpha2" << ",";
    // Event shape variables
    header << "S,St,A,mTens1,mTens2,mTens3" << ",";
    // Outputs
    header << "mG,mX1,mN1,dM1_1,dM1_2,dM2_1,dM2_2";
  }
  if (inputsType >= 1){}
  txtout << header.str() << endl;
  
  // Start random generator
  auto rng = default_random_engine {};
  
  // Start final state particle variables
  float q1APt=0;      float q1AEta=0;      float q1APhi=0;      float q1AE=0;      float q1APx=0;      float q1APy=0;      float q1APz=0;
  float q2APt=0;      float q2AEta=0;      float q2APhi=0;      float q2AE=0;      float q2APx=0;      float q2APy=0;      float q2APz=0;
  float q3APt=0;      float q3AEta=0;      float q3APhi=0;      float q3AE=0;      float q3APx=0;      float q3APy=0;      float q3APz=0;
  float q4APt=0;      float q4AEta=0;      float q4APhi=0;      float q4AE=0;      float q4APx=0;      float q4APy=0;      float q4APz=0;
  float q1BPt=0;      float q1BEta=0;      float q1BPhi=0;      float q1BE=0;      float q1BPx=0;      float q1BPy=0;      float q1BPz=0;
  float q2BPt=0;      float q2BEta=0;      float q2BPhi=0;      float q2BE=0;      float q2BPx=0;      float q2BPy=0;      float q2BPz=0;
  float q3BPt=0;      float q3BEta=0;      float q3BPhi=0;      float q3BE=0;      float q3BPx=0;      float q3BPy=0;      float q3BPz=0;
  float q4BPt=0;      float q4BEta=0;      float q4BPhi=0;      float q4BE=0;      float q4BPx=0;      float q4BPy=0;      float q4BPz=0;
  float metPt=0;      float metEta=0;      float metPhi=0;      float metE=0;      float metPx=0;      float metPy=0;      float metPz=0;
  // Start event level variables 
  float alpha=0;       float alpha2=0; 
  float S=0;           float St = 0;         float A=0;            
  float mTens1=0;      float mTens2=0;       float mTens3=0; 
  // Start output variables info
  int mG=0;            int mX1=0;            int mN1=0;
  float dM1_1=0;       float dM1_2=0;
  float dM2_1=0;       float dM2_2=0;
  
  // Event generation loop
  for (int kk=0; kk<nEvents; kk++){

    while (true){
    
      if (genType == "randGen"){
        // Generate a random SUSY mass
        // Gluino mass (mG) from 1GeV to maxResonanceM
        mG = rand()%(int)(maxResonanceM-1)+1;
        mX1 = rand()%(int)(maxResonanceM-2)+1;
        mN1 = rand()%(int)(maxResonanceM-3);
        while (mG > maxResonanceM || mG <= mX1 || mX1 <= mN1+0.1){
          mG = rand()%(int)(maxResonanceM-1)+1;
          mX1 = rand()%(int)(maxResonanceM-2)+1;
          mN1 = rand()%(int)(maxResonanceM-3);
        }
      }
  
      // Print event status
      if (kk == 0) cout << "Event " << kk+1 << "/" << nEvents << ": mG=" << mG << "GeV, mX1=" << mX1 << "GeV, mN1=" << mN1 << "GeV" << endl;
      else if ((kk+1)%100000 == 0) cout << "Event " << kk+1 << "/" << nEvents << ": mG=" << mG << "GeV, mX1=" << mX1 << "GeV, mN1=" << mN1 << "GeV" << endl;
      else if ((kk+1)==nEvents) cout << "Event " << kk+1 << "/" << nEvents << ": mG=" << mG << "GeV, mX1=" << mX1 << "GeV, mN1=" << mN1 << "GeV" << endl;
   
      // Start TLorentzVectors 
      tlv pGA(0,0,0,0), pGB(0,0,0,0); 
      tlv pq1A(0,0,0,0), pq1B(0,0,0,0); 
      tlv pq2A(0,0,0,0), pq2B(0,0,0,0); 
      tlv pX1A(0,0,0,0), pX1B(0,0,0,0); 
      tlv pW1A(0,0,0,0), pW2A(0,0,0,0);
      tlv pq3A(0,0,0,0), pq3B(0,0,0,0); 
      tlv pq4A(0,0,0,0), pq4B(0,0,0,0); 
      tlv pN1A(0,0,0,0), pN1B(0,0,0,0); 
      
      // Run generator
      if (mX1-mN1 <= 80.4)
        myGenerator::getKine_GG_XX_WW_had(mG, mX1, mN1, mX1-mN1-0.1,
                                          pGA, pq1A, pq2A, pX1A,
                                          pGB, pq1B, pq2B, pX1B,
                                          pW1A, pq3A, pq4A, pN1A,
                                          pW2A, pq3B, pq4B, pN1B);
      else
        myGenerator::getKine_GG_XX_WW_had(mG, mX1, mN1, 80.4,
                                          pGA, pq1A, pq2A, pX1A,
                                          pGB, pq1B, pq2B, pX1B,
                                          pW1A, pq3A, pq4A, pN1A,
                                          pW2A, pq3B, pq4B, pN1B);

      // Store visible objects and sort them if necessary
      vector<tlv> vObjs = {pq1A, pq2A, pq3A, pq4A, pq1B, pq2B, pq3B, pq4B};
      if (visObjSortRule == "pT") sort(vObjs.begin(), vObjs.end(), comparePt);

      // Update final state visible objects info
      q1APt = vObjs[0].Pt();      q1AEta = vObjs[0].Eta();      q1APhi = vObjs[0].Phi();      q1AE = vObjs[0].E();    
      q1APx = vObjs[0].Px();      q1APy = vObjs[0].Py();        q1APz = vObjs[0].Pz();
      q2APt = vObjs[1].Pt();      q2AEta = vObjs[1].Eta();      q2APhi = vObjs[1].Phi();      q2AE = vObjs[1].E();
      q2APx = vObjs[1].Px();      q2APy = vObjs[1].Py();        q2APz = vObjs[1].Pz();
      q3APt = vObjs[2].Pt();      q3AEta = vObjs[2].Eta();      q3APhi = vObjs[2].Phi();      q3AE = vObjs[2].E();
      q3APx = vObjs[2].Px();      q3APy = vObjs[2].Py();        q3APz = vObjs[2].Pz();
      q4APt = vObjs[3].Pt();      q4AEta = vObjs[3].Eta();      q4APhi = vObjs[3].Phi();      q4AE = vObjs[3].E();    
      q4APx = vObjs[3].Px();      q4APy = vObjs[3].Py();        q4APz = vObjs[3].Pz();
      q1BPt = vObjs[4].Pt();      q1BEta = vObjs[4].Eta();      q1BPhi = vObjs[4].Phi();      q1BE = vObjs[4].E();    
      q1BPx = vObjs[4].Px();      q1BPy = vObjs[4].Py();        q1BPz = vObjs[4].Pz();
      q2BPt = vObjs[5].Pt();      q2BEta = vObjs[5].Eta();      q2BPhi = vObjs[5].Phi();      q2BE = vObjs[5].E();
      q2BPx = vObjs[5].Px();      q2BPy = vObjs[5].Py();        q2BPz = vObjs[5].Pz();
      q3BPt = vObjs[6].Pt();      q3BEta = vObjs[6].Eta();      q3BPhi = vObjs[6].Phi();      q3BE = vObjs[6].E();
      q3BPx = vObjs[6].Px();      q3BPy = vObjs[6].Py();        q3BPz = vObjs[6].Pz();
      q4BPt = vObjs[7].Pt();      q4BEta = vObjs[7].Eta();      q4BPhi = vObjs[7].Phi();      q4BE = vObjs[7].E();
      q4BPx = vObjs[7].Px();      q4BPy = vObjs[7].Py();        q4BPz = vObjs[7].Pz();

      // Create MET
      tlv pMis = pN1A+pN1B; pMis.SetXYZT(pMis.Px(), pMis.Py(), 0., 0.);

      // Update MET info
      metPt = pMis.Pt();      metPhi = pMis.Phi();      metE = pMis.E();      metPx = pMis.Px();      metPy = pMis.Py();      metPz = pMis.Pz();

      // Get event shape variables
      // Start by defining general momentum tensor
      TMatrixD matrix(3,3);
      float px2 = pow(q1APx,2)+pow(q2APx,2)+pow(q3APx,2)+pow(q4APx,2)+pow(q1BPx,2)+pow(q2BPx,2)+pow(q3BPx,2)+pow(q4BPx,2);
      float py2 = pow(q1APy,2)+pow(q2APy,2)+pow(q3APy,2)+pow(q4APy,2)+pow(q1BPy,2)+pow(q2BPy,2)+pow(q3BPy,2)+pow(q4BPy,2);
      float pz2 = pow(q1APz,2)+pow(q2APz,2)+pow(q3APz,2)+pow(q4APz,2)+pow(q1BPz,2)+pow(q2BPz,2)+pow(q3BPz,2)+pow(q4BPz,2);
      float pxpy = q1APx*q1APy+q2APx*q2APy+q3APx*q3APy+q4APx*q4APy+q1BPx*q1BPy+q2BPx*q2BPy+q3BPx*q3BPy+q4BPx*q4BPy;
      float pxpz = q1APx*q1APz+q2APx*q2APz+q3APx*q3APz+q4APx*q4APz+q1BPx*q1BPz+q2BPx*q2BPz+q3BPx*q3BPz+q4BPx*q4BPz;
      float pypz = q1APy*q1APz+q2APy*q2APz+q3APy*q3APz+q4APy*q4APz+q1BPy*q1BPz+q2BPy*q2BPz+q3BPy*q3BPz+q4BPy*q4BPz;
      TArrayD data(9);
      data[0] = px2;      data[3] = pxpy;      data[6] = pxpz;
      data[1] = pxpy;     data[4] = py2;       data[7] = pypz;
      data[2] = pxpz;     data[5] = pypz;      data[8] = pz2;
      matrix.SetMatrixArray(data.GetArray());
      // Get eigenvalues
      TMatrixDEigen eigen(matrix);
      TMatrixD diagMatrix = eigen.GetEigenValues();
      double lambda1 = TMatrixDRow(diagMatrix, 0)(0);
      double lambda2 = TMatrixDRow(diagMatrix, 1)(1);
      double lambda3 = TMatrixDRow(diagMatrix, 2)(2);
      vector<double> eigenVals = {lambda1, lambda2, lambda3};
      sort(eigenVals.begin(), eigenVals.end(), compareDoubles);
      // Normalize eigenvalues
      double eigenValNorm = lambda1+lambda2+lambda3;
      for (unsigned int i=0; i<eigenVals.size(); i++){
        eigenVals[i] = eigenVals[i]/eigenValNorm;
      }

      // Update event shape variables
      S = (3.0/2.0)*(eigenVals[1]+eigenVals[2]);
      St = (2.0*eigenVals[1])/(eigenVals[0]+eigenVals[1]);
      A = (3.0/2.0)*(eigenVals[2]);
      mTens1 = lambda1/eigenValNorm;
      mTens2 = lambda2/eigenValNorm;
      mTens3 = lambda3/eigenValNorm;
      
      // Set outputs
      int out1 = mG-mX1;
      int out2 = mX1-mN1;
      vector<int> outputs = {out1, out2};
      if (outputSortRule == "mag") sort(outputs.begin(), outputs.end(), compareOutputs);
      dM1_1 = outputs[0];      dM1_2 = outputs[0];
      dM2_1 = outputs[1];      dM2_2 = outputs[1];

      // Set alpha based on outputs
      alpha = (dM1_1-dM2_1)/(2*maxResonanceM)+0.5;
      if (dM1_1 > dM2_1) { alpha2 = 0; }
      else if (dM1_1 == dM2_1) { alpha2 = 0.5; }
      else {alpha2 = 1;}
  
      // Set more input variables
      if (inputsType >= 1){}
      
      // Added to avoid nans
      bool q1Anans = isnan(q1APt) || isnan(q1AEta) || isnan(q1APhi);
      bool q2Anans = isnan(q2APt) || isnan(q2AEta) || isnan(q2APhi);
      bool q3Anans = isnan(q3APt) || isnan(q3AEta) || isnan(q3APhi);
      bool q4Anans = isnan(q4APt) || isnan(q4AEta) || isnan(q4APhi);
      bool q1Bnans = isnan(q1BPt) || isnan(q1BEta) || isnan(q1BPhi);
      bool q2Bnans = isnan(q2BPt) || isnan(q2BEta) || isnan(q2BPhi);
      bool q3Bnans = isnan(q3BPt) || isnan(q3BEta) || isnan(q3BPhi);
      bool q4Bnans = isnan(q4BPt) || isnan(q4BEta) || isnan(q4BPhi);
      bool metnans = isnan(metPt) || isnan(metEta) || isnan(metPhi);
      bool nans = q1Anans || q2Anans || q3Anans || q4Anans || q1Bnans || q2Bnans || q3Bnans || q4Bnans || metnans;

      if (nans){ 
        cout << "Found nans..." << endl;
        cout << "mG=" << mG << "GeV, mX1=" << mX1 << "GeV, mN1=" << mN1 << "GeV" << endl;
        continue;
      }
      else break;
    } 

    // Fill text file
    if (inputsType >=0){
      txtout << q1APt << "," << q1AEta << "," << q1APhi << "," << q1AE << "," << q1APx << "," << q1APy << "," << q1APz << ",";
      txtout << q2APt << "," << q2AEta << "," << q2APhi << "," << q2AE << "," << q2APx << "," << q2APy << "," << q2APz << ",";
      txtout << q3APt << "," << q3AEta << "," << q3APhi << "," << q3AE << "," << q3APx << "," << q3APy << "," << q3APz << ",";
      txtout << q4APt << "," << q4AEta << "," << q4APhi << "," << q4AE << "," << q4APx << "," << q4APy << "," << q4APz << ",";
      txtout << q1BPt << "," << q1BEta << "," << q1BPhi << "," << q1BE << "," << q1BPx << "," << q1BPy << "," << q1BPz << ",";
      txtout << q2BPt << "," << q2BEta << "," << q2BPhi << "," << q2BE << "," << q2BPx << "," << q2BPy << "," << q2BPz << ",";
      txtout << q3BPt << "," << q3BEta << "," << q3BPhi << "," << q3BE << "," << q3BPx << "," << q3BPy << "," << q3BPz << ",";
      txtout << q4BPt << "," << q4BEta << "," << q4BPhi << "," << q4BE << "," << q4BPx << "," << q4BPy << "," << q4BPz << ",";
      txtout << metPt << "," << metEta << "," << metPhi << "," << metE << "," << metPx << "," << metPy << "," << metPz << ",";
      txtout << alpha << "," << alpha2 << ",";
      txtout << S << "," << St << "," << A << "," << mTens1 << "," << mTens2 << "," << mTens3 << ",";
      txtout << mG << "," << mX1 << "," << mN1 << ",";
      txtout << dM1_1 << "," << dM1_2 << "," << dM2_1 << "," << dM2_2;
    }
    if (inputsType >=1){}
    txtout << endl;
  }

  // Close file
  txtout.close();  
}

void getKine_GG_XX_WW_semiLep_run(int genCode, string ext){

  // Start file
  fstream txtout;
  stringstream fileName;
  std::filesystem::create_directory("./data");
  fileName << "./data/genCode" << genCode << ext << ".csv";
  txtout.open(fileName.str(), ios::out);
    
  // Define file header
  stringstream header;
  if (inputsType >= 0){
    // Visible objects info
    header << "q1APt,q1AEta,q1APhi,q1AE,q1APx,q1APy,q1APz" << ",";
    header << "q2APt,q2AEta,q2APhi,q2AE,q2APx,q2APy,q2APz" << ",";
    header << "q3APt,q3AEta,q3APhi,q3AE,q3APx,q3APy,q3APz" << ",";
    header << "q4APt,q4AEta,q4APhi,q4AE,q4APx,q4APy,q4APz" << ",";
    header << "q1BPt,q1BEta,q1BPhi,q1BE,q1BPx,q1BPy,q1BPz" << ",";
    header << "q2BPt,q2BEta,q2BPhi,q2BE,q2BPx,q2BPy,q2BPz" << ",";
    header << "l1BPt,l1BEta,l1BPhi,l1BE,l1BPx,l1BPy,l1BPz" << ",";
    header << "metPt,metEta,metPhi,metE,metPx,metPy,metPz" << ",";
    // Arbitrary variables
    header << "alpha,alpha2" << ",";
    // Event shape variables
    header << "S,St,A,mTens1,mTens2,mTens3" << ",";
    // Outputs
    header << "mG,mX1,mN1,dM1_1,dM1_2,dM2_1,dM2_2";
  }
  if (inputsType >= 1){}
  txtout << header.str() << endl;
  
  // Start random generator
  auto rng = default_random_engine {};
  
  // Start final state particle variables
  float q1APt=0;      float q1AEta=0;      float q1APhi=0;      float q1AE=0;      float q1APx=0;      float q1APy=0;      float q1APz=0;
  float q2APt=0;      float q2AEta=0;      float q2APhi=0;      float q2AE=0;      float q2APx=0;      float q2APy=0;      float q2APz=0;
  float q3APt=0;      float q3AEta=0;      float q3APhi=0;      float q3AE=0;      float q3APx=0;      float q3APy=0;      float q3APz=0;
  float q4APt=0;      float q4AEta=0;      float q4APhi=0;      float q4AE=0;      float q4APx=0;      float q4APy=0;      float q4APz=0;
  float q1BPt=0;      float q1BEta=0;      float q1BPhi=0;      float q1BE=0;      float q1BPx=0;      float q1BPy=0;      float q1BPz=0;
  float q2BPt=0;      float q2BEta=0;      float q2BPhi=0;      float q2BE=0;      float q2BPx=0;      float q2BPy=0;      float q2BPz=0;
  float l1BPt=0;      float l1BEta=0;      float l1BPhi=0;      float l1BE=0;      float l1BPx=0;      float l1BPy=0;      float l1BPz=0;
  float metPt=0;      float metEta=0;      float metPhi=0;      float metE=0;      float metPx=0;      float metPy=0;      float metPz=0;
  // Start event level variables 
  float alpha=0;       float alpha2=0; 
  float S=0;           float St = 0;         float A=0;            
  float mTens1=0;      float mTens2=0;       float mTens3=0; 
  // Start output variables info
  int mG=0;            int mX1=0;            int mN1=0;
  float dM1_1=0;       float dM1_2=0;
  float dM2_1=0;       float dM2_2=0;
  
  // Event generation loop
  for (int kk=0; kk<nEvents; kk++){

    while (true){
    
      if (genType == "randGen"){
        // Generate a random SUSY mass
        // Gluino mass (mG) from 1GeV to maxResonanceM
        mG = rand()%(int)(maxResonanceM-1)+1;
        mX1 = rand()%(int)(maxResonanceM-2)+1;
        mN1 = rand()%(int)(maxResonanceM-3);
        while (mG > maxResonanceM || mG <= mX1 || mX1 <= mN1+0.1){
          mG = rand()%(int)(maxResonanceM-1)+1;
          mX1 = rand()%(int)(maxResonanceM-2)+1;
          mN1 = rand()%(int)(maxResonanceM-3);
        }
      }
  
      // Print event status
      if (kk == 0) cout << "Event " << kk+1 << "/" << nEvents << ": mG=" << mG << "GeV, mX1=" << mX1 << "GeV, mN1=" << mN1 << "GeV" << endl;
      else if ((kk+1)%100000 == 0) cout << "Event " << kk+1 << "/" << nEvents << ": mG=" << mG << "GeV, mX1=" << mX1 << "GeV, mN1=" << mN1 << "GeV" << endl;
      else if ((kk+1)==nEvents) cout << "Event " << kk+1 << "/" << nEvents << ": mG=" << mG << "GeV, mX1=" << mX1 << "GeV, mN1=" << mN1 << "GeV" << endl;
   
      // Start TLorentzVectors
      tlv pGA(0,0,0,0), pGB(0,0,0,0); 
      tlv pq1A(0,0,0,0), pq1B(0,0,0,0); 
      tlv pq2A(0,0,0,0), pq2B(0,0,0,0); 
      tlv pX1A(0,0,0,0), pX1B(0,0,0,0); 
      tlv pW1A(0,0,0,0), pW2A(0,0,0,0);
      tlv pq3A(0,0,0,0), pl1B(0,0,0,0); 
      tlv pq4A(0,0,0,0), pv1B(0,0,0,0); 
      tlv pN1A(0,0,0,0), pN1B(0,0,0,0); 
      
      // Run generator
      if (mX1-mN1 < 80.4) 
        myGenerator::getKine_GG_XX_WW_semiLep(mG, mX1, mN1, mX1-mN1-0.1,
                                              pGA, pq1A, pq2A, pX1A,
                                              pGB, pq1B, pq2B, pX1B,
                                              pW1A, pq3A, pq4A, pN1A,
                                              pW2A, pl1B, pv1B, pN1B);
      else
        myGenerator::getKine_GG_XX_WW_semiLep(mG, mX1, mN1, 80.4,
                                              pGA, pq1A, pq2A, pX1A,
                                              pGB, pq1B, pq2B, pX1B,
                                              pW1A, pq3A, pq4A, pN1A,
                                              pW2A, pl1B, pv1B, pN1B);

      // Store visible objects and sort them if necessary
      vector<tlv> vJets = {pq1A, pq2A, pq3A, pq4A, pq1B, pq2B};
      vector<tlv> vLeps = {pl1B};
      if (visObjSortRule == "pT"){ 
        sort(vJets.begin(), vJets.end(), comparePt);
        sort(vLeps.begin(), vLeps.end(), comparePt);
      }

      // Update final state visible objects info
      q1APt = vJets[0].Pt();      q1AEta = vJets[0].Eta();      q1APhi = vJets[0].Phi();      q1AE = vJets[0].E();    
      q1APx = vJets[0].Px();      q1APy = vJets[0].Py();        q1APz = vJets[0].Pz();
      q2APt = vJets[1].Pt();      q2AEta = vJets[1].Eta();      q2APhi = vJets[1].Phi();      q2AE = vJets[1].E();
      q2APx = vJets[1].Px();      q2APy = vJets[1].Py();        q2APz = vJets[1].Pz();
      q3APt = vJets[2].Pt();      q3AEta = vJets[2].Eta();      q3APhi = vJets[2].Phi();      q3AE = vJets[2].E();
      q3APx = vJets[2].Px();      q3APy = vJets[2].Py();        q3APz = vJets[2].Pz();
      q4APt = vJets[3].Pt();      q4AEta = vJets[3].Eta();      q4APhi = vJets[3].Phi();      q4AE = vJets[3].E();    
      q4APx = vJets[3].Px();      q4APy = vJets[3].Py();        q4APz = vJets[3].Pz();
      q1BPt = vJets[4].Pt();      q1BEta = vJets[4].Eta();      q1BPhi = vJets[4].Phi();      q1BE = vJets[4].E();    
      q1BPx = vJets[4].Px();      q1BPy = vJets[4].Py();        q1BPz = vJets[4].Pz();
      q2BPt = vJets[5].Pt();      q2BEta = vJets[5].Eta();      q2BPhi = vJets[5].Phi();      q2BE = vJets[5].E();
      q2BPx = vJets[5].Px();      q2BPy = vJets[5].Py();        q2BPz = vJets[5].Pz();
      l1BPt = vLeps[0].Pt();      l1BEta = vLeps[0].Eta();      l1BPhi = vLeps[0].Phi();      l1BE = vLeps[0].E();
      l1BPx = vLeps[0].Px();      l1BPy = vLeps[0].Py();        l1BPz = vLeps[0].Pz();

      // Create MET
      tlv pMis = pN1A+pN1B+pv1B; pMis.SetXYZT(pMis.Px(), pMis.Py(), 0., 0.);

      // Update MET info
      metPt = pMis.Pt();      metPhi = pMis.Phi();      metE = pMis.E();      metPx = pMis.Px();      metPy = pMis.Py();      metPz = pMis.Pz();

      // Get event shape variables
      // Start by defining general momentum tensor
      TMatrixD matrix(3,3);
      float px2 = pow(q1APx,2)+pow(q2APx,2)+pow(q3APx,2)+pow(q4APx,2)+pow(q1BPx,2)+pow(q2BPx,2)+pow(l1BPx,2);
      float py2 = pow(q1APy,2)+pow(q2APy,2)+pow(q3APy,2)+pow(q4APy,2)+pow(q1BPy,2)+pow(q2BPy,2)+pow(l1BPy,2);
      float pz2 = pow(q1APz,2)+pow(q2APz,2)+pow(q3APz,2)+pow(q4APz,2)+pow(q1BPz,2)+pow(q2BPz,2)+pow(l1BPz,2);
      float pxpy = q1APx*q1APy+q2APx*q2APy+q3APx*q3APy+q4APx*q4APy+q1BPx*q1BPy+q2BPx*q2BPy+l1BPx*l1BPy;
      float pxpz = q1APx*q1APz+q2APx*q2APz+q3APx*q3APz+q4APx*q4APz+q1BPx*q1BPz+q2BPx*q2BPz+l1BPx*l1BPz;
      float pypz = q1APy*q1APz+q2APy*q2APz+q3APy*q3APz+q4APy*q4APz+q1BPy*q1BPz+q2BPy*q2BPz+l1BPy*l1BPz;
      TArrayD data(9);
      data[0] = px2;      data[3] = pxpy;      data[6] = pxpz;
      data[1] = pxpy;     data[4] = py2;       data[7] = pypz;
      data[2] = pxpz;     data[5] = pypz;      data[8] = pz2;
      matrix.SetMatrixArray(data.GetArray());
      // Get eigenvalues
      TMatrixDEigen eigen(matrix);
      TMatrixD diagMatrix = eigen.GetEigenValues();
      double lambda1 = TMatrixDRow(diagMatrix, 0)(0);
      double lambda2 = TMatrixDRow(diagMatrix, 1)(1);
      double lambda3 = TMatrixDRow(diagMatrix, 2)(2);
      vector<double> eigenVals = {lambda1, lambda2, lambda3};
      sort(eigenVals.begin(), eigenVals.end(), compareDoubles);
      // Normalize eigenvalues
      double eigenValNorm = lambda1+lambda2+lambda3;
      for (unsigned int i=0; i<eigenVals.size(); i++){
        eigenVals[i] = eigenVals[i]/eigenValNorm;
      }

      // Update event shape variables
      S = (3.0/2.0)*(eigenVals[1]+eigenVals[2]);
      St = (2.0*eigenVals[1])/(eigenVals[0]+eigenVals[1]);
      A = (3.0/2.0)*(eigenVals[2]);
      mTens1 = lambda1/eigenValNorm;
      mTens2 = lambda2/eigenValNorm;
      mTens3 = lambda3/eigenValNorm;
      
      // Set outputs
      int out1 = mG-mX1;
      int out2 = mX1-mN1;
      vector<int> outputs = {out1, out2};
      if (outputSortRule == "mag") sort(outputs.begin(), outputs.end(), compareOutputs);
      dM1_1 = outputs[0];      dM1_2 = outputs[0];
      dM2_1 = outputs[1];      dM2_2 = outputs[1];

      // Set alpha based on outputs
      alpha = (dM1_1-dM2_1)/(2*maxResonanceM)+0.5;
      if (dM1_1 > dM2_1) { alpha2 = 0; }
      else if (dM1_1 == dM2_1) { alpha2 = 0.5; }
      else {alpha2 = 1;}

      // Set more input variables
      if (inputsType >= 1){}
  
      // Added to avoid nans
      bool q1Anans = isnan(q1APt) || isnan(q1AEta) || isnan(q1APhi);
      bool q2Anans = isnan(q2APt) || isnan(q2AEta) || isnan(q2APhi);
      bool q3Anans = isnan(q3APt) || isnan(q3AEta) || isnan(q3APhi);
      bool q4Anans = isnan(q4APt) || isnan(q4AEta) || isnan(q4APhi);
      bool q1Bnans = isnan(q1BPt) || isnan(q1BEta) || isnan(q1BPhi);
      bool q2Bnans = isnan(q2BPt) || isnan(q2BEta) || isnan(q2BPhi);
      bool l1Bnans = isnan(l1BPt) || isnan(l1BEta) || isnan(l1BPhi);
      bool metnans = isnan(metPt) || isnan(metEta) || isnan(metPhi);
      bool nans = q1Anans || q2Anans || q3Anans || q4Anans || q1Bnans || q2Bnans || l1Bnans || metnans;

      if (nans) continue;
      else break;
    } 

    // Fill text file
    if (inputsType >=0){
      txtout << q1APt << "," << q1AEta << "," << q1APhi << "," << q1AE << "," << q1APx << "," << q1APy << "," << q1APz << ",";
      txtout << q2APt << "," << q2AEta << "," << q2APhi << "," << q2AE << "," << q2APx << "," << q2APy << "," << q2APz << ",";
      txtout << q3APt << "," << q3AEta << "," << q3APhi << "," << q3AE << "," << q3APx << "," << q3APy << "," << q3APz << ",";
      txtout << q4APt << "," << q4AEta << "," << q4APhi << "," << q4AE << "," << q4APx << "," << q4APy << "," << q4APz << ",";
      txtout << q1BPt << "," << q1BEta << "," << q1BPhi << "," << q1BE << "," << q1BPx << "," << q1BPy << "," << q1BPz << ",";
      txtout << q2BPt << "," << q2BEta << "," << q2BPhi << "," << q2BE << "," << q2BPx << "," << q2BPy << "," << q2BPz << ",";
      txtout << l1BPt << "," << l1BEta << "," << l1BPhi << "," << l1BE << "," << l1BPx << "," << l1BPy << "," << l1BPz << ",";
      txtout << metPt << "," << metEta << "," << metPhi << "," << metE << "," << metPx << "," << metPy << "," << metPz << ",";
      txtout << alpha << "," << alpha2 << ",";
      txtout << S << "," << St << "," << A << "," << mTens1 << "," << mTens2 << "," << mTens3 << ",";
      txtout << mG << "," << mX1 << "," << mN1 << ",";
      txtout << dM1_1 << "," << dM1_2 << "," << dM2_1 << "," << dM2_2;
    }
    if (inputsType >=1){}
    txtout << endl;
  }

  // Close file
  txtout.close();  
}

void getKine_GG_run(int genCode, string ext){
  
  // Start file
  fstream txtout;
  stringstream fileName;
  std::filesystem::create_directory("./data");
  fileName << "./data/genCode" << genCode << ext << ".csv";
  txtout.open(fileName.str(), ios::out);
    
  // Define file header
  stringstream header;
  if (inputsType >= 0){
    // Visible objects info
    header << "q1APt,q1AEta,q1APhi,q1AE,q1APx,q1APy,q1APz" << ",";
    header << "q2APt,q2AEta,q2APhi,q2AE,q2APx,q2APy,q2APz" << ",";
    header << "q1BPt,q1BEta,q1BPhi,q1BE,q1BPx,q1BPy,q1BPz" << ",";
    header << "q2BPt,q2BEta,q2BPhi,q2BE,q2BPx,q2BPy,q2BPz" << ",";
    header << "metPt,metEta,metPhi,metE,metPx,metPy,metPz" << ",";
    // Arbitrary variables
    header << "alpha,alpha2" << ",";
    // Event shape variables
    header << "S,St,A,mTens1,mTens2,mTens3" << ",";
    // Outputs
    header << "mG,mN1,dM1_1,dM1_2";
  }
  if (inputsType >= 1){}
  txtout << header.str() << endl;
  
  // Start random generator
  auto rng = default_random_engine {};
  
  // Start final state particle variables
  float q1APt=0;      float q1AEta=0;      float q1APhi=0;      float q1AE=0;      float q1APx=0;      float q1APy=0;      float q1APz=0;
  float q2APt=0;      float q2AEta=0;      float q2APhi=0;      float q2AE=0;      float q2APx=0;      float q2APy=0;      float q2APz=0;
  float q1BPt=0;      float q1BEta=0;      float q1BPhi=0;      float q1BE=0;      float q1BPx=0;      float q1BPy=0;      float q1BPz=0;
  float q2BPt=0;      float q2BEta=0;      float q2BPhi=0;      float q2BE=0;      float q2BPx=0;      float q2BPy=0;      float q2BPz=0;
  float metPt=0;      float metEta=0;      float metPhi=0;      float metE=0;      float metPx=0;      float metPy=0;      float metPz=0;
  // Start event level variables 
  float alpha=0;       float alpha2=0; 
  float S=0;           float St = 0;         float A=0;            
  float mTens1=0;      float mTens2=0;       float mTens3=0; 
  // Start output variables info
  int mG=0;            int mN1=0;
  float dM1_1=0;       float dM1_2=0;
  
  // Event generation loop
  for (int kk=0; kk<nEvents; kk++){

    while (true){

      if (genType == "randGen"){
        // Generate a random SUSY mass
        // Gluino mass (mG) from 1GeV to maxMass
        mG = rand()%(int)(maxResonanceM-1)+1;
        mN1 = rand()%(int)(maxResonanceM-1);
        while (mG > maxResonanceM || mG <= mN1){
          mG = rand()%(int)(maxResonanceM-1)+1;
          mN1 = rand()%(int)(maxResonanceM-1);
        }
        dM1_1 = mG-mN1;
        dM1_2 = dM1_1;
      }
      
      // Print event status
      if (kk == 0) cout << "Event " << kk+1 << "/" << nEvents << ": mG=" << mG << "GeV, mN1=" << mN1 << "GeV" << endl;
      else if ((kk+1)%100000 == 0) cout << "Event " << kk+1 << "/" << nEvents << ": mG=" << mG << "GeV, mN1=" << mN1 << "GeV" << endl;
      else if ((kk+1)==nEvents) cout << "Event " << kk+1 << "/" << nEvents << ": mG=" << mG << "GeV, mN1=" << mN1 << "GeV" << endl;

      
      // Start TLorentzVectors
      tlv pGA(0,0,0,0), pGB(0,0,0,0); 
      tlv pq1A(0,0,0,0), pq1B(0,0,0,0); 
      tlv pq2A(0,0,0,0), pq2B(0,0,0,0); 
      tlv pN1A(0,0,0,0), pN1B(0,0,0,0); 
  
      // Run generator  
      myGenerator::getKine_GG(mG, mN1,
                              pGA, pq1A, pq2A, pN1A,
                              pGB, pq1B, pq2B, pN1B);
      
      // Store visible objects and sort them if necessary
      vector<tlv> vObjs = {pq1A, pq2A, pq1B, pq2B};
      if (visObjSortRule == "pT") sort(vObjs.begin(), vObjs.end(), comparePt);

      // Update final state visible objects info
      q1APt = vObjs[0].Pt();      q1AEta = vObjs[0].Eta();      q1APhi = vObjs[0].Phi();      q1AE = vObjs[0].E();    
      q1APx = vObjs[0].Px();      q1APy = vObjs[0].Py();        q1APz = vObjs[0].Pz();
      q2APt = vObjs[1].Pt();      q2AEta = vObjs[1].Eta();      q2APhi = vObjs[1].Phi();      q2AE = vObjs[1].E();
      q2APx = vObjs[1].Px();      q2APy = vObjs[1].Py();        q2APz = vObjs[1].Pz();
      q1BPt = vObjs[2].Pt();      q1BEta = vObjs[2].Eta();      q1BPhi = vObjs[2].Phi();      q1BE = vObjs[2].E();    
      q1BPx = vObjs[2].Px();      q1BPy = vObjs[2].Py();        q1BPz = vObjs[2].Pz();
      q2BPt = vObjs[3].Pt();      q2BEta = vObjs[3].Eta();      q2BPhi = vObjs[3].Phi();      q2BE = vObjs[3].E();
      q2BPx = vObjs[3].Px();      q2BPy = vObjs[3].Py();        q2BPz = vObjs[3].Pz();

      // Create MET
      tlv pMis = pN1A+pN1B; pMis.SetXYZT(pMis.Px(), pMis.Py(), 0., 0.);

      // Update MET info
      metPt = pMis.Pt();      metPhi = pMis.Phi();      metE = pMis.E();      metPx = pMis.Px();      metPy = pMis.Py();      metPz = pMis.Pz();

      // Get event shape variables
      // Start by defining general momentum tensor
      TMatrixD matrix(3,3);
      float px2 = pow(q1APx,2)+pow(q2APx,2)+pow(q1BPx,2)+pow(q2BPx,2);
      float py2 = pow(q1APy,2)+pow(q2APy,2)+pow(q1BPy,2)+pow(q2BPy,2);
      float pz2 = pow(q1APz,2)+pow(q2APz,2)+pow(q1BPz,2)+pow(q2BPz,2);
      float pxpy = q1APx*q1APy+q2APx*q2APy+q1BPx*q1BPy+q2BPx*q2BPy;
      float pxpz = q1APx*q1APz+q2APx*q2APz+q1BPx*q1BPz+q2BPx*q2BPz;
      float pypz = q1APy*q1APz+q2APy*q2APz+q1BPy*q1BPz+q2BPy*q2BPz;
      TArrayD data(9);
      data[0] = px2;      data[3] = pxpy;      data[6] = pxpz;
      data[1] = pxpy;     data[4] = py2;       data[7] = pypz;
      data[2] = pxpz;     data[5] = pypz;      data[8] = pz2;
      matrix.SetMatrixArray(data.GetArray());
      // Get eigenvalues
      TMatrixDEigen eigen(matrix);
      TMatrixD diagMatrix = eigen.GetEigenValues();
      double lambda1 = TMatrixDRow(diagMatrix, 0)(0);
      double lambda2 = TMatrixDRow(diagMatrix, 1)(1);
      double lambda3 = TMatrixDRow(diagMatrix, 2)(2);
      vector<double> eigenVals = {lambda1, lambda2, lambda3};
      sort(eigenVals.begin(), eigenVals.end(), compareDoubles);
      // Normalize eigenvalues
      double eigenValNorm = lambda1+lambda2+lambda3;
      for (unsigned int i=0; i<eigenVals.size(); i++){
        eigenVals[i] = eigenVals[i]/eigenValNorm;
      }

      // Update event shape variables
      S = (3.0/2.0)*(eigenVals[1]+eigenVals[2]);
      St = (2.0*eigenVals[1])/(eigenVals[0]+eigenVals[1]);
      A = (3.0/2.0)*(eigenVals[2]);
      mTens1 = lambda1/eigenValNorm;
      mTens2 = lambda2/eigenValNorm;
      mTens3 = lambda3/eigenValNorm;
      
      // Set outputs
      int out1 = mG-mN1;
      vector<int> outputs = {out1};
      if (outputSortRule == "mag") sort(outputs.begin(), outputs.end(), compareOutputs);
      dM1_1 = outputs[0];      dM1_2 = outputs[0];
      
      // Set alpha based on outputs
      alpha = -1;
      alpha2 = -1;
    
      // Set more input variables
      if (inputsType >= 1){};
      
      // Added to avoid nans
      bool q1Anans = isnan(q1APt) || isnan(q1AEta) || isnan(q1APhi);
      bool q2Anans = isnan(q2APt) || isnan(q2AEta) || isnan(q2APhi);
      bool q1Bnans = isnan(q1BPt) || isnan(q1BEta) || isnan(q1BPhi);
      bool q2Bnans = isnan(q2BPt) || isnan(q2BEta) || isnan(q2BPhi);
      bool metnans = isnan(metPt) || isnan(metEta) || isnan(metPhi);
      bool nans = q1Anans || q2Anans || q1Bnans || q2Bnans || metnans;

      if (nans) continue;
      else break;
    } 

    // Fill text file
    if (inputsType >=0){
      txtout << q1APt << "," << q1AEta << "," << q1APhi << "," << q1AE << "," << q1APx << "," << q1APy << "," << q1APz << ",";
      txtout << q2APt << "," << q2AEta << "," << q2APhi << "," << q2AE << "," << q2APx << "," << q2APy << "," << q2APz << ",";
      txtout << q1BPt << "," << q1BEta << "," << q1BPhi << "," << q1BE << "," << q1BPx << "," << q1BPy << "," << q1BPz << ",";
      txtout << q2BPt << "," << q2BEta << "," << q2BPhi << "," << q2BE << "," << q2BPx << "," << q2BPy << "," << q2BPz << ",";
      txtout << metPt << "," << metEta << "," << metPhi << "," << metE << "," << metPx << "," << metPy << "," << metPz << ",";
      txtout << alpha << "," << alpha2 << ",";
      txtout << S << "," << St << "," << A << "," << mTens1 << "," << mTens2 << "," << mTens3 << ",";
      txtout << mG << "," << mN1 << ",";
      txtout << dM1_1 << "," << dM1_2;
    }
    if (inputsType >= 1){};
    txtout << endl;
  }

  // Close file
  txtout.close();  
}

void generateEvent(string process, int genCode){
  cout << "----- Event generation -----" << endl;
  if (process == "getKine_GG_XX_had"){
    if (fileType == "both"){
      cout << "Generating train dataset..." << endl;
      getKine_GG_XX_had_run(genCode, "train");
      cout << "Generating test dataset..." << endl;
      getKine_GG_XX_had_run(genCode, "test");
    } 
    else{ 
      cout << "Generating " << fileType << "dataset..." << endl;
      getKine_GG_XX_had_run(genCode, fileType);
    }
  }
  else if (process == "getKine_GG_XX_semiLep"){
    if (fileType == "both"){
      cout << "Generating train dataset..." << endl;
      getKine_GG_XX_semiLep_run(genCode, "train");
      cout << "Generating test dataset..." << endl;
      getKine_GG_XX_semiLep_run(genCode, "test");
    } 
    else{ 
      cout << "Generating " << fileType << "dataset..." << endl;
      getKine_GG_XX_semiLep_run(genCode, fileType);
    }
  }
  else if (process == "getKine_GG_XX_WW_had"){
    if (fileType == "both"){
      cout << "Generating train dataset..." << endl;
      getKine_GG_XX_WW_had_run(genCode, "train");
      cout << "Generating test dataset..." << endl;
      getKine_GG_XX_WW_had_run(genCode, "test");
    } 
    else{ 
      cout << "Generating " << fileType << "dataset..." << endl;
      getKine_GG_XX_WW_had_run(genCode, fileType);
    }
  }
  else if (process == "getKine_GG_XX_WW_semiLep"){
    if (fileType == "both"){
      cout << "Generating train dataset..." << endl;
      getKine_GG_XX_WW_semiLep_run(genCode, "train");
      cout << "Generating test dataset..." << endl;
      getKine_GG_XX_WW_semiLep_run(genCode, "test");
    } 
    else{ 
      cout << "Generating " << fileType << "dataset..." << endl;
      getKine_GG_XX_WW_semiLep_run(genCode, fileType);
    }
  }
  else if (process == "getKine_GG"){
    if (fileType == "both"){
      cout << "Generating train dataset..." << endl;
      getKine_GG_run(genCode, "train");
      cout << "Generating test dataset..." << endl;
      getKine_GG_run(genCode, "test");
    } 
    else{ 
      cout << "Generating " << fileType << "dataset..." << endl;
      getKine_GG_run(genCode, fileType);
    }
  }
}

///////////////////////////////////////////////////////
//
// Generator!
//
///////////////////////////////////////////////////////

template<typename... Args>
void generator(string p, Args...){
  // Available options are:
  // 1. nEvents:
  //      Number of events to be generated
  //      Default: 1e5
  // 2. maxResonanceM: 
  //      Max. mass of resonance (particle) to be generated. 
  //      The total available event energy is less than 13TeV,
  //      so in the case of pair production, maxResonanceM cannot exceed 6.5TeV.
  //      Default: 3TeV
  // 3. fileType:
  //      Could be set to "", "train", "test", "both"
  //      "": Generates a general file without naming convention
  //      "train" and "test": Generates a file with a particular naming convention
  //      "both": Generates a train and test file.
  //      Default: "both"
  // 4. visObjSortRule:
  //      Sort rule for all visible objects.
  //      Sorting is performed based on groups of object types.
  //      So far, the groups considered are quarks, leptons
  //      "truth": Objects are not sorted and they are accommodated in the generation order.
  //      "pT": Sort groups of objects based on transverse momentum
  //      Default: "pT"
  // 5. outputSortRule:
  //      Sort rule for the output labels
  //      "": Do not perform any sorting
  //      "mag": Orders the outputs by magnitude from smallest to largest
  //      Default: ""
  // 6. inputsType:
  //      Set of inputs to output
  //      0: 4-momentum of visible objects + MET info + event variables outputs
  //      Default: 0
  // 7. genType:
  //      How do we want the generation to be performed
  //      "randGen": Randomly generate resonance masses
  //      Default: "randGen"
  // MISSING
  // SPECIFIC MASS SPLITTING COMBINATIONS
  
  // Start process
  process = p;
  if (!checkProcess()) return;

  // Read the event generation options
  readOptions();
  if (!checkOptions()) return;

  // Start log file
  startLog();
  
  // Search log file for similar process
  pair<int, bool> searchOut = searchLog();
  int genCode = searchOut.first;
  bool match = searchOut.second;

  // Generate event
  generateEvent(process, genCode); 

  // Update log file
  if (!match) updateLog(genCode);
  
  // End of generation
  cout << "----- END OF GENERATION -----" << endl;
  cout << "Thank for using our ultra fast kinematic generator" << endl;
  cout << "Best," << endl;
  cout << "Shion Chen and Luis Felipe Gutierrez" << endl;
}
