# Event plotter

Package used for plotting various variables from datasets in the framework.

## Plotting

#### Directly running programm
```
python3 plotter.py [options]
```
See below for list of arguments and options

#### Running shell script
```
./generatePlots.sh
```

#### Arguments

##### Allowed arguments

```genCode```
* Kinematic generator dataset for plotting
* Default: None

```regressionModelCode```
* Regression model test dataset for plotting
* Default: None

```regressionTestCode```
* Regression test results dataset for plotting
* Default: None

```classificationModelCode```
* Classification model train dataset for plotting
* Default: None

```classificationTestCode```
* Classification test results dataset for plotting
* Default: None

```cutterCode```
* Event cutter dataset code for plotting
* Default: None

## WARNINGs

* To avoid dataset code confusion, do not delete the ```.log``` file which keeps track of the datasets created.
