import sys
import os
import math
import numpy as np
import pandas as pd
from  matplotlib import pyplot as plt
from matplotlib import colors as mpColors
import atlas_mpl_style as ampl
ampl.use_atlas_style()

#################################################
#
# Arguments
#
#################################################
genCode = None
regressionModelCode = None
regressionTestCode = None
regressionTruthVals = []
classificationModelCode = None
classificationTestCode = None
cutterCode = None

def saveArgs(**kwargs):
  for a, v in kwargs.items():
    if a == "genCode":
      try:
        global genCode
        genCode = int(v)
      except:
        pass
    elif a == "regressionModelCode":
      try:
        global regressionModelCode
        regressionModelCode = int(v)
      except:
        pass
    elif a == "regressionTestCode":
      try:
        global regressionTestCode
        regressionTestCode = int(v)
      except:
        pass
    elif a == "regressionTruthVals":
      try:
        global regressionTruthVals
        regressionTruthVals = eval(v)
      except:
        pass
    elif a == "classificationModelCode":
      try:
        global classificationModelCode
        classificationModelCode = int(v)
      except:
        pass
    elif a == "classificationTestCode":
      try:
        global classificationTestCode
        classificationTestCode = int(v)
      except:
        pass
    elif a == "cutterCode":
      try:
        global cutterCode
        cutterCode = int(v)
      except:
        pass
    
  return

def readArgs():
  print("----- Event plotter arguments -----")
  print("genCode: "+str(genCode))
  print("regressionModelCode: "+str(regressionModelCode))
  print("regressionTestCode: "+str(regressionTestCode))
  print("regressionTruthVals: "+str(regressionTruthVals))
  print("classificationModelCode: "+str(classificationModelCode))
  print("classificationTestCode: "+str(classificationTestCode))
  print("cutterCode: "+str(cutterCode))
  return

def checkArgs():
  print("----- Event plotter arguments check -----")
  accepted = True

  # Check genCode argument
  if genCode != None and not searchGenCode(genCode):
    print("Please provide a valid genCode...")
    print("See kinematicGenerator log file for accepted datasets...")
    accepted = False

  # Check regressionModelCode argument
  if regressionModelCode != None and not searchRegressionModelCode(regressionModelCode):
    print("Please provide a valid regressionModelCode...")
    print("See regressionModelling log file for accepted datasets...")
    accepted = False
  
  # Check regressionTestCode argument
  if regressionTestCode != None and not searchRegressionTestCode(regressionTestCode):
    print("Please provide a valid regressionTestCode...")
    print("See regressionTesting log file for accepted datasets...")
    accepted = False
  
  # Check classificationModelCode argument
  if classificationModelCode != None and not searchClassificationModelCode(classificationModelCode):
    print("Please provide a valid classificationModelCode...")
    print("See classificationModelling log file for accepted datasets...")
    accepted = False
  
  # Check classificationTestCode argument
  if classificationTestCode != None and not searchClassificationTestCode(classificationTestCode):
    print("Please provide a valid classificationTestCode...")
    print("See classificationTesting log file for accepted datasets...")
    accepted = False
  
  # Check cutterCode argument
  if cutterCode != None and not searchCutterCode(cutterCode):
    print("Please provide a valid cutterCode...")
    print("See eventCutter log file for accepted datasets...")
    accepted = False
  
  # Check only one code per plotting set provided
  codes = [genCode, regressionModelCode, regressionTestCode, classificationModelCode, classificationTestCode, cutterCode]
  noNoneCounter = sum([0 if type(x)==type(None) else 1 for x in codes])
  if noNoneCounter != 1:
    print("Please only provide one code per plotting call...")
    accepted = False

  # Check for regressionTruthVals
  if False in [True if type(x) == np.int64 or type(x)==np.float64 else False for x in list(np.array(regressionTruthVals).flatten())]:
    print("Please provide integer or float regressionTruthVals...")
    accepted = False
  
  # Accepted values
  if accepted:
    print("Provided arguments are OK... Proceeding with next steps")
  else:
    print("Some arguments are not accepted...")
    print("Exiting program without plotting...")
  return accepted


#################################################
#
# Models config
#
#################################################

def searchParticles(particlesCode):
  with open("../models/config", "r") as f:
    particles = []
    lines = f.readlines()
    found = False
    for line in lines:
      if "particlesCode" in line:
        code = int(line.split(":")[1].strip())
        if code == particlesCode:
          found = True
      elif found:
        particles = eval(line.split(":")[1].strip())
        return particles

def searchParticleVars(particleVarsCode):
  with open("../models/config", "r") as f:
    particleVars = []
    lines = f.readlines()
    found = False
    for line in lines:
      if "particleVarsCode" in line:
        code = int(line.split(":")[1].strip())
        if code == particleVarsCode:
          found = True
      elif found:
        particleVars = eval(line.split(":")[1].strip())
        return particleVars


#################################################
#
# Kinematic generator log
#
#################################################

def searchGenCode(genCode):
  with open("../kinematicGenerator/generator.log","r") as f:
    lines = f.readlines()
    for line in lines:
      if "genCode" in line:
        code = int(line.split(":")[1].strip())
        if code == genCode:
          return True
  return False


#################################################
#
# Regression modelling log
#
#################################################

def searchRegressionModelCode(regressionModelCode):
  with open("../models/regressionModelling.log","r") as f:
    lines = f.readlines()
    for line in lines:
      if "modelCode" in line:
        code = int(line.split(":")[1].strip())
        if code == regressionModelCode:
          return True
  return False

def searchRegressionModelParticles(regressionModelCode):
  with open("../models/regressionModelling.log","r") as f:
    lines = f.readlines()
    found = False
    particlesCode = None
    for line in lines:
      if "modelCode" in line:
        code = int(line.split(":")[1].strip())
        if code == regressionModelCode:
          found = True
      elif found:
        if "particlesCode" in line:
          particlesCode = int(line.split(":")[1].strip())
          break
    if particlesCode != None:
      return searchParticles(particlesCode)
    else:
      return None

def searchRegressionModelParticleVars(regressionModelCode):
  with open("../models/regressionModelling.log","r") as f:
    lines = f.readlines()
    found = False
    particleVarsCode = None
    for line in lines:
      if "modelCode" in line:
        code = int(line.split(":")[1].strip())
        if code == regressionModelCode:
          found = True
      elif found:
        if "particleVarsCode" in line:
          particleVarsCode = int(line.split(":")[1].strip())
          break
    if particleVarsCode != None:
      return searchParticleVars(particleVarsCode)
    else:
      return None


#################################################
#
# Regression testing log
#
#################################################

def searchRegressionTestCode(regressionTestCode):
  with open("../models/regressionTesting.log","r") as f:
    lines = f.readlines()
    for line in lines:
      if "testCode" in line:
        code = int(line.split(":")[1].strip())
        if code == regressionTestCode:
          return True
  return False

def searchRegressionTestDataType(regressionTestCode):
  with open("../models/regressionTesting.log","r") as f:
    lines = f.readlines()
    found = False
    dataType = None
    for line in lines:
      if "testCode" in line:
        code = int(line.split(":")[1].strip())
        if code == regressionTestCode:
          found = True
      elif found:
        if "datasetCode" in line:
          datasetCode = line.split(":")[1].strip()
          datasetsLog = open("../eventExtractor/extractor.log","r")
          found2 = False
          for line2 in datasetsLog.readlines():
            if "datasetCode" in line2:
              datasetCode2 = line2.split(":")[1].strip()
              if datasetCode == datasetCode2:
                found2 = True
            elif found2:
              if "nType" in line2:
                if "Bkg" in line2:
                  return "Bkg"
                elif "Sig" in line2:
                  return "Sig"
    return None

def searchRegressionTestModel(regressionTestCode):
  with open("../models/regressionTesting.log","r") as f:
    lines = f.readlines()
    found = False
    dataType = None
    for line in lines:
      if "testCode" in line:
        code = int(line.split(":")[1].strip())
        if code == regressionTestCode:
          found = True
      elif found:
        if "modelCode" in line:
          model = int(line.split(":")[1].strip())
          return model


#################################################
#
# Classification modelling log
#
#################################################

def searchClassificationModelCode(classificationModelCode):
  with open("../models/classificationModelling.log","r") as f:
    lines = f.readlines()
    for line in lines:
      if "modelCode" in line:
        code = int(line.split(":")[1].strip())
        if code == classificationModelCode:
          return True
  return False

def searchClassificationModelParticles(classificationModelCode):
  with open("../models/classificationModelling.log","r") as f:
    lines = f.readlines()
    found = False
    particlesCode = None
    for line in lines:
      if "modelCode" in line:
        code = int(line.split(":")[1].strip())
        if code == classificationModelCode:
          found = True
      elif found:
        if "particlesCode" in line:
          particlesCode = int(line.split(":")[1].strip())
          break
    if particlesCode != None:
      return searchParticles(particlesCode)
    else:
      return None

def searchClassificationModelParticleVars(classificationModelCode):
  with open("../models/classificationModelling.log","r") as f:
    lines = f.readlines()
    found = False
    particleVarsCode = None
    for line in lines:
      if "modelCode" in line:
        code = int(line.split(":")[1].strip())
        if code == classificationModelCode:
          found = True
      elif found:
        if "particleVarsCode" in line:
          particleVarsCode = int(line.split(":")[1].strip())
          break
    if particleVarsCode != None:
      return searchParticleVars(particleVarsCode)
    else:
      return None

def searchClassificationModelInputsScale(classificationModelCode):
  with open("../models/classificationModelling.log","r") as f:
    lines = f.readlines()
    found = False
    for line in lines:
      if "modelCode" in line:
        code = int(line.split(":")[1].strip())
        if code == classificationModelCode:
          found = True
      elif found:
        if "inputsScale" in line:
          inputsScale = line.split(":")[1].strip()
          return inputsScale
    return None


#################################################
#
# Classification testing log
#
#################################################

def searchClassificationTestCode(classificationTestCode):
  with open("../models/classificationTesting.log","r") as f:
    lines = f.readlines()
    for line in lines:
      if "testCode" in line:
        code = int(line.split(":")[1].strip())
        if code == classificationTestCode:
          return True
  return False

def searchClassificationTestModel(classificationTestCode):
  with open("../models/classificationTesting.log","r") as f:
    lines = f.readlines()
    found = False
    dataType = None
    for line in lines:
      if "testCode" in line:
        code = int(line.split(":")[1].strip())
        if code == classificationTestCode:
          found = True
      elif found:
        if "modelCode" in line:
          model = int(line.split(":")[1].strip())
          return model


#################################################
#
# Event cutter log
#
#################################################

def searchCutterCode(cutterCode):
  with open("../eventCutter/cutter.log","r") as f:
    lines = f.readlines()
    for line in lines:
      if "cutCode" in line:
        code = int(line.split(":")[1].strip())
        if code == cutterCode:
          return True
  return False



#################################################
#
# Log
#
#################################################

def startLog():
  print("----- Start event plotter log file -----")
  
  # If log file does not exist, create one
  if not os.path.isfile("./plotter.log"):
    f = open('./plotter.log', 'w')
    f.close()
    print("Log file does not exist. Creating new log file...")
  else:
    print("Using existing log file...")

def searchLog():
  print("----- Searching event plotter log file -----")

  # Check existing entries
  f = open('./plotter.log', 'r')
  # Variables
  plotCode = -1
  genCodeMatch = False
  regressionModelCodeMatch = False
  regressionTestCodeMatch = False
  regressionTruthValsMatch = False
  classificationModelCodeMatch = False
  classificationTestCodeMatch = False
  cutterCodeMatch = False
  # Read file
  lines = f.readlines()
  # Dissect lines
  for line in lines:
    code,value = line.replace(" ", "").replace("\n", "").split(":")
    # Check plotCode
    if code == "plotCode":
      # First entry
      if plotCode == -1:
        plotCode = int(value)
      # Followng entries:
      else:
        # Check if previous codes matches options
        match1 = genCode != None and genCodeMatch
        match2 = regressionModelCode != None and regressionModelCodeMatch and regressionTruthValsMatch
        match3 = regressionTestCode != None and regressionTestCodeMatch and regressionTruthValsMatch
        match4 = classificationModelCode != None and classificationModelCodeMatch
        match5 = classificationTestCode != None and classificationTestCodeMatch
        match6 = cutterCode != None and cutterCodeMatch
        match = match1 or match2 or match3 or match4 or match5 or match6
        if match:
          break
        # If previous codes do not match options, update plotCode and reset bools
        else:
          plotCode = int(value)
          genCodeMatch = False
          regressionModelCodeMatch = False
          regressionTestCodeMatch = False
          classificationModelCodeMatch = False
          classificationTestCodeMatch = False
          cutterCodeMatch = False
    # Check genCode
    elif code == "genCode":
      if genCode == int(value): genCodeMatch = True
    # Check regressionModelCode
    elif code == "regressionModelCode":
      if regressionModelCode == int(value): regressionModelCodeMatch = True
    # Check regressionTestCode
    elif code == "regressionTestCode":
      if regressionTestCode == int(value): regressionTestCodeMatch = True
    # Check regressionTruthVals
    elif code == "regressionTruthVals":
      if regressionTruthVals == eval(value): regressionTruthValsMatch = True
    # Check classificationModelCode
    elif code == "classificationModelCode":
      if classificationModelCode == int(value): classificationModelCodeMatch = True
    # Check classificationTestCode
    elif code == "classificationTestCode":
      if classificationTestCode == int(value): classificationTestCodeMatch = True
    # Check cutterCode
    elif code == "cutterCode":
      if cutterCode == int(value): cutterCodeMatch = True
  # Close file
  f.close()
  # Update match in case it is the last entry
  match1 = genCode != None and genCodeMatch
  match2 = regressionModelCode != None and regressionModelCodeMatch and regressionTruthValsMatch
  match3 = regressionTestCode != None and regressionTestCodeMatch and regressionTruthValsMatch
  match4 = classificationModelCode != None and classificationModelCodeMatch
  match5 = classificationTestCode != None and classificationTestCodeMatch
  match6 = cutterCode != None and cutterCodeMatch
  match = match1 or match2 or match3 or match4 or match5 or match6
  # Check the matching plotCode, otherwise create a new one
  if plotCode == -1:
    # Create new entry
    plotCode = 0
    match = False
  else:
    # If it does not match, generate a new mergeCode
    if not match: plotCode += 1

  # Logging
  if match:
    print("Found plots with exact same conditions...")
    print("Using plotCode: "+str(plotCode))
  else:
    print("No plots with the same conditions found...")
    print("New plotCode: "+str(plotCode))

  # Return output
  return plotCode, match

def updateLog():
  print("----- Update event plotter log file -----")
  f = open("./plotter.log", "a")
  f.write("plotCode: "+str(plotCode)+"\n")
  if genCode != None:
    f.write("  genCode: "+str(genCode)+"\n")
  elif regressionModelCode != None:
    f.write("  regressionModelCode: "+str(regressionModelCode)+"\n")
    f.write("  regressionTruthVals: "+str(regressionTruthVals)+"\n")
  elif regressionTestCode != None:
    f.write("  regressionTestCode: "+str(regressionTestCode)+"\n")
    f.write("  regressionTruthVals: "+str(regressionTruthVals)+"\n")
  elif classificationModelCode != None:
    f.write("  classificationModelCode: "+str(classificationModelCode)+"\n")
  elif classificationTestCode != None:
    f.write("  classificationTestCode: "+str(classificationTestCode)+"\n")
  elif cutterCode != None:
    f.write("  cutterCode: "+str(cutterCode)+"\n")
  f.close()
  print("Added entry for plotCode="+str(plotCode))


#################################################
#
# Event plotter
#
#################################################

def plotter():
  print("----- Plotting events -----")

  # Create output directory
  currentDirs = os.listdir("./")
  if "plots" not in currentDirs:
    os.system("mkdir plots")

  # Kinematic generator plots
  if genCode != None:
    print("Plotting genCode{}...".format(genCode))

    # Get dataframe
    df = None
    try:
      df = pd.read_csv("../kinematicGenerator/data/genCode"+str(genCode)+"train.csv")
    except:
      pass
    if type(df) == type(None):
      print("No \"train\" dataset found...")
      return

    # Gather mass splittings
    massSplits = [x for x in df.columns if x in ['dM1_1', 'dM2_1']]
    
    # Gather resonances
    resonances = [x for x in df.columns if x in ['mG', 'mX1', 'mN1']]

    # Execute plots
    if len(massSplits) == 1:
      plotMassesGrid(df, resonances)
      plotMassSplit(df)
      plotGenCodeVars(df,"Pt")
      plotGenCodeVars(df,"Eta")
      plotGenCodeVars(df,"Phi")
      plotGenCodeVars(df,"E")
    else: 
      plotMassesGrid(df, resonances)
      plotMassSplitGrid(df, massSplits)
      plotGenCodeVars(df,"Pt")
      plotGenCodeVars(df,"Eta")
      plotGenCodeVars(df,"Phi")
      plotGenCodeVars(df,"E")
      #plotCorrelation(df,"metPt", "dM1_1")
      #plotCorrelation(df,"metPt", "dM2_1")
      #plotCorrelation2(df,"dM1_1", "dM2_1", "metPt")

  # Regression model plots
  elif regressionModelCode != None or regressionTestCode != None:

    # Print step
    if regressionModelCode != None:
      print("Plotting regression modelCode{}...".format(regressionModelCode))
    else:
      print("Plotting regression testCode{}...".format(regressionTestCode))
  
    # Get dataframe
    df = None
    if regressionModelCode != None:
      fName = "regressionModelCode{}Test.csv".format(regressionModelCode)
    else:
      fName = "regressionTestCode{}.csv".format(regressionTestCode)
    try:
      df = pd.read_csv("../models/datasets/{}".format(fName))
    except:
      pass
    if type(df) == type(None):
      print("No dataset found...")
      return
  
    # Get mass splittings
    massSplits = [x for x in df.columns if x in ['dM1_1', 'dM2_1']]

    # Get model code 
    modelCode = regressionModelCode
    if regressionTestCode != None:
      modelCode = searchRegressionTestModel(regressionTestCode)

    # Get dataType if available
    dataType = None
    if regressionTestCode != None:
      dataType = searchRegressionTestDataType(regressionTestCode)
    
    # Execute plots
    if len(massSplits) == 1:
      plotMeff(df, massSplits, modelCode, regressionTestCode, regressionTruthVals)
      plotOneDimRegressionOut(df, massSplits, modelCode, regressionTestCode, regressionTruthVals)
    else:
      plotTwoDimRegressionOut(df, massSplits, modelCode, regressionTestCode, dataType, regressionTruthVals)
      plotTwoDimRegressionOutHeat(df, massSplits, modelCode, regressionTestCode, dataType, regressionTruthVals)
      plotTwoDimRegressionOutProj(df, massSplits, modelCode, regressionTestCode, dataType, regressionTruthVals)
      plotRegressionVars(df, modelCode, regressionTestCode)
      if dataType == "Bkg":
        bkgs = set(list(df["comment"]))
        for bkg in bkgs:
          bkgStr = bkg.split("_")[0]
          plotTwoDimRegressionOutHeat(df[df["comment"]==bkg], massSplits, modelCode, regressionTestCode, dataType, regressionTruthVals, bkgStr)
          plotTwoDimRegressionOutProj(df[df["comment"]==bkg], massSplits, modelCode, regressionTestCode, dataType, regressionTruthVals, bkgStr)
          plotRegressionVars(df[df["comment"]==bkg], modelCode, regressionTestCode, bkgStr)

  # Classification model plots
  elif classificationModelCode != None or classificationTestCode != None:

    # Print step
    if classificationModelCode != None:
      print("Plotting classification modelCode{}...".format(classificationModelCode)) 
    else:
      print("Plotting classification testCode{}...".format(classificationTestCode)) 

    # Get dataframe
    df = None
    if classificationModelCode != None:
      fName = "classificationModelCode{}Test.csv".format(classificationModelCode)
    else:
      fName = "classificationTestCode{}.csv".format(classificationTestCode)
    try:
      df = pd.read_csv("../models/datasets/{}".format(fName))
    except:
      pass
    if type(df) == type(None):
      print("No dataset found...")
      return

    # Get classifier score
    scoreName = "NNScore"
   
    # Get model code
    modelCode = classificationModelCode
    if classificationTestCode != None:
      modelCode = searchClassificationTestModel(classificationTestCode)

    # Get inputs scale
    inputsScale = searchClassificationModelInputsScale(modelCode)

    # Execute plots
    plotClassificationPerformance(df, scoreName, modelCode)
    plotClassificationVars(df[df["signal"]==1], inputsScale, modelCode, classificationTestCode, "Signal")
    plotClassificationVars(df[df["signal"]==0], inputsScale, modelCode, classificationTestCode, "Background")
    plotClassificationOneMassDependency(df, "Predicted-dM1_1", modelCode)
    plotClassificationOneMassDependency(df, "Predicted-dM2_1", modelCode)
    plotClassificationTwoMassDependency(df, modelCode)
    bkgs = set(list(df[df["signal"]==0]["comment"]))
    for bkg in bkgs:
      bkgStr = bkg.split("_")[0]
      plotClassificationVars(df[df["signal"]==0], inputsScale, modelCode, classificationTestCode, "Background "+bkgStr)

  # Event cutter plots
  elif cutterCode != None:
    
    # Print step
    print("Plotting event cutter code{}...".format(cutterCode))

    # Get dataframe
    df = None
    try:
      df = pd.read_csv("../eventCutter/datasets/cutCode{}.csv".format(cutterCode))
    except:
      pass
    if type(df) == type(None):
      print("No dataset found...")

    # Execute plots
    plotCutterNNScoreMassDependency(df, "Predicted-dM1_1", cutterCode)
    plotCutterNNScoreMassDependency(df, "Predicted-dM2_1", cutterCode)
  
  # End of plotter function 
  return      

def plotMassesGrid(df, resonances):
  print("Plotting masses grid...")
  
  # Get bins
  bins = np.linspace(0,3000, 61)
  
  # Get x,y values
  x,y = np.meshgrid(bins,bins)

  # Get z values to plot
  zs = []
  combs = []
  for m1 in range(len(resonances)):
    for m2 in range(len(resonances)):
      if m1 < m2:
        # Resonances
        resX = resonances[m1]
        resY = resonances[m2]
        # Get z for combination of resonances
        z = [[0 for i in range(len(bins)-1)] for j in range(len(bins)-1)]
        for j in range(len(bins)-1):   # y loop
          for i in range(len(bins)-1): # x loop
            # Get x limits
            xLow = bins[i]
            xUp = bins[i+1]
            # Get y limits
            yLow = bins[j]
            yUp = bins[j+1]
            # Get dataframe
            desDF = df[(df[resX] >=xLow) & (df[resX] < xUp) & (df[resY] >= yLow) & (df[resY] < yUp)]
            # Save number of entries
            z[j][i] = len(desDF.index)
        zs.append(z)
        combs.append([resX,resY])
  
  # Make a plot for each combination of masses
  for i in range(len(combs)):
    
    # Start figure
    plt.close()
    plt.figure()

    # Plot
    plt.pcolormesh(x,y,zs[i], cmap='afmhot_r', norm=mpColors.LogNorm(vmin=np.amin(z)+0.1, vmax=np.amax(z)))
    plt.colorbar(label="Entries")
    
    # Format x and y axis
    plt.xlim([0,3000])
    plt.ylim([0,3000])
    labelX = ""
    labelY = ""
    if combs[i][0] == "mG":
      labelX = r"$m(\tilde{g})$ [GeV]"
    elif combs[i][0] == "mX1":
      labelX = r"$m(\tilde{\chi^{\pm}}_1)$ [GeV]"
    elif combs[i][0] == "mN1":
      labelX = r"$m(\tilde{\chi}_0)$ [GeV]"
    if combs[i][1] == "mG":
      labelY = r"$m(\tilde{g})$ [GeV]"
    elif combs[i][1] == "mX1":
      labelY = r"$m(\tilde{\chi^{\pm}}_1)$ [GeV]"
    elif combs[i][1] == "mN1":
      labelY = r"$m(\tilde{\chi}_0)$ [GeV]"
    plt.xlabel(labelX)
    plt.ylabel(labelY)
    
    # Add labels
    plt.text(0.05,0.9, "ATLAS", fontstyle='italic', fontweight='bold', transform=plt.gca().transAxes)
    plt.text(0.195, 0.9, "Work In Progress", transform=plt.gca().transAxes)
    plt.text(0.05,0.84, "Generation code "+str(genCode), transform=plt.gca().transAxes)

    # Save plot
    formats = ["png", "pdf"]
    name = "MassesGrid_{}-{}".format(combs[i][0], combs[i][1])
    for f in formats:
      plt.savefig("./plots/plotCode{}".format(plotCode)+name+"."+f, transparent=True, dpi=200)
  
  # End of function
  return

def plotMassSplit(df):
  print("Plotting mass splitting...")
  
  # Get bins
  bins = 60
  
  # Get values
  vals = list(df['dM1_1'])
  
  # Start figure
  plt.close()
  plt.figure()
  
  # Plot
  plt.hist(vals,bins=60, color='black', linewidth=2, histtype='step')
    
  # Format x and y axis
  plt.xlim([0,3000])
  labelX = r"$\Delta m$ [GeV]"
  labelY = "Entries"
  plt.xlabel(labelX)
  plt.ylabel(labelY)
    
  # Add labels
  plt.text(0.55,0.9, "ATLAS", fontstyle='italic', fontweight='bold', transform=plt.gca().transAxes)
  plt.text(0.665, 0.9, "Work In Progress", transform=plt.gca().transAxes)
  plt.text(0.55,0.84, "Generation code "+str(genCode), transform=plt.gca().transAxes)

  # Save plot
  formats = ["png", "pdf"]
  name = "MassSplit"
  for f in formats:
    plt.savefig("./plots/plotCode{}".format(plotCode)+name+"."+f, transparent=True, dpi=200)
  
  # End of function
  return
    
def plotMassSplitGrid(df, massSplits):
  print("Plotting mass splittings grid...")
  
  # Get bins
  bins = np.linspace(0,3000, 61)
  
  # Get x,y values
  x,y = np.meshgrid(bins,bins)

  # Get z values to plot
  zs = []
  combs = []
  for ms1 in range(len(massSplits)):
    for ms2 in range(len(massSplits)):
      if ms1 < ms2:
        # Mass splits
        msX = massSplits[ms1]
        msY = massSplits[ms2]
        # Get z for combination of mass splittings 
        z = [[0 for i in range(len(bins)-1)] for j in range(len(bins)-1)]
        for j in range(len(bins)-1):   # y loop
          for i in range(len(bins)-1): # x loop
            # Get x limits
            xLow = bins[i]
            xUp = bins[i+1]
            # Get y limits
            yLow = bins[j]
            yUp = bins[j+1]
            # Get dataframe
            desDF = df[(df[msX] >=xLow) & (df[msX] < xUp) & (df[msY] >= yLow) & (df[msY] < yUp)]
            # Save results
            z[j][i] = len(desDF.index)
        zs.append(z)
        combs.append([msX, msY])
  
  # Make a plot for each combination of mass splits
  for i in range(len(combs)):
    
    # Start figure
    plt.close()
    plt.figure()
  
    # Plot
    plt.pcolormesh(x,y,zs[i], cmap='afmhot_r', norm=mpColors.LogNorm(vmin=np.amin(z)+0.1, vmax=np.amax(z)))
    plt.colorbar(label="Entries")
    
    # Format x and y axis
    plt.xlim([0,3000])
    plt.ylim([0,3000])
    labelX = ""
    labelY = ""
    if "dM1" in combs[i][0]:
      labelX = r"$\Delta m_1$ [GeV]"
    elif "dM2" in combs[i][0]:
      labelX = r"$\Delta m_2$ [GeV]"
    if "dM1" in combs[i][1]:
      labelY = r"$\Delta m_1$ [GeV]"
    elif "dM2" in combs[i][1]:
      labelY = r"$\Delta m_2$ [GeV]"
    plt.xlabel(labelX)
    plt.ylabel(labelY)
    
    # Add labels
    plt.text(0.5,0.9, "ATLAS", fontstyle='italic', fontweight='bold', transform=plt.gca().transAxes)
    plt.text(0.645, 0.9, "Work In Progress", transform=plt.gca().transAxes)
    plt.text(0.5,0.84, "Generation code "+str(genCode), transform=plt.gca().transAxes)

    # Save plot
    formats = ["png", "pdf"]
    name = "MassSplitGrid_{}-{}".format(combs[i][0], combs[i][1])
    for f in formats:
      plt.savefig("./plots/plotCode{}".format(plotCode)+name+"."+f, transparent=True, dpi=200)
  
  # End of function
  return
  
def plotGenCodeVars(df, varName):
  print("Plotting variable {}...".format(varName))

  # Get desired variables
  cols = []
  for var in df.columns:
    if varName != "E" and varName in var:
      cols.append(var)
    elif varName == "E" and varName == var[-1]:
      cols.append(var)
  
  # Particles with given variable
  particles = []
  for c in cols:
    part = c.replace(varName,"")
    particles.append(part)
  
  # Get desired values
  vals = []
  mins = []
  maxs = []
  for c in cols:
    val = np.array(df[c])
    mins.append(val.min())
    maxs.append(val.max())
    vals.append(val)
  xMin = min(mins)
  xMax = max(maxs)

  # Get x axis bins
  bins = np.linspace(xMin, xMax, 50)
 
  # General plotting configs
  colors = ['black', 'red', 'blue', 'green', 'orange', 'cyan', 'magenta', 'lime', 'grey']
 
  # Start fresh plot
  plt.close()
  plt.figure()
  
  # Plot each particles values
  for i in range(len(particles)):
    plt.hist(vals[i], bins=bins, linewidth=2, histtype='step', color=colors[i], label=particles[i])
  
  # Format x and y axis
  xCushion = 1
  yCushion = 1
  if varName in ["Eta", "Phi"]:
    xCushion = 1.1
    if varName == "Phi":
      yCushion = 1.5
  plt.xlim([xMin*xCushion, xMax*xCushion])
  if varName in ["Pt", "Eta", "E"]:
    plt.yscale("log")
  else:
    yMin, yMax = plt.ylim()
    plt.ylim([yMin, yMax*yCushion])
    plt.ticklabel_format(axis='y', style='sci', scilimits=(-2,3))
  if varName == "Pt":
    plt.xlabel(r"$p_T$ [GeV]")
  elif varName == "Eta":
    plt.xlabel(r"$\eta$")
  elif varName == "Phi":
    plt.xlabel(r"$\phi$")
  elif varName == "E":
    plt.xlabel(r"Energy [GeV]")
  plt.ylabel("Unweighted entries")

  # Add labels
  xShiftLabel = 0
  if varName == "Pt":
    xShiftLabel = 0.5
  elif varName == "Eta":
    xShiftLabel = 0
  elif varName == "Phi":
    xShiftLabel = 0
  elif varName == "E":
    xShiftLabel = 0.5
  plt.text(0.05+xShiftLabel,0.9, "ATLAS", fontstyle='italic', fontweight='bold', transform=plt.gca().transAxes)
  plt.text(0.165+xShiftLabel,0.9, "Work In Progress", transform=plt.gca().transAxes)
  plt.text(0.05+xShiftLabel,0.84, "Generation code "+str(genCode), transform=plt.gca().transAxes)
      
  # Add legend
  xShiftLeg = 0
  yShiftLeg = 0
  if varName == "Pt":
    xShiftLeg = 0.5
    yShiftLeg = -0.12
  elif varName == "Eta":
    xShiftLeg = 0
    yShiftLeg = -0.12
  elif varName == "Phi":
    xShiftLeg = 0.5
    yShiftLeg = 0
  elif varName == "E":
    xShiftLeg = 0.5
    yShiftLeg = -0.12
  totalyShiftLeg = yShiftLeg - (0.0475*round(len(particles)/2+0.1) + 0.0125) # equation for legend shift is 0.0475*rows+0.0125
  plt.legend(loc=(0.04+xShiftLeg, 0.9+totalyShiftLeg), ncols=2, frameon=False)
   
  # Save plot
  formats = ["png", "pdf"]
  for f in formats:
    plt.savefig("./plots/plotCode{}".format(plotCode)+varName+"."+f, transparent=True, dpi=200)
   
  # End of plotGenCodeVars
  return 

def plotCorrelation(df, var1, var2):
  print("Plotting correlation: {} vs. {}...".format(var1, var2))
  
  # Get desired values
  var1Vals = np.array(df[var1])
  xMin = var1Vals.min()
  xMax = var1Vals.max()
  var2Vals = np.array(df[var2])
  yMin = var2Vals.min()
  yMax = var2Vals.max()
 
  # Get bins
  xBins = np.linspace(xMin, xMax, 51)
  yBins = np.linspace(yMin, yMax, 51)

  # Get x,y values
  x,y = np.meshgrid(xBins,yBins)

  # Get z values to plot
  z = [[0 for i in range(len(xBins)-1)] for j in range(len(yBins)-1)]
  for j in range(len(yBins)-1):   # y loop
    for i in range(len(xBins)-1): # x loop
      # Get x limits
      xLow = xBins[i]
      xUp = xBins[i+1]
      # Get y limits
      yLow = yBins[j]
      yUp = yBins[j+1]
      # Get dataframes
      desDF = df[(df[var1] >=xLow) & (df[var1] < xUp) & (df[var2] >= yLow) & (df[var2] < yUp)]
      z[j][i] = len(desDF.index)
  
  # Start figure
  plt.close()
  plt.figure()

  # Plot
  plt.pcolormesh(x,y,z, cmap='afmhot_r', norm=mpColors.LogNorm(vmin=np.amin(z)+0.1, vmax=np.amax(z)))
  plt.colorbar()
    
  # Format x and y axis
  plt.xlim([xMin,xMax])
  plt.ylim([yMin,yMax])
  if var1 == "metPt":
    plt.xlabel(r"$p^{miss}_T$ [GeV]")
  if var2 == "dM1_1":
    plt.ylabel(r"$\Delta m_1$ [GeV]")
  elif var2 == "dM2_1":
    plt.ylabel(r"$\Delta m_2$ [GeV]")
    
  # Add labels
  plt.text(0.05,0.9, "ATLAS", fontstyle='italic', fontweight='bold', transform=plt.gca().transAxes)
  plt.text(0.195, 0.9, "Work In Progress", transform=plt.gca().transAxes)
  plt.text(0.05,0.84, "Generation code "+str(genCode), transform=plt.gca().transAxes)
  plt.text(0.05,0.78, "Number of entries", transform=plt.gca().transAxes)

  # Save plot
  formats = ["png", "pdf"]
  name = "Corr-{}-{}".format(var1,var2)
  for f in formats:
    plt.savefig("./plots/plotCode{}".format(plotCode)+name+"."+f, transparent=True, dpi=200)
  
  # End of function
  return
    
def plotCorrelation2(df, var1, var2, var3):
  print("Plotting correlation: {} in {} vs. {}...".format(var3, var1, var2))
  
  # Get desired values
  var1Vals = np.array(df[var1])
  if "dM" in var1:
    xMin = 0
    xMax = 3000
  else:  
    xMin = var1Vals.min()
    xMax = var1Vals.max()
  var2Vals = np.array(df[var2])
  if "dM" in var2:
    yMin = 0
    yMax = 3000
  else:  
    yMin = var2Vals.min()
    yMax = var2Vals.max()
 
  # Get bins
  xBins = np.linspace(xMin, xMax, 51)
  yBins = np.linspace(yMin, yMax, 51)

  # Get x,y values
  x,y = np.meshgrid(xBins,yBins)

  # Get z values to plot
  z = [[0 for i in range(len(xBins)-1)] for j in range(len(yBins)-1)]
  for j in range(len(yBins)-1):   # y loop
    for i in range(len(xBins)-1): # x loop
      # Get x limits
      xLow = xBins[i]
      xUp = xBins[i+1]
      # Get y limits
      yLow = yBins[j]
      yUp = yBins[j+1]
      # Get dataframes
      desDF = df[(df[var1] >=xLow) & (df[var1] < xUp) & (df[var2] >= yLow) & (df[var2] < yUp)]
      if len(desDF.index) > 0:
        z[j][i] = desDF[var3].mean()
  
  # Start figure
  plt.close()
  plt.figure()

  # Plot
  plt.pcolormesh(x,y,z, cmap='afmhot_r')#, norm=mpColors.LogNorm(vmin=np.amin(z)+0.1, vmax=np.amax(z)))
  if var3 == "metPt":
    plt.colorbar(label=r'Average $p^{miss}_T$ [GeV]')
  else:
    plt.colorbar()
    
  # Format x and y axis
  plt.xlim([xMin,xMax])
  plt.ylim([yMin,yMax])
  if var1 == "dM1_1":
    plt.xlabel(r"$\Delta m_1$ [GeV]")
  elif var1 == "dM2_1":
    plt.xlabel(r"$\Delta m_2$ [GeV]")
  if var2 == "dM1_1":
    plt.ylabel(r"$\Delta m_1$ [GeV]")
  elif var2 == "dM2_1":
    plt.ylabel(r"$\Delta m_2$ [GeV]")
    
  # Add labels
  plt.text(0.5,0.9, "ATLAS", fontstyle='italic', fontweight='bold', transform=plt.gca().transAxes)
  plt.text(0.645, 0.9, "Work In Progress", transform=plt.gca().transAxes)
  plt.text(0.5,0.84, "Generation code "+str(genCode), transform=plt.gca().transAxes)

  # Save plot
  formats = ["png", "pdf"]
  name = "Corr-{}-{}-{}".format(var1,var2,var3)
  for f in formats:
    plt.savefig("./plots/plotCode{}".format(plotCode)+name+"."+f, transparent=True, dpi=200)
  
  # End of function
  return

def plotMeff(df, outputs, modelCode, testCode, desiredVals=[]):
  print("Plotting meff...")
  
  # General parameters
  spacing = 50
  bins = np.linspace(0, 3000, 61)
  colors = ["black", "red", "blue", "green", "orange"]

  # Get desired DFs
  desDFs = []
  for out in outputs:
    outDFs = []
    for i in range(len(desiredVals)):
      desVal = desiredVals[i]
      outDF = df[(df[out] >= desVal-spacing) & (df[out] < desVal+spacing)]
      outDFs.append(outDF)
    desDFs.append(outDFs)
  
  # Get desired columns
  cols = []
  for col in df.columns:
    if "Pt" in col:
      cols.append(col)

  # Plot meff plot for each output
  for o in range(len(outputs)):

    # Get meff
    meffs = []
    for i in range(len(desiredVals)): 
      meff = None
      for col in cols:
        if type(meff) == type(None):
          meff = desDFs[o][i][col]
      else:
        meff += desDFs[o][i][col]
      meff = np.array(meff)
      meffs.append(meff)

    # Start fresh plot
    plt.close()
    plt.figure()

    # Plot meffs and references
    for i in range(len(desiredVals)):
      label = r"$\Delta m={}\pm{}$".format(desiredVals[i],spacing)
      plt.hist(meffs[i], bins=bins[:-1], histtype='step', color=colors[i], linewidth=2, label=label)
      plt.axvline(x=desiredVals[i], linestyle='dashed', color=colors[i], linewidth=1)
      plt.axvline(x=desiredVals[i]-spacing, linestyle='dotted', color=colors[i], linewidth=1)
      plt.axvline(x=desiredVals[i]+spacing, linestyle='dotted', color=colors[i], linewidth=1)
  
    # Format x and y axis
    plt.xlabel(r"$m_{eff}/2$ [GeV]")
    plt.ylabel(r"Entries")
    plt.xlim([0,3000])
        
    # Add labels and legend
    plt.text(0.55,0.9, "ATLAS", fontstyle='italic', fontweight='bold', transform=plt.gca().transAxes)
    plt.text(0.665,0.9, "Work In Progress", transform=plt.gca().transAxes)
    regStr = "Regression Model {}".format(modelCode)
    if testCode != None:
      regStr += ", Test {}".format(testCode)
    plt.text(0.55,0.84, regStr, transform=plt.gca().transAxes)
    proxyStr = r"Proxy for "
    if out in ["dM1_1", "dM1_2"]:
      proxyStr += "$\Delta m_1$"
    elif out in ["dM2_1", "dM2_2"]:
      proxyStr += "$\Delta m_2$"
    plt.text(0.55,0.78, proxyStr, transform=plt.gca().transAxes)
    plt.legend(loc=(0.54, 0.9 - 0.18 - (0.0475*len(desiredVals) + 0.0125)), frameon=False)

    # Save plot
    name = "Meff-"+out
    formats = ["png", "pdf"]
    for f in formats:
      plt.savefig("./plots/plotCode{}".format(plotCode)+name+"."+f, transparent=True, dpi=200)
  
  # End of function
  return
    
def plotOneDimRegressionOut(df, outputs, modelCode, testCode, desiredVals):
  print("Plotting regression output...")
  
  # General parameters
  spacing = 50
  bins = np.linspace(0, 3000, 61)
  colors = ["black", "red", "blue", "green", "orange"]

  # Get desired DFs
  desDFs = []
  for out in outputs:
    outDFs = []
    for i in range(len(desiredVals)):
      desVal = desiredVals[i]
      outDF = df[(df[out] >= desVal-spacing) & (df[out] < desVal+spacing)]
      outDFs.append(outDF)
    desDFs.append(outDFs)
  
  # Get predicted dMs
  predDMs = []
  for o in range(len(outputs)):
    outPredDMs = []
    for i in range(len(desiredVals)): 
      outPredDM = desDFs[o][i]["Predicted-"+outputs[o]]
      outPredDMs.append(outPredDM)
    predDMs.append(outPredDMs)

  # Make one plot for each output
  for o in range(len(outputs)):

    # Start fresh plot
    plt.close()
    plt.figure()

    # Plot predicted dMs and references
    for i in range(len(desiredVals)):
      label = r"$\Delta m={}\pm{}$".format(desiredVals[i],spacing)
      plt.hist(predDMs[o][i], bins=bins[:-1], histtype='step', color=colors[i], linewidth=2, label=label)
      plt.axvline(x=desiredVals[i], linestyle='dashed', color=colors[i], linewidth=1)
      plt.axvline(x=desiredVals[i]-spacing, linestyle='dotted', color=colors[i], linewidth=1)
      plt.axvline(x=desiredVals[i]+spacing, linestyle='dotted', color=colors[i], linewidth=1)
  
    # Format x and y axis
    xlabel = r"Regression Predicted "
    if outputs[o] in ["dM1_1", "dM1_2"]:
      xlabel += "$\Delta m_1$ [GeV]"
    elif outputs[o] in ["dM2_1", "dM2_2"]:
      xlabel += "$\Delta m_2$ [GeV]"
    plt.xlabel(xlabel)
    plt.ylabel(r"Entries")
    plt.xlim([0,3000])
        
    # Add labels and legend
    plt.text(0.55,0.9, "ATLAS", fontstyle='italic', fontweight='bold', transform=plt.gca().transAxes)
    plt.text(0.665,0.9, "Work In Progress", transform=plt.gca().transAxes)
    regStr = "Regression Model {}".format(modelCode)
    if testCode != None:
      regStr += ", Test {}".format(testCode)
    plt.text(0.55,0.84, regStr, transform=plt.gca().transAxes)
    plt.legend(loc=(0.54, 0.9 - 0.12 - (0.0475*len(desiredVals) + 0.0125)), frameon=False)

    # Save plot
    name = "OneDimRegOut-"+outputs[o]
    formats = ["png", "pdf"]
    for f in formats:
      plt.savefig("./plots/plotCode{}".format(plotCode)+name+"."+f, transparent=True, dpi=200)

  # End of function
  return

def plotTwoDimRegressionOut(df, outputs, modelCode, testCode, dataType=None, desiredVals=[]):
  ''' Plots a pair of mass splittings '''

  # General parameters
  spacing = 50
  bins = np.linspace(0, 3000, 61)
  colors = ["black", "red", "blue", "green", "orange"]
 
  # Go through pairs of outputs
  for i in range(len(outputs)):
    for j in range(len(outputs)):
      if i < j:
  
        # Start fresh plot
        plt.close()
        plt.figure()

        # Loop through desiredVals
        for k in range(len(desiredVals)):

          # Get DF only with truth mass splitting in the range of the desired value
          des1 = desiredVals[k][0]
          des2 = desiredVals[k][1]
          des1Up = desiredVals[k][0]+spacing
          des1Low = desiredVals[k][0]-spacing
          des2Up = desiredVals[k][1]+spacing
          des2Low = desiredVals[k][1]-spacing
          dfOut1 = df[outputs[i]]
          dfOut2 = df[outputs[j]]
          filtDF = df[(dfOut1 >= des1Low) & (dfOut1 < des1Up) & (dfOut2 >= des2Low) & (dfOut2 < des2Up)]
        
          # If there are no entries with a truth mass splitting in the range of the desired value
          # Skip its plotting
          # Else plot them
          if len(filtDF.index) == 0:
            continue
          else:
            x = np.array(filtDF['Predicted-'+outputs[i]])
            y = np.array(filtDF['Predicted-'+outputs[j]])
            plt.scatter(x, y, color=colors[k], alpha=0.2, marker='o', s=2, linestyle="None")
            if dataType != "Bkg":
              t = np.linspace(0, 2*np.pi, 100)
              xTruth = desiredVals[k][i] + spacing*np.sin(t)
              yTruth = desiredVals[k][j] + spacing*np.cos(t)
              label=r"$\Delta m_{}={}\pm{}\,$GeV, $\Delta m_{}={}\pm{}\,$GeV".format(i+1, des1, spacing, j+1, des2, spacing)
              plt.plot(xTruth, yTruth, color=colors[k], linewidth=2, label=label)

        # Format x and y axis
        plt.xlabel(r"Predicted $\Delta m_{}$ [GeV]".format(i+1))
        plt.ylabel(r"Predicted $\Delta m_{}$ [GeV]".format(j+1))
        plt.xlim([0,3000])
        plt.ylim([0,3000])
        
        # Add labels and legend
        xShift = 0.0
        if dataType == "Bkg":
          xShift = 0.5
        plt.text(0.05+xShift,0.9, "ATLAS", fontstyle='italic', fontweight='bold', transform=plt.gca().transAxes)
        plt.text(0.165+xShift,0.9, "Work In Progress", transform=plt.gca().transAxes)
        regStr = "Regression Model {}".format(modelCode)
        if testCode != None:
          regStr += ", Test {}".format(testCode)
        plt.text(0.05+xShift,0.84, regStr, transform=plt.gca().transAxes)
        if dataType == "Bkg":
          plt.text(0.05+xShift,0.78, r"Predicted $\Delta m$s for background", transform=plt.gca().transAxes)
        else:
          plt.text(0.05+xShift,0.78, r"Predicted $\Delta m$s for given truth $\Delta m$s", transform=plt.gca().transAxes)
          plt.legend(loc=(0.04+xShift, 0.9 - 0.18 - (0.0475*len(desiredVals) + 0.0125)), frameon=False)

        # Save plot
        name = "TwoDimRegOut{}VsOut{}".format(i+1,j+1)
        formats = ["png", "pdf"]
        for f in formats:
          plt.savefig("./plots/plotCode{}".format(plotCode)+name+"."+f, transparent=True, dpi=200)
  
  # End of function
  return

def plotTwoDimRegressionOutHeat(df, outputs, modelCode, testCode, dataType=None, desiredVals=[], bkgName=""):
  ''' Plots a pair of mass splittings '''

  # General parameters
  spacing = 50
  bins = np.linspace(0, 3000, 61)
  colors = ["black", "red", "blue", "green", "orange"]
 
  # Go through pairs of outputs
  for i in range(len(outputs)):
    for j in range(len(outputs)):
      if i < j:
  
        # Start fresh plot
        plt.close()
        plt.figure()

        # Loop through desiredVals
        des1s = []
        des1Ups = []
        des1Lows = []
        des2s = []
        des2Ups = []
        des2Lows = []
        filtDFs = []
        for k in range(len(desiredVals)):

          # Get DF only with truth mass splitting in the range of the desired value
          des1 = desiredVals[k][0]
          des2 = desiredVals[k][1]
          des1Up = desiredVals[k][0]+spacing
          des1Low = desiredVals[k][0]-spacing
          des2Up = desiredVals[k][1]+spacing
          des2Low = desiredVals[k][1]-spacing
          dfOut1 = df[outputs[i]]
          dfOut2 = df[outputs[j]]
          filtDF = df[(dfOut1 >= des1Low) & (dfOut1 < des1Up) & (dfOut2 >= des2Low) & (dfOut2 < des2Up)]

          # Save info
          des1s.append(des1)
          des1Ups.append(des1Up)
          des1Lows.append(des1Low)
          des2s.append(des2)
          des2Ups.append(des2Up)
          des2Lows.append(des2Low)
          filtDFs.append(filtDF) 

        # Get x,y mesh
        x,y = np.meshgrid(bins,bins)

        # Get z values
        zsUnweighted = []
        zsWeighted = []
        for fDF  in filtDFs:
          tempZUnweighted = []
          tempZWeighted = []
          for q in range(len(bins)-1):
            zRowUnweighted = []
            zRowWeighted = []
            for p in range(len(bins)-1):
              # Get x limits
              xLow = bins[p]
              xUp = bins[p+1]
              # Get y limits
              yLow = bins[q]
              yUp = bins[q+1]
              # Get desired DF
              dfOut1 = fDF["Predicted-"+outputs[i]]
              dfOut2 = fDF["Predicted-"+outputs[j]]
              desDF = fDF[(dfOut1 >= xLow) & (dfOut1 < xUp) & (dfOut2 >=yLow) & (dfOut2 < yUp)]
              # Save desired values
              zRowUnweighted.append(len(desDF.index))
              if dataType in ["Sig", "Bkg"]:
                zRowWeighted.append(desDF["combWeight"].sum())
            tempZUnweighted.append(zRowUnweighted)
            tempZWeighted.append(zRowWeighted)
          zsUnweighted.append(np.array(tempZUnweighted))
          zsWeighted.append(np.array(tempZWeighted))
        zs = [zsUnweighted, zsWeighted]
        
        # Plot both weighted and unweighted
        for z in range(len(zs)):

          # Plot one at a time
          for k in range(len(zs[z])):

            # Some zs like the weighted one is only defined for signal and background
            # so nothing can be plotted and it will likely raise errors
            if len(zs[z][k].flatten()) == 0:
              break
        
            # Start fresh plot
            plt.close()
            plt.figure()

            # Get form label
            form = ""
            if z == 0:
              form = "Unweighted"
            else:
              form = "Weighted"
    
            # Plot
            plt.pcolormesh(x,y,zs[z][k], cmap='afmhot_r', vmin=0)
            plt.colorbar(label="{} entries".format(form))

            # Plot reference lines
            if dataType != "Bkg":
              plt.axvline(x=des1s[k], color='grey')
              plt.axvline(x=des1Ups[k], color='grey', linestyle='dotted')
              plt.axvline(x=des1Lows[k], color='grey', linestyle='dotted')
              plt.axhline(y=des2s[k], color='grey')
              plt.axhline(y=des2Ups[k], color='grey', linestyle='dotted')
              plt.axhline(y=des2Lows[k], color='grey', linestyle='dotted')

            # Format x and y axis
            plt.xlabel(r"Predicted $\Delta m_{}$ [GeV]".format(i+1))
            plt.ylabel(r"Predicted $\Delta m_{}$ [GeV]".format(j+1))
            plt.xlim([0,3000])
            plt.ylim([0,3000])
        
            # Add labels
            plt.text(0.05,0.9, "ATLAS", fontstyle='italic', fontweight='bold', transform=plt.gca().transAxes)
            plt.text(0.185,0.9, "Work In Progress", transform=plt.gca().transAxes)
            regStr = "Regression Model {}".format(modelCode)
            if testCode != None:
              regStr += ", Test {}".format(testCode)
            plt.text(0.05,0.84, regStr, transform=plt.gca().transAxes)
            if dataType != "Bkg":
              label = r"$\Delta m_{}={}\pm{}\,$GeV, $\Delta m_{}={}\pm{}\,$GeV".format(i+1, des1s[k], spacing, j+1, des2s[k], spacing)
            else:
              label = r"Background {}".format(bkgName)
            plt.text(0.05, 0.78, label, transform=plt.gca().transAxes)

            # Save plot
            if dataType != "Bkg":
              name = "TwoDimRegOut{}VsOut{}Val{}Val{}Heat{}".format(i+1,j+1, des1s[k], des2s[k], form)
            else:
              name = "TwoDimRegOut{}VsOut{}Bkg{}Heat{}".format(i+1,j+1, bkgName, form)
            formats = ["png", "pdf"]
            for f in formats:
              plt.savefig("./plots/plotCode{}{}.{}".format(plotCode,name,f), transparent=True, dpi=200)

  # End of function
  return

def plotTwoDimRegressionOutProj(df, outputs, modelCode, testCode, dataType=None, desiredVals=[], bkgName=""):
  ''' For each combination of mass splittings, plot the projection to the x and y axis. '''

  # General parameters
  spacing = 50
  bins = np.linspace(0, 3000, 61)
  colors = ["black", "red", "blue", "green", "orange"]

  # Go through pairs of outputs
  for i in range(len(outputs)):
    for j in range(len(outputs)):
      if i < j:
  
        # Loop through desiredVals
        des1s = []
        des1Ups = []
        des1Lows = []
        des2s = []
        des2Ups = []
        des2Lows = []
        filtDFs = []
        for k in range(len(desiredVals)):

          # Get DF only with truth mass splitting in the range of the desired value
          des1 = desiredVals[k][0]
          des2 = desiredVals[k][1]
          des1Up = desiredVals[k][0]+spacing
          des1Low = desiredVals[k][0]-spacing
          des2Up = desiredVals[k][1]+spacing
          des2Low = desiredVals[k][1]-spacing
          dfOut1 = df[outputs[i]]
          dfOut2 = df[outputs[j]]
          filtDF = df[(dfOut1 >= des1Low) & (dfOut1 < des1Up) & (dfOut2 >= des2Low) & (dfOut2 < des2Up)]

          # Save info
          des1s.append(des1)
          des1Ups.append(des1Up)
          des1Lows.append(des1Low)
          des2s.append(des2)
          des2Ups.append(des2Up)
          des2Lows.append(des2Low)
          filtDFs.append(filtDF)

        # Plot each projection individually
        for k in range(len(filtDFs)): 
          DF = filtDFs[k]

          # Get x projections
          x = np.array(DF['Predicted-'+outputs[i]])
          
          # Get weights
          if dataType in ["Sig", "Bkg"]:
            w = np.array(DF['combWeight'])

          # Plot weighted and unweighted versions
          for z in range(2):

            # Get form based on index
            form = ""
            if z == 0:
              form = "Unweighted"
            else:
              form = "Weighted"

            # Only weighted data available for signal and background
            if z == 1 and dataType not in ["Sig", "Bkg"]:
              continue

            # Start fresh plot
            plt.close()
            plt.figure()
            
            # Plot x projections
            if dataType != "Bkg":
              plt.axvline(x=des1s[k], color='grey')
              plt.axvline(x=des1Ups[k], color='grey', linestyle='dotted')
              plt.axvline(x=des1Lows[k], color='grey', linestyle='dotted')
              label = r"$\Delta m_{}={}\pm{}\,$GeV, $\Delta m_{}={}\pm{}\,$GeV".format(i+1,des1s[k],spacing,j+1,des2s[k],spacing)
            else:
              label = r"Background {}".format(bkgName)
            if form == "Weighted":           
              plt.hist(x, bins=bins, weights=w, color='black', histtype='step', linewidth=2,label=label)
            else:
              plt.hist(x, bins=bins, color='black', histtype='step', linewidth=2,label=label)

            # Format x and y axis
            plt.xlabel(r"Predicted $\Delta m_{}$ [GeV]".format(i+1))
            plt.ylabel(r"{} entries".format(form))
            plt.xlim([0,3000])
            plt.ticklabel_format(axis='y', style='sci', scilimits=(-2,3)) 
        
            # Add labels
            plt.text(0.05,0.9, "ATLAS", fontstyle='italic', fontweight='bold', transform=plt.gca().transAxes)
            plt.text(0.165,0.9, "Work In Progress", transform=plt.gca().transAxes)
            regStr = "Regression Model {}".format(modelCode)
            if testCode != None:
              regStr += ", Test {}".format(testCode)
            plt.text(0.05,0.84, regStr, transform=plt.gca().transAxes)

            # Add legend
            plt.legend(loc=(0.04, 0.75), frameon=False)

            # Save x projections
            if dataType != "Bkg":
              name = "TwoDimRegOut{}VsOut{}Val{}Val{}ProjOut{}{}".format(i+1, j+1, des1s[k], des2s[k], i+1, form)
            else:
              name = "TwoDimRegOut{}VsOut{}Bkg{}ProjOut{}{}".format(i+1, j+1, bkgName, i+1, form)
            formats = ["png", "pdf"]
            for f in formats:
              plt.savefig("./plots/plotCode{}{}.{}".format(plotCode,name,f), transparent=True, dpi=200)

            # Start fresh plot
            plt.close()
            plt.figure()

            # Get y projections
            y = np.array(DF['Predicted-'+outputs[j]])
            
            # Plot y projections
            if dataType != "Bkg":
              plt.axvline(x=des2s[k], color='grey')
              plt.axvline(x=des2Ups[k], color='grey', linestyle='dotted')
              plt.axvline(x=des2Lows[k], color='grey', linestyle='dotted')
              label = r"$\Delta m_{}={}\pm{}\,$GeV, $\Delta m_{}={}\pm{}\,$GeV".format(i+1,des1s[k],spacing,j+1,des2s[k],spacing)
            else:
              label = r"Background {}".format(bkgName)
            if form == "Weighted":           
              plt.hist(y, bins=bins, weights=w, color='black', histtype='step', linewidth=2,label=label)
            else:
              plt.hist(y, bins=bins, color='black', histtype='step', linewidth=2,label=label)
           
            # Format x and y axis 
            plt.xlabel(r"Predicted $\Delta m_{}$ [GeV]".format(j+1))
            plt.ylabel(r"{} entries".format(form))
            plt.xlim([0,3000])
            plt.ticklabel_format(axis='y', style='sci', scilimits=(-2,3)) 
            
            # Add labels
            plt.text(0.05,0.9, "ATLAS", fontstyle='italic', fontweight='bold', transform=plt.gca().transAxes)
            plt.text(0.165,0.9, "Work In Progress", transform=plt.gca().transAxes)
            regStr = "Regression Model {}".format(modelCode)
            if testCode != None:
              regStr += ", Test {}".format(testCode)
            plt.text(0.05,0.84, regStr, transform=plt.gca().transAxes)

            # Add legend
            plt.legend(loc=(0.04, 0.75), frameon=False)
            
            # Save y projections
            if dataType != "Bkg":
              name = "TwoDimRegOut{}VsOut{}Val{}Val{}ProjOut{}{}".format(i+1, j+1, des1s[k], des2s[k], j+1, form)
            else:
              name = "TwoDimRegOut{}VsOut{}Bkg{}ProjOut{}{}".format(i+1, j+1, bkgName, j+1, form)
            formats = ["png", "pdf"]
            for f in formats:
              plt.savefig("./plots/plotCode{}{}.{}".format(plotCode,name,f), transparent=True, dpi=200)

  # End of function
  return


def plotRegressionVars(df, modelCode, testCode, bkgName=""):
  ''' Plot variables used for regression for the given model '''
  
  # General variables
  colors = ['black', 'red', 'blue', 'green', 'orange', 'cyan', 'magenta', 'lime', 'grey']

  # Obtain variables used for the given regression model
  posParticles = searchRegressionModelParticles(modelCode)
  particleVars = searchRegressionModelParticleVars(modelCode)
  
  # Update particles with actual one existing in dataset
  datasetCols = df.columns
  particles = []
  for p in posParticles:
    for c in df.columns:
      if p in c:
        particles.append(p)
        break
      
  # Plot each variable separately
  for var in particleVars:
    
    # Gather data for all particles
    inputVals = [np.array(df[p+var]) for p in particles]
      
    # Get minimum and maximum value to plot to define bins
    xMin = 0
    xMax = 0
    if var == "Pt":
      xMin = 0
      xMax = 700
    elif var == "E":
      xMin = 0
      xMax = 700
    elif var == "Phi":
      xMin = -3.5
      xMax = -3.5
    elif var == "Eta":
      xMin = -8
      xMax = 8
    bins = np.linspace(xMin, xMax, 100)
    
    # Weighted and unweighted plotting
    for z in range(2):
      
      # Form
      form = ""
      if z == 0:
        form = "Unweighted"
      elif z == 1:
        form = "Weighted"
      
      # Try to get weights
      try:
        w = df["combWeight"]
      except:
        pass

      # Start fresh plot
      plt.close()
      plt.figure()
    
      # Get x format
      xCushion = 1
      xLabel = ""
      if var == "Pt":
        xLabel = r"$p_T$ [GeV]"
      elif var == "Eta":
        xCushion = 1.1
        xLabel = r"$\eta$"
      elif var == "Phi":
        xCushion = 1.1
        xLabel = r"$\phi$"
      elif var == "E":
        xLabel = r"Energy [GeV]"
     
      # Labels format 
      xShiftLabel = 0
      if var == "Pt":
        xShiftLabel = 0.5
      elif var == "Eta":
        xShiftLabel = 0.5
      elif var == "Phi":
        xShiftLabel = 0
      elif var == "E":
        xShiftLabel = 0.5
    
      # Legend format
      xShiftLeg = 0
      yShiftLeg = 0
      if var == "Pt":
        xShiftLeg = 0.5
        yShiftLeg = -0.12
      elif var == "Eta":
        xShiftLeg = 0.5
        yShiftLeg = -0.12
      elif var == "Phi":
        xShiftLeg = 0.5
        yShiftLeg = 0
      elif var == "E":
        xShiftLeg = 0.5
        yShiftLeg = -0.12

      # Plot data from each particle in the same plot
      for i in range(len(particles)):
        if form == "Weighted":
          plt.hist(inputVals[i], bins=bins, weights=w, linewidth=2, color=colors[i], histtype='step', label=particles[i])
        else:
          plt.hist(inputVals[i], bins=bins, linewidth=2, color=colors[i], histtype='step', label=particles[i])

      # Format x axis
      plt.xlabel(xLabel)
      plt.xlim([xMin*xCushion, xMax*xCushion])
    
      # Format y axis
      plt.ticklabel_format(axis='y', style='sci', scilimits=(-2,3))
      plt.ylabel(r"{} entries".format(form))
      ymin, ymax = plt.ylim()
      plt.ylim([0, ymax])
      if var == 'Phi':
        plt.ylim([0, ymax*1.5])

      # Add labels
      plt.text(0.05+xShiftLabel,0.9, "ATLAS", fontstyle='italic', fontweight='bold', transform=plt.gca().transAxes)
      plt.text(0.165+xShiftLabel,0.9, "Work In Progress", transform=plt.gca().transAxes)
      regStr = "Regression Model {}".format(modelCode)
      if testCode != None:
        regStr += ", Test {}".format(testCode)
      plt.text(0.05+xShiftLabel,0.84, regStr, transform=plt.gca().transAxes)
      if bkgName != "":
        yShiftLeg -= 0.06
        plt.text(0.05+xShiftLabel,0.78, "Background {}".format(bkgName), transform=plt.gca().transAxes)
    
      # Add legend
      plt.legend(loc=(0.04+xShiftLeg, 0.9+yShiftLeg-(0.0475*round(len(particles)/2+0.1) + 0.0125)), ncols=2, frameon=False)

      # Save plot
      formats = ["png", "pdf"]
      name = bkgName+var+form
      for f in formats:
        plt.savefig("./plots/plotCode{}{}.{}".format(plotCode,name,f), transparent=True, dpi=200)

def plotClassificationPerformance(df, scoreName, modelCode):
  '''Plots the signal and background classification performance'''

  # General parameters
  bins = np.linspace(0, 1, 100)

  # Get results
  signal = np.array(df[df["signal"]==1][scoreName])
  bkg = np.array(df[df["signal"]==0][scoreName])

  # Get weights
  sigWeights = np.array(df[df["signal"]==1]["classifierWeight"])
  bkgWeights = np.array(df[df["signal"]==0]["classifierWeight"])

  # Plot both weighted and unweighted
  for z in range(2):

    # Get form
    form = ""
    if z == 0:
      form = "Unweighted"
    else:
      form = "Weighted"

    # Start fresh plot
    plt.close()
    plt.figure()
  
    # Plot data
    if form == "Weighted":
      plt.hist(signal, bins=bins, weights=sigWeights, linewidth=2, color='black', histtype='step', label='Test signal', )
      plt.hist(bkg, bins=bins, weights=bkgWeights, linewidth=2, color='red', histtype='step', label='Test background')
    else:
      plt.hist(signal, bins=bins, linewidth=2, color='black', histtype='step', label='Test signal')
      plt.hist(bkg, bins=bins, linewidth=2, color='red', histtype='step', label='Test background')
      
    # Format x and y axis
    plt.xlabel(r"NN score")
    plt.ylabel(r"{} Entries".format(form))
    plt.xlim([0,1])
    plt.yscale("log")
    ymin, ymax = plt.ylim()
    plt.ylim([ymin, ymax*pow(10,2.5)])
    
    # Add labels
    plt.text(0.55,0.9, "ATLAS", fontstyle='italic', fontweight='bold', transform=plt.gca().transAxes)
    plt.text(0.665,0.9, "Work In Progress", transform=plt.gca().transAxes)
    plt.text(0.55,0.84, "Classification Model {}".format(modelCode), transform=plt.gca().transAxes)
    
    # Add legend
    plt.legend(loc=(0.54, 0.7), frameon=False)
  
    # Save plot
    name = "NNScore"
    formats = ["png", "pdf"]
    for f in formats:
      plt.savefig("./plots/plotCode{}{}{}.{}".format(plotCode,name, form, f), transparent=True, dpi=200)

def plotClassificationVars(df, inputsScale, modelCode, testCode, dataName=""):
  ''' Plot variables used for classification for the given model '''
  
  # General variables
  colors = ['black', 'red', 'blue', 'green', 'orange', 'cyan', 'magenta', 'lime', 'grey']

  # Obtain variables used for the given classification model
  posParticles = searchClassificationModelParticles(modelCode)
  particleVars = searchClassificationModelParticleVars(modelCode)
 
  # Update particles with actual one existing in dataset
  datasetCols = df.columns
  particles = []
  for p in posParticles:
    for c in df.columns:
      if p in c:
        particles.append(p)
        break
      
  # Plot each variable separately
  for var in particleVars:
    
    # Gather data for all particles
    inputVals = [np.array(df[p+var]) for p in particles]
    if inputsScale == "massSplit":
      inputVals = [np.array(df[p+var])/(np.array(df["Predicted-dM1_1"])+np.array(df["Predicted-dM2_1"])) for p in particles]
      
    # Get minimum and maximum value to plot to define bins
    xMin = 0
    xMax = 0
    if var == "Pt":
      xMin = 0
      if inputsScale == "massSplit":
        xMax = 1
      else:
        xMax = 700
    elif var == "E":
      xMin = 0
      xMax = 700
    elif var == "Phi":
      xMin = -3.5
      xMax = -3.5
    elif var == "Eta":
      xMin = -8
      xMax = 8
    bins = np.linspace(xMin, xMax, 100)
    
    # Weighted and unweighted plotting
    for z in range(2):
      
      # Form
      form = ""
      if z == 0:
        form = "Unweighted"
      elif z == 1:
        form = "Weighted"
      
      # Try to get weights
      try:
        w = df["classifierWeight"]
      except:
        pass

      # Start fresh plot
      plt.close()
      plt.figure()
    
      # Get x format
      xCushion = 1
      xLabel = ""
      if var == "Pt":
        if inputsScale == "massSplit":
          xLabel = r"$p_T / \Sigma_i \Delta m^{pred.}_i$"
        else:
          xLabel = r"$p_T$ [GeV]"
      elif var == "Eta":
        xCushion = 1.1
        xLabel = r"$\eta$"
      elif var == "Phi":
        xCushion = 1.1
        xLabel = r"$\phi$"
      elif var == "E":
        xLabel = r"Energy [GeV]"
     
      # Labels format 
      xShiftLabel = 0
      if var == "Pt":
        xShiftLabel = 0.5
      elif var == "Eta":
        xShiftLabel = 0.5
      elif var == "Phi":
        xShiftLabel = 0
      elif var == "E":
        xShiftLabel = 0.5
    
      # Legend format
      xShiftLeg = 0
      yShiftLeg = 0
      if var == "Pt":
        xShiftLeg = 0.5
        yShiftLeg = -0.12
      elif var == "Eta":
        xShiftLeg = 0.5
        yShiftLeg = -0.12
      elif var == "Phi":
        xShiftLeg = 0.5
        yShiftLeg = 0
      elif var == "E":
        xShiftLeg = 0.5
        yShiftLeg = -0.12

      # Plot data from each particle in the same plot
      for i in range(len(particles)):
        if form == "Weighted":
          plt.hist(inputVals[i], bins=bins, weights=w, linewidth=2, color=colors[i], histtype='step', label=particles[i])
        else:
          plt.hist(inputVals[i], bins=bins, linewidth=2, color=colors[i], histtype='step', label=particles[i])

      # Format x axis
      plt.xlabel(xLabel)
      plt.xlim([xMin*xCushion, xMax*xCushion])
    
      # Format y axis
      plt.ticklabel_format(axis='y', style='sci', scilimits=(-2,3))
      plt.ylabel(r"{} entries".format(form))
      ymin, ymax = plt.ylim()
      plt.ylim([0, ymax])
      if var == 'Phi':
        plt.ylim([0, ymax*1.5])

      # Add labels
      plt.text(0.05+xShiftLabel,0.9, "ATLAS", fontstyle='italic', fontweight='bold', transform=plt.gca().transAxes)
      plt.text(0.165+xShiftLabel,0.9, "Work In Progress", transform=plt.gca().transAxes)
      regStr = "Classification Model {}".format(modelCode)
      if testCode != None:
        regStr += ", Test {}".format(testCode)
      plt.text(0.05+xShiftLabel,0.84, regStr, transform=plt.gca().transAxes)
      if dataName != "":
        yShiftLeg -= 0.06
        plt.text(0.05+xShiftLabel,0.78, dataName, transform=plt.gca().transAxes)
    
      # Add legend
      plt.legend(loc=(0.04+xShiftLeg, 0.9+yShiftLeg-(0.0475*round(len(particles)/2+0.1) + 0.0125)), ncols=2, frameon=False)

      # Save plot
      formats = ["png", "pdf"]
      name = dataName.replace(" ","")+var+form
      for f in formats:
        plt.savefig("./plots/plotCode{}{}.{}".format(plotCode,name,f), transparent=True, dpi=200)
    
def plotClassificationOneMassDependency(df, massLabel, modelCode):
  ''' Plots the NN score as a function of massLabel for background '''
  
  # General parameters
  xBins = np.linspace(0, 3000, 51)
  xBinsMidPoint = np.array([(xBins[i]+xBins[i+1])/2.0 for i in range(len(xBins)-1)])

  # Plot weighted and unweighted versions
  for z in range(2):

    # Get form
    form = ""
    if z == 0:
      form = "Unweighted"
    else:
      form = "Weighted"

    # Get values per bin
    sigScoreMeans = []
    sigScoreStds = []
    bkgScoreMeans = []
    bkgScoreStds = []
    for i in range(len(xBins)-1):
      
      # Get x bin limits
      xLow = xBins[i]
      xUp = xBins[i+1]

      # Get filtered dataframes
      sigFiltDF = df[(df["signal"]==1) & (df[massLabel] >= xLow) & (df[massLabel] < xUp)]
      bkgFiltDF = df[(df["signal"]==0) & (df[massLabel] >= xLow) & (df[massLabel] < xUp)]
    
      # Get NN score average
      sigScoreMean = -99
      bkgScoreMean = -99
      if form == "Unweighted":
        sigScoreMean = sigFiltDF["NNScore"].mean()
        bkgScoreMean = bkgFiltDF["NNScore"].mean()
      else:
        if len(sigFiltDF.index) != 0:
          sigScoreMean = np.average(sigFiltDF["NNScore"], weights=sigFiltDF["scaledCombWeight"])
        if len(bkgFiltDF.index) != 0:
          bkgScoreMean = np.average(bkgFiltDF["NNScore"], weights=bkgFiltDF["scaledCombWeight"])
      sigScoreMeans.append(sigScoreMean)
      bkgScoreMeans.append(bkgScoreMean)

      # Get NN score std
      sigScoreStd = -99
      bkgScoreStd = -99
      if form == "Unweighted":
        sigScoreStd = sigFiltDF["NNScore"].std()
        bkgScoreStd = bkgFiltDF["NNScore"].std()
      else:
        if len(sigFiltDF.index) != 0:
          sigScoreStd = np.average((sigFiltDF["NNScore"]-sigScoreMean)**2, weights=sigFiltDF["scaledCombWeight"])
        if len(bkgFiltDF.index) != 0:
          bkgScoreStd = np.average((bkgFiltDF["NNScore"]-bkgScoreMean)**2, weights=bkgFiltDF["scaledCombWeight"])
      sigScoreStds.append(sigScoreStd)
      bkgScoreStds.append(bkgScoreStd)

    # Start fresh plot
    plt.close()
    plt.figure()
    
    # Plot
    plt.plot(xBinsMidPoint, sigScoreMeans, linestyle="None", color='black', marker='o', markersize=4, label="Signal")
    plt.plot(xBinsMidPoint, bkgScoreMeans, linestyle="None", color='red', marker='o', markersize=4, label="Background")
  
    # Format x and y axis
    if "Predicted-dM1" in massLabel:
      plt.xlabel(r"Predicted $\Delta m_1$ [GeV]")
    elif "Predicted-dM2" in massLabel:
      plt.xlabel(r"Predicted $\Delta m_2$ [GeV]")
    else:
      plt.xlabel(r"{}".format(massLabel))
    plt.ylabel(r"{} Avg. NN Score".format(form))
    plt.xlim([0,3000])
    plt.ylim([-0.95,1.95])
        
    # Add labels
    plt.text(0.05,0.9, "ATLAS", fontstyle='italic', fontweight='bold', transform=plt.gca().transAxes)
    plt.text(0.165,0.9, "Work In Progress", transform=plt.gca().transAxes)
    plt.text(0.05,0.84, "Classification Model {}".format(modelCode), transform=plt.gca().transAxes)
  
    # Add legend
    plt.legend(loc=(0.04, 0.7), frameon=False)

    # Save plot
    name = "MassDependency"+massLabel+form
    formats = ["png", "pdf"]
    for f in formats:
      plt.savefig("./plots/plotCode{}{}.{}".format(plotCode, name, f), transparent=True, dpi=200)

def plotClassificationTwoMassDependency(df, modelCode):
  ''' Plots the NN score dependency on mass splitting'''
  
  # General parameters
  bins = np.linspace(0, 3000, 51)

  # Plot weighted and unweighted version
  for z in range(2):

    # Get form
    form = ""
    if z == 0:
      form = "Unweighted"
    else:
      form = "Weighted"

    # Get average score per bin
    avgScore = [[0 for y in range(len(bins)-1)] for x in range(len(bins)-1)]
    for j in range(len(bins)-1):
      for i in range(len(bins)-1):

        # Get x bin limits
        xLow = bins[i]
        xUp = bins[i+1]

        # Get y bin limits
        yLow = bins[j]
        yUp = bins[j+1]

        # Get filtered dataframe
        filteredDF = df[(df["Predicted-dM1_1"] >= xLow) & (df["Predicted-dM1_1"] < xUp) & (df["Predicted-dM2_1"] >= yLow) & (df["Predicted-dM2_1"] < yUp)]
      
        # Compute average score
        avgS = -99
        if form == "Unweighted":
          avgS = filteredDF["NNScore"].mean()
        else:
          if len(filteredDF.index) != 0:
            avgS = np.average(filteredDF["NNScore"], weights=filteredDF["scaledCombWeight"])
        avgScore[j][i] = avgS

    # Get x,y mesh
    x,y = np.meshgrid(bins,bins)

    # Start fresh plot
    plt.close()
    plt.figure()
    
    # Plot
    plt.pcolormesh(x,y,avgScore, cmap='afmhot_r', vmin=0, vmax=1)
    plt.colorbar(label="{} Avg. NN Score".format(form))
  
    # Format x and y axis
    plt.xlabel(r"Predicted $\Delta m_1$ [GeV]")
    plt.ylabel(r"Predicted $\Delta m_2$ [GeV]")
    plt.xlim([0,3000])
    plt.ylim([0,3000])
        
    # Add labels
    plt.text(0.55,0.9, "ATLAS", fontstyle='italic', fontweight='bold', transform=plt.gca().transAxes)
    plt.text(0.695,0.9, "Work In Progress", transform=plt.gca().transAxes)
    plt.text(0.55,0.84, "Classification Model {}".format(modelCode), transform=plt.gca().transAxes)
    plt.text(0.55,0.78, "Combined signal and bkg", transform=plt.gca().transAxes)

    # Save plot
    name = "MassDependency"+form
    formats = ["png", "pdf"]
    for f in formats:
      plt.savefig("./plots/plotCode{}{}.{}".format(plotCode, name, f), transparent=True, dpi=200)

def plotCutterNNScoreMassDependency(df, massLabel, cutterCode):
  ''' Plots the NN score as a function of massLabel for background '''
  
  # General parameters
  xBins = np.linspace(0, 3000, 51)
  xBinsMidPoint = np.array([(xBins[i]+xBins[i+1])/2.0 for i in range(len(xBins)-1)])

  # Plot weighted and unweighted version
  for z in range(2):

    # Get form
    form = ""
    if z == 0:
      form = "Unweighted"
    else:
      form = "Weighted"

    # Get values per bin
    sigScoreMeans = []
    sigScoreStds = []
    bkgScoreMeans = []
    bkgScoreStds = []
    for i in range(len(xBins)-1):
      
      # Get x bin limits
      xLow = xBins[i]
      xUp = xBins[i+1]

      # Get filtered dataframes
      sigFiltDF = df[(df["signal"]==1) & (df[massLabel] >= xLow) & (df[massLabel] < xUp)]
      bkgFiltDF = df[(df["signal"]==0) & (df[massLabel] >= xLow) & (df[massLabel] < xUp)]
    
      # Get NN score average
      sigScoreMean = -99
      bkgScoreMean = -99
      if form == "Unweighted":
        sigScoreMean = sigFiltDF["NNScore"].mean()
        bkgScoreMean = bkgFiltDF["NNScore"].mean()
      else:
        if len(sigFiltDF.index) != 0:
          sigScoreMean = np.average(sigFiltDF["NNScore"], weights=sigFiltDF["scaledCombWeight"])
        if len(bkgFiltDF.index) != 0:
          bkgScoreMean = np.average(bkgFiltDF["NNScore"], weights=bkgFiltDF["scaledCombWeight"])
      sigScoreMeans.append(sigScoreMean)
      bkgScoreMeans.append(bkgScoreMean)

      # Get NN score std
      sigScoreStd = -99
      bkgScoreStd = -99
      if form == "Unweighted":
        sigScoreStd = sigFiltDF["NNScore"].std()
        bkgScoreStd = bkgFiltDF["NNScore"].std()
      else:
        if len(sigFiltDF.index) != 0:
          sigScoreStd = np.average((sigFiltDF["NNScore"]-sigScoreMean)**2, weights=sigFiltDF["scaledCombWeight"])
        if len(bkgFiltDF.index) != 0:
          bkgScoreStd = np.average((bkgFiltDF["NNScore"]-bkgScoreMean)**2, weights=bkgFiltDF["scaledCombWeight"])
      sigScoreStds.append(sigScoreStd)
      bkgScoreStds.append(bkgScoreStd)

    # Start fresh plot
    plt.close()
    plt.figure()
    
    # Plot
    plt.plot(xBinsMidPoint, sigScoreMeans, linestyle="None", color='black', marker='o', markersize=4, label="Signal")
    plt.plot(xBinsMidPoint, bkgScoreMeans, linestyle="None", color='red', marker='o', markersize=4, label="Background")
  
    # Format x and y axis
    if "Predicted-dM1" in massLabel:
      plt.xlabel(r"Predicted $\Delta m_1$ [GeV]")
    elif "Predicted-dM2" in massLabel:
      plt.xlabel(r"Predicted $\Delta m_2$ [GeV]")
    else:
      plt.xlabel(r"{}".format(massLabel))
    plt.ylabel(r"{} Avg. NN Score".format(form))
    plt.xlim([0,3000])
    ymin = min([min([x for x in sigScoreMeans if x >= 0]), min([x for x in bkgScoreMeans if x >= 0])])
    ymax = max([max([x for x in sigScoreMeans if x <= 1]), max([x for x in bkgScoreMeans if x <= 1])])
    plt.ylim([ymin*0.8,ymax*1.2])
        
    # Add labels
    plt.text(0.05,0.9, "ATLAS", fontstyle='italic', fontweight='bold', transform=plt.gca().transAxes)
    plt.text(0.165,0.9, "Work In Progress", transform=plt.gca().transAxes)
    plt.text(0.05,0.84, "Cuts set {}".format(cutterCode), transform=plt.gca().transAxes)
  
    # Add legend
    plt.legend(loc=(0.04, 0.7), frameon=False)

    # Save plot
    name = "MassDependency"+massLabel+form
    formats = ["png", "pdf"]
    for f in formats:
      plt.savefig("./plots/plotCode{}{}.{}".format(plotCode, name, f), transparent=True, dpi=200)


#################################################
#
# Plot events
#
#################################################
plotCode = -1

def main(**kwargs):
  '''
  Mandatory mode:

    1. genCode:
        Kinematic generator dataset code
        Default: None
    2. regressionModelCode:
        Regression model dataset code
        Default: None
    3. regressionTestCode:
        Regression test dataset code
        Default: None
    4. classificationModelCode:
        Classification model dataset code
        Default: None
    5. classificationTestCode:
        Classification test dataset code
        Default: None
    6. cutterCode:
        Event cutter dataset code
        Default: None
    
  Additional arguments:

    1. regressionTruthVals:
        Truth values to use for plotting regression results
        Default: []
  '''

  # Save arguments
  saveArgs(**kwargs)

  # Read arguments
  readArgs()
  if not checkArgs():
    return

  # Start log file
  startLog()

  # Search log file for similar mergers
  global plotCode
  plotCode, match = searchLog()

  # Extract events
  plotter()

  # Update log file
  if not match: updateLog()
  
  # End of event merger
  print("----- END OF EVENT PLOTTER -----")
  print("Thanks for using our event plotter algorithm")
  print("Best,")
  print("Shion Chen and Luis Felipe Gutierrez")

if __name__=="__main__":
  main(**dict(arg.split("=") for arg in sys.argv[1:]))
