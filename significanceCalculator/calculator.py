import sys
import os
import math
import pandas as pd
import numpy as np
import random
import ROOT
import matplotlib
matplotlib.use('Agg')
from  matplotlib import pyplot as plt
from matplotlib import colors as mpColors
import atlas_mpl_style as ampl
ampl.use_atlas_style()


#################################################
#
# Arguments
#
#################################################
path = None
var1 = None
var2 = None

def saveArgs(**kwargs):
  for a, v in kwargs.items():
    if a == "path":
      try:
        global path
        path = v
      except:
        pass
    elif a == "var1":
      try:
        global var1
        var1 = v
      except:
        pass
    elif a == "var2":
      try:
        global var2
        var2 = v
      except:
        pass
  return

def readArgs():
  print("----- Significance calculator arguments -----")
  print("path: "+path)
  print("var1: "+var1)
  print("var2: "+str(var2))
  return

def checkArgs():
  print("----- Significance calculator arguments check -----")
  accepted = True

  # Check path argument
  if not os.path.isfile(path):
    print("Please provide a valid path...")
    accepted = False
  
  # Check var1 argument
  if var1 == None or type(var1) != str:
    print("Please provide a valid var1...")
    accepted = False
  
  # Check var2 argument
  # var2 is not necessary, so no need to check it.
  
  # Accepted values
  if accepted:
    print("Provided arguments are OK... Proceeding with next steps")
  else:
    print("Some arguments are not accepted...")
    print("Exiting program without significance calculation...")
  return accepted


#################################################
#
# Log
#
#################################################

def startLog():
  print("----- Start significance calculator log file -----")
  
  # If log file does not exist, create one
  if not os.path.isfile("./calculator.log"):
    f = open('./calculator.log', 'w')
    f.close()
    print("Log file does not exist. Creating new log file...")
  else:
    print("Using existing log file...")

def searchLog():
  print("----- Searching significance calculator log file -----")

  # Check existing entries
  f = open('./calculator.log', 'r')
  # Variables
  significanceCode = -1
  pathMatch = False
  var1Match = False
  var2Match = False
  match = False
  # Read file
  lines = f.readlines()
  # Dissect lines
  for line in lines:
    code,value = line.replace(" ", "").replace("\n", "").split(":")
    # Check significanceCode
    if code == "significanceCode":
      # First entry
      if significanceCode == -1:
        significanceCode = int(value)
      # Followng entries:
      else:
        # Check if previous significanceCode matches options
        match1 = pathMatch
        match2 = var1Match and var2Match
        match = match1 and match2
        if match:
          break
        # If previous significanceCode does not match options, update significanceCode and reset bools
        else:
          significanceCode = int(value)
          pathMatch = False
          var1Match = False
          var2Match = False
    # Check path
    elif code == "path":
      if path == value: pathMatch = True
    # Check var1
    elif code == "var1":
      if var1 == value: var1Match = True
    # Check var2
    elif code == "var2":
      if str(var2) == str(value): var2Match = True
  # Close file
  f.close()
  # Update match in case it is the last entry
  match1 = pathMatch
  match2 = var1Match and var2Match
  match = match1 and match2
  # Check the matching significanceCode, otherwise create a new one
  if significanceCode == -1:
    # Create new entry
    significanceCode = 0
    match = False
  else:
    # If it does not match, generate a new significanceCode
    if not match: significanceCode += 1

  # Logging
  if match:
    print("Found significance calculation with exact same conditions...")
    print("Using significanceCode: "+str(significanceCode))
  else:
    print("No significance calculation with the same conditions found...")
    print("New significanceCode: "+str(significanceCode))

  # Return output
  return significanceCode, match

def updateLog():
  print("----- Update signficance calculator log file -----")
  f = open("./calculator.log", "a")
  f.write("significanceCode: "+str(significanceCode)+"\n")
  f.write("  path: "+path+"\n")
  f.write("  var1: "+var1+"\n")
  f.write("  var2: "+str(var2)+"\n")
  f.close()
  print("Added entry for significanceCode="+str(significanceCode))


#################################################
#
# Significance calculator
#
#################################################

def calculator():
  print("----- Calculating significance -----")

  # Get dataframe
  print("Getting DFs...")
  if var2 != None:
    DF = pd.read_csv(path)
  else:
    DF = pd.read_csv(path)
  signalDF = DF[DF["signal"]==1]
  bkgDF = DF[DF["signal"]==0]

  # Get bins
  bins = np.linspace(0,3000, 21)

  # Get bkg for 1D and 2D significance
  print("Getting backgrounds...")
  binnedBkgCounter = []
  if var2 == None:
    for i in range(len(bins)-1):
      # Get x limits
      xLow = bins[i]
      xUp = bins[i+1]
      # Get dataframes
      desBkgDF = bkgDF[(bkgDF[var1] >= xLow) & (bkgDF[var1] < xUp)]
      # Save sum of weights
      binnedBkgCounter.append(float(desBkgDF["scaledCombWeight"].sum()))
  else:
    binnedBkgCounter = [[0 for i in range(len(bins)-1)] for j in range(len(bins)-1)]
    for j in range(len(bins)-1):   # y loop
      for i in range(len(bins)-1): # x loop
        # Get x limits
        xLow = bins[i]
        xUp = bins[i+1]
        # Get y limits
        yLow = bins[j]
        yUp = bins[j+1]
        # Get dataframes
        desBkgDF = bkgDF[(bkgDF[var1] >= xLow) & (bkgDF[var1] < xUp) & (bkgDF[var2] >= yLow) & (bkgDF[var2] < yUp)]
        # Save sum of weights
        binnedBkgCounter[j][i] = float(desBkgDF["scaledCombWeight"].sum())
   
  # Get various signals for 1D and 2D significance
  print("Getting signals...")
  indSignals = list(set(signalDF["comment"]))
  binnedSignalCounters = []
  for iS in indSignals:
    binnedSignalCounter = []
    if var2 == None:
      for i in range(len(bins)-1):
        # Get x limits
        xLow = bins[i]
        xUp = bins[i+1]
        # Get dataframes
        if iS == "":
          desSigDF = signalDF[(signalDF[var1] >= xLow) & (signalDF[var1] < xUp)]
        else:
          desSigDF = signalDF[(signalDF[var1] >= xLow) & (signalDF[var1] < xUp) & (signalDF["comment"]==iS)]
        # Save sum of weights
        binnedSignalCounter.append(float(desSigDF["scaledCombWeight"].sum()))
    else:
      binnedSignalCounter = [[0 for i in range(len(bins)-1)] for j in range(len(bins)-1)]
      for j in range(len(bins)-1):   # y loop
        for i in range(len(bins)-1): # x loop
          # Get x limits
          xLow = bins[i]
          xUp = bins[i+1]
          # Get y limits
          yLow = bins[j]
          yUp = bins[j+1]
          # Get dataframes
          if iS == "":
            desSigDF = signalDF[(signalDF[var1] >=xLow) & (signalDF[var1] < xUp) & (signalDF[var2] >= yLow) & (signalDF[var2] < yUp)]
          else:
            desSigDF = signalDF[(signalDF[var1] >=xLow) & (signalDF[var1] < xUp) & (signalDF[var2] >= yLow) & (signalDF[var2] < yUp) & (signalDF["comment"]==iS)]
          binnedSignalCounter[j][i] = float(desSigDF["scaledCombWeight"].sum())
    binnedSignalCounters.append(binnedSignalCounter)
  
  # Calculate significances
  print("Calculating significances...")
  allZs = []
  for sig in binnedSignalCounters:
    zExps = []
    if var2 == None:
      zExps = [0 for i in range(len(bins)-1)]
      for i in range(len(bins)-1):
        if binnedBkgCounter[i] == 0 and sig[i] == 0:
          zExps[i] = -99
        else:
          zExps[i] = ROOT.RooStats.NumberCountingUtils.BinomialExpZ(sig[i], binnedBkgCounter[i], 0.3) # Assuming 30% relative bkg uncertainty
    else:
      zExps = [[0 for i in range(len(bins)-1)] for j in range(len(bins)-1)]
      for j in range(len(bins)-1):
        for i in range(len(bins)-1):
          if binnedBkgCounter[j][i] == 0 and sig[j][i] == 0:
            zExps[j][i] = -99
          else:
            zExps[j][i] = ROOT.RooStats.NumberCountingUtils.BinomialExpZ(sig[j][i], binnedBkgCounter[j][i], 0.3) # Assuming 30% relative bkg uncertainty
    allZs.append(zExps)

  # Replace large and diverging values
  for zInd in range(len(allZs)):
    if var2 == None:
      for i in range(len(allZs[zInd])):
        if allZs[zInd][i] == float('inf') or allZs[zInd][i] > 10:
          allZs[zInd][i] = 5
          if binnedBkgCounter[i] < 0.5 and binnedSignalCounters[zInd][i] < 0.5:
            allZs[zInd][i] = 0
    else:
      for i in range(len(allZs[zInd])):
        for j in range(len(allZs[zInd][0])):
          if allZs[zInd][i][j] == float('inf') or allZs[zInd][i][j] > 10:
            allZs[zInd][i][j] = 5
            if binnedBkgCounter[i][j] < 0.5 and binnedSignalCounters[zInd][i][j] < 0.5:
              allZs[zInd][i][j] = 0

  # Convert expected binomial Z to numpy array
  for i in range(len(allZs)):
    allZs[i] = np.array(allZs[i])
    binnedSignalCounters[i] = np.array(binnedSignalCounters[i])
  binnedBkgCounter = np.array(binnedBkgCounter)

  # Create output directory
  currentDirs = os.listdir("./")
  if "significances" not in currentDirs:
    os.system("mkdir significances")

  # Plot significances
  print("Plotting significance variables...")
  if var2 == None:
    oneDimPlots(var1, bins, 0, 3000, allZs, indSignals, "significance") 
    oneDimPlots(var1, bins, 0, 3000, binnedSignalCounters, indSignals, "signal") 
    oneDimPlots(var1, bins, 0, 3000, binnedBkgCounter, "bkg") 
  else:
    twoDimPlots(var1, bins, 0, 3000, var2, bins, 0, 3000, allZs, "significance", indSignals)
    twoDimPlots(var1, bins, 0, 3000, var2, bins, 0, 3000, binnedSignalCounters, "signal", indSignals)
    twoDimPlots(var1, bins, 0, 3000, var2, bins, 0, 3000, [binnedBkgCounter], "bkg")
  
  # End of function
  return

#################################################
#
# Plots
#
#################################################

def oneDimPlots(xVar, xBins, xMin, xMax, y):
    
  # Start figure
  plt.close()
  plt.figure()

  # Plot
  plt.plot(xBins, y, color='black')
   
  # Format x and y axis
  plt.xlim([xMin, xMax])
  if "Predicted-dM1" in xVar: 
    plt.xlabel(r"Predicted $\Delta m_1$ [GeV]")
  else:
    plt.xlabel(xVar)
  if ext == "significance":
    plt.ylabel("Expected binomial Z")
  else:
    plt.yscale("log")
    if ext == "signal":
      plt.ylabel("Weighted signal")
    elif ext == "bkg":
      plt.ylabel("Weighted background")
    
  # Add labels
  plt.text(0.55,0.9, "ATLAS", fontstyle='italic', fontweight='bold', transform=plt.gca().transAxes)
  plt.text(0.665,0.9, "Work In Progress", transform=plt.gca().transAxes)

  # Save plot
  formats = ["png", "pdf"]
  if ext == "significance":
    name = ""
  elif ext == "signal":
    name = "Sig"
  elif ext == "bkg":
    name = "Bkg"
  for f in formats:
    plt.savefig("./significances/significanceCode{}".format(significanceCode)+name+"."+f, transparent=True, dpi=200)
  

def twoDimPlots(xVar, xBins, xMin, xMax, yVar, yBins, yMin, yMax, zs, dataType, zsStr=[]):
    
  # Grid of bins
  x,y = np.meshgrid(xBins,yBins)
     
  # Plot each z independently
  for i in range(len(zs)):

    # Start figure
    plt.close()
    plt.figure()

    # Plot variables
    if dataType == "significance":
      plt.pcolormesh(x,y,zs[i], cmap='afmhot_r', vmin=-5, vmax=5)
    else:
      plt.pcolormesh(x,y,zs[i], cmap='afmhot_r', vmin=0)
    if dataType == "significance":
      plt.colorbar(label = "Expected binomial Z")
    elif dataType == "signal":
      plt.colorbar(label = "Weighted signal")
    elif dataType == "bkg":
      plt.colorbar(label = "Weighted background")
    
    # Plot reference lines
    if dataType != "bkg" and zsStr[i] != "":
      masses = zsStr[i].split("_")[2:-1]
      dM1 = int(masses[0])-int(masses[1])
      dM2 = int(masses[1])-int(masses[2])
      plt.axvline(x=dM1, color='grey')
      plt.axhline(y=dM2, color='grey')
    
    # Format x and y axis
    plt.xlim([xMin, xMax])
    plt.ylim([yMin, yMax])
    if "Predicted-dM1" in xVar: 
      plt.xlabel(r"Predicted $\Delta m_1$ [GeV]")
    else:
      plt.xlabel(xVar)
    if "Predicted-dM2" in yVar: 
      plt.ylabel(r"Predicted $\Delta m_2$ [GeV]")
    else:
      plt.ylabel(yVar)
    
    # Add labels
    plt.text(0.35,0.9, "ATLAS", fontstyle='italic', fontweight='bold', transform=plt.gca().transAxes)
    plt.text(0.495, 0.9, "Work In Progress", transform=plt.gca().transAxes)
    if dataType == "significance":
      if zsStr[i] != "":
        massesStr = "({},{},{}) GeV".format(masses[0], masses[1], masses[2])
        label2 = r"$m(\tilde{g},\tilde{\chi}^{\pm}_1,\tilde{\chi}_0)=$"+massesStr
        plt.text(0.35, 0.84, label2, transform=plt.gca().transAxes)
      combSig = np.sqrt((zs[i].flatten()**2).sum())
      label = r"$Z_{comb}=$"+"{:.2f}".format(combSig)
      plt.text(0.35, 0.78, label, transform=plt.gca().transAxes)
    elif dataType != "bkg" and zsStr[i] != "":
      massesStr = "({},{},{}) GeV".format(masses[0], masses[1], masses[2])
      label = r"$m(\tilde{g},\tilde{\chi}^{\pm}_1,\tilde{\chi}_0)=$"+massesStr
      plt.text(0.35, 0.84, label, transform=plt.gca().transAxes)
    elif dataType != "bkg":
      label = "Combined signal"
      plt.text(0.35, 0.84, label, transform=plt.gca().transAxes)

    # Save plot
    formats = ["png", "pdf"]
    if dataType == "significance":
      name = "Zs"
    elif dataType == "signal":
      name = "Sig"
    elif dataType == "bkg":
      name = "Bkg"
    if dataType != "bkg" and zsStr[i] == "":
      name += "All"
    elif dataType != "bkg":
      name += "-".join(masses) 
    for f in formats:
      plt.savefig("./significances/significanceCode{}".format(significanceCode)+name+"."+f, transparent=True, dpi=200)


#################################################
#
# Calculate significance
#
#################################################
significanceCode = -1

def main(**kwargs):
  '''
  Mandatory arguments:

    1. path:
        Path to data file with variable to calculate significance on
        Default: None
    2. var1:
        Variable 1 to calculate significance on
        Default: None
    3. var2:
        Variable 2 to calculate significance on
        Default: None
  '''

  # Save arguments
  saveArgs(**kwargs)

  # Read arguments
  readArgs()
  if not checkArgs():
    return

  # Start log file
  startLog()

  # Search log file for similar mergers
  global significanceCode
  significanceCode, match = searchLog()

  # Extract events
  calculator()

  # Update log file
  if not match: updateLog()

  # End of significance calculator
  print("----- END OF SIGNIFICANCE CALCULATOR -----")
  print("Thanks for using our significance calculator algorithm")
  print("Best,")
  print("Shion Chen and Luis Felipe Gutierrez")

if __name__=="__main__":
  main(**dict(arg.split("=") for arg in sys.argv[1:]))
