# Significance calculator

## Calculating significances

#### Directly running programm
```
python3 calculator.py path="PATH/TO/EXISTING/CSVFILE" var1="dM1_1" [...]
```
See below for list of arguments

#### Running shell script
```
./calculateSignificance.sh
```

#### Arguments

##### Mandatory arguments

```path```
* Path to file with data on which significance is going to be performed
* File must contain var1 and a signal field.
* Default: None

```var1```
* Variable 1 to perform significance calculations on
* Default: None

##### Optional arguments

```var2```
* Variable 2 needed to perform 2D significance calculations
* Default: None


## WARNINGs

* To avoid dataset code confusion, do not delete the ```.log``` file which keeps track of the datasets created.

## To do list:

* None
