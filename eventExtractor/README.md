# Event extractor

Package that gathers the important branches from a .root file for easy framework manipulation.

This package also performs minimal calculations such as various types of weights.

## Converting ROOT files to datasets

#### Directly running programm
```
python3 extractor.py path="EXISTING/PATH/TO/ROOT" nType="gluinoOneStep1LSig"
```
See below for list of arguments

#### Running shell script
```
./generateDatasets.sh
```

#### Arguments

##### Mandatory arguments
```path```
* ROOT ntuple path
* Default: None

```nType```
* ROOT ntuple unpacking mode.
* Unfortunately each ntuple has a different format, so the type will likely be deferent for each ntuple.
* "gluinoOneStep1LBkg": Ntuple with the format of the 1L gluino pair production search.
* "gluinoOneStep1LSig": Ntuple with the format of the 1L gluino pair production search.
* Default: None

## WARNINGs

* To avoid dataset code confusion, do not delete the ```.log``` file which keeps track of the datasets created.
