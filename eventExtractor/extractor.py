import sys
import os
import math
import random
import ROOT

#################################################
#
# Arguments
#
#################################################
path = None
nType = None

def saveArgs(**kwargs):
  for a, v in kwargs.items():
    if a == "path":
      try:
        global path
        path = v
      except:
        pass
    elif a == "nType":
      try:
        global nType
        nType = v
      except:
        pass
  return

def readArgs():
  print("----- Event extractor arguments -----")
  print("path: "+path)
  print("nType: "+nType)
  return

def checkArgs():
  print("----- Event extractor arguments check -----")
  accepted = True

  # Check path argument
  if path == None or (not os.path.isfile(path) and not os.path.exists(path)):
    print(path)
    print(os.path.isfile(path))
    print("Please provide a valid ntuple path...")
    print("path must be a .root file")
    accepted = False
  
  # Check nType argument
  if nType not in ["gluinoOneStep1LBkg", "gluinoOneStep1LSig"]:
    print("Please provide a valid nType...")
    print("nType accepted values: \"gluinoOneStep1LBkg\", (default) \"gluinoOneStep1LSig\"")
    accepted = False

  # Accepted values
  if accepted:
    print("Provided arguments are OK... Proceeding with next steps")
  else:
    print("Some arguments are not accepted...")
    print("Exiting program without extraction...")
  return accepted


#################################################
#
# Log
#
#################################################

def startLog():
  print("----- Start event extractor log file -----")
  
  # If log file does not exist, create one
  if not os.path.isfile("./extractor.log"):
    f = open('./extractor.log', 'w')
    f.close()
    print("Log file does not exist. Creating new log file...")
  else:
    print("Using existing log file...")

def searchLog():
  print("----- Searching event extractor log file -----")

  # Check existing entries
  f = open('./extractor.log', 'r')
  # Variables
  datasetCode = -1
  pathMatch = False
  nTypeMatch = False
  match = False
  # Read file
  lines = f.readlines()
  # Dissect lines
  for line in lines:
    code,value = line.replace(" ", "").replace("\n", "").split(":")
    # Check datasetCode
    if code == "datasetCode":
      # First entry
      if datasetCode == -1:
        datasetCode = int(value)
      # Followng entries:
      else:
        # Check if previous datasetCode matches options
        match = pathMatch and nTypeMatch
        if match:
          break
        # If previous dataCode does not match options, update datasetCode and reset bools
        else:
          datasetCode = int(value)
          pathMatch = False
          nTypeMatch = False
    # Check path
    elif code == "path":
      if path == value: pathMatch = True
    # Check nType
    elif code == "nType":
      if nType == value: nTypeMatch = True
  # Close file
  f.close()
  # Update match in case it is the last entry
  match = pathMatch and nTypeMatch
  # Check the matching datasetCode, otherwise create a new one
  if datasetCode == -1:
    # Create new entry
    datasetCode = 0
    match = False
  else:
    # If it does not match, generate a new datasetCode
    if not match: datasetCode += 1

  # Logging
  if match:
    print("Found datatest with exact same conditions...")
    print("Using datasetCode: "+str(datasetCode))
  else:
    print("No test with the same conditions found...")
    print("New datasetCode: "+str(datasetCode))

  # Return output
  return datasetCode, match

def updateLog():
  print("----- Update event extractor log file -----")
  f = open("./extractor.log", "a")
  f.write("datasetCode: "+str(datasetCode)+"\n")
  f.write("  path: "+str(path)+"\n")
  f.write("  nType: "+str(nType)+"\n")
  f.close()
  print("Added entry for datasetCode="+str(datasetCode))


#################################################
#
# Event extractor
#
#################################################

def extractor():
  print("----- Extracting events -----")

  # Open ROOT file
  print("Opening file...")
  f = ROOT.TFile.Open(path,"READ")
 
  # Get file keys
  keys = f.GetListOfKeys()
  
  # Get tree names depending on the nType
  print("Getting TTrees...")
  treeNames = []
  if nType == "gluinoOneStep1LBkg":
    treeNames = [k.GetName() for k in keys if "data" not in k.GetName() and "weight" not in k.GetName()]
  elif nType == "gluinoOneStep1LSig":
    treeNames = [k.GetName() for k in keys if "GG_onestepCC" in k.GetName()]
  
  # Create output directory
  currentDirs = os.listdir("./")
  if "datasets" not in currentDirs:
    os.system("mkdir datasets")
  
  # Extract each tree information
  print("Extracting information...")

  if "gluinoOneStep1L" in nType:
    
    # Start output file
    outF = open("./datasets/datasetCode{}.csv".format(datasetCode), "w")
    header = "q1APt,q2APt,q3APt,q4APt,q1BPt,q2BPt,l1BPt,metPt,mG,mX,mN,dM1_1,dM1_2,dM2_1,dM2_2,pileupWeight,leptonWeight,bTagWeight,jvtWeight,eventWeight,genWeight,ttNNLOWeight,xsec,combWeight,sumOfWeight,comment\n"
    outF.write(header)

    # Get info from TTree
    for treeN in treeNames:
      tree = f.Get(treeN)
      
      # General information
      if "Sig" in nType:
        mG, mX, mN = treeN.replace("GG_onestepCC_","").replace("_NoSys","").split("_")
        dM1_1 = int(mG)-int(mX)
        dM1_2 = dM1_1
        dM2_1 = int(mX)-int(mN)
        dM2_2 = dM2_1
      else:
        mG, mX, mN = -1, -1, -1
        dM1_1 = -1
        dM1_2 = dM1_1
        dM2_1 = -1
        dM2_2 = dM2_1

      # Entry specific info
      for entryN in range(tree.GetEntries()):
        tree.GetEntry(entryN)

        # Get quarks pT
        q1APt = getattr(tree, "jet1Pt")
        q2APt = getattr(tree, "jet2Pt")
        q3APt = getattr(tree, "jet3Pt")
        q4APt = getattr(tree, "jet4Pt")
        q1BPt = 0
        try:
          q1BPt = getattr(tree, "jet5Pt")
        except:
          pass
        q2BPt = getattr(tree, "jet6Pt")
    
        # Get lepton pT
        l1BPt = getattr(tree, "lep1Pt")
        l2BPt = getattr(tree, "lep2Pt")

        # Get met
        metPt = getattr(tree, "met")

        # Get event weight
        pileupWeight = getattr(tree, "pileupWeight")
        leptonWeight = getattr(tree, "leptonWeight")
        bTagWeight = getattr(tree, "bTagWeight")
        jvtWeight = getattr(tree, "jvtWeight")
        eventWeight = getattr(tree, "eventWeight")
        genWeight = getattr(tree, "genWeight")
        ttNNLOWeight = getattr(tree, "ttNNLOWeight")
        xsec = getattr(tree, "xsec")
        dsid = getattr(tree, "DSID")
        # Missing xsec affects genWeight
        if "GG_onestepCC" in treeN:
          # There is a bug with some signal samples cross-sections that are set to -1
          if dsid == 371639: patchedXS=0.0228
          elif dsid == 371638: patchedXS=0.0217 
          elif dsid == 371636: patchedXS=0.0153 
          elif dsid == 371634: patchedXS=0.00770 
          elif dsid == 371637: patchedXS=0.00616 
          elif dsid == 371632: patchedXS=0.00396 
          elif dsid == 371635: patchedXS=0.00318 
          elif dsid == 371630: patchedXS=0.00207 
          elif dsid == 371633: patchedXS=0.00167 
          elif dsid == 371649: patchedXS=0.00135 
          elif dsid == 371628: patchedXS=0.00109 
          elif dsid == 371631: patchedXS=0.000885 
          elif dsid == 371648: patchedXS=0.000718 
          elif dsid == 371626: patchedXS=0.000583 
          elif dsid == 371629: patchedXS=0.000473 
          elif dsid == 371647: patchedXS=0.000385 
          elif dsid == 371624: patchedXS=0.000313 
          elif dsid == 371627: patchedXS=0.000255 
          elif dsid == 371622: patchedXS=0.000208 
          elif dsid == 371646: patchedXS=0.000208 
          elif dsid == 371619: patchedXS=0.000169 
          elif dsid == 371623: patchedXS=0.000169 
          elif dsid == 371620: patchedXS=0.000138 
          elif dsid == 371625: patchedXS=0.000138 
          elif dsid == 371621: patchedXS=0.000113 
          elif dsid == 371645: patchedXS=0.000113 
          elif dsid == 371643: patchedXS=0.0000918 
          elif dsid == 371640: patchedXS=0.0000749 
          elif dsid == 371644: patchedXS=0.0000749 
          elif dsid == 371641: patchedXS=0.0000611 
          elif dsid == 371642: patchedXS=0.0000499
          else: patchedXS = xsec
          # Patch genWeight
          if xsec == -1:
            genWeight = -patchedXS*genWeight # genWeight is originally negative due to negative xsec
          # Patch xsec
          xsec = patchedXS
        sumOfWeight = float(xsec/genWeight)
         # pileupWeight was removed from combWeight due to significant bug during reco signal generation
        combWeight = 139*1000*leptonWeight*bTagWeight*jvtWeight*eventWeight*genWeight*ttNNLOWeight

        # Add the tree name in comment
        comment = treeN
        
        # Add event to output file
        if l2BPt <= 0 and q2BPt > 0:
          entryStr = "{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},\"{}\"\n".format(q1APt, 
                                                                                                                  q2APt, 
                                                                                                                  q3APt, 
                                                                                                                  q4APt, 
                                                                                                                  q1BPt, 
                                                                                                                  q2BPt, 
                                                                                                                  l1BPt, 
                                                                                                                  metPt, 
                                                                                                                  mG, 
                                                                                                                  mX, 
                                                                                                                  mN, 
                                                                                                                  dM1_1, 
                                                                                                                  dM1_2, 
                                                                                                                  dM2_1, 
                                                                                                                  dM2_2,
                                                                                                                  pileupWeight,
                                                                                                                  leptonWeight,
                                                                                                                  bTagWeight,
                                                                                                                  jvtWeight,
                                                                                                                  eventWeight,
                                                                                                                  genWeight,
                                                                                                                  ttNNLOWeight,
                                                                                                                  xsec,
                                                                                                                  combWeight,
                                                                                                                  sumOfWeight,
                                                                                                                  comment)
          outF.write(entryStr)
    
    # Close output file 
    outF.close()

  # End of function
  return


#################################################
#
# Extract events
#
#################################################
datasetCode = -1

def main(**kwargs):
  '''
  Mandatory arguments:

    1. path:
        ROOT ntuple path
    2. nType:
        ROOT ntuple unpacking mode.
        Unfortunately each ntuple has a different format, so the type will likely
        be deferent for each ntuple.
        "gluinoOneStep1LBkg": Ntuple with the format of the 1L gluino pair production search.
        "gluinoOneStep1LSig": Ntuple with the format of the 1L gluino pair production search.
        Default: "gluinoOneStep1LSig"
  '''

  # Save arguments
  saveArgs(**kwargs)

  # Read arguments
  readArgs()
  if not checkArgs():
    return

  # Start log file
  startLog()

  # Search log file for similar extractions
  global datasetCode
  datasetCode, match = searchLog()

  # Extract events
  extractor()

  # Update log file
  if not match: updateLog()

  # End of event extraction
  print("----- END OF EVENT EXTRACTOR -----")
  print("Thanks for using our event extractor algorithm")
  print("Best,")
  print("Shion Chen and Luis Felipe Gutierrez")

if __name__=="__main__":
  main(**dict(arg.split("=") for arg in sys.argv[1:]))
