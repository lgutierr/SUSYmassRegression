import os
import time
import pandas as pd
import numpy as np
from matplotlib import pyplot as plt
print("Imported modules...")

def publishedLimits(processes, outNamePrefix):
  # Get contour files
  publishedLimitsPath = "../data/"
  contours = []
  for p in processes:
    tmpPath = None
    if "gluino" in p:
      tmpPath = os.path.join(publishedLimitsPath, "gluinoLimits/HEPdata/")
    contours.append(os.path.join(tmpPath, p+"ExpContour.csv"))
    contours.append(os.path.join(tmpPath, p+"ObsContour.csv"))
  # Read file
  DFs = dict()
  for c in contours:
    f = pd.read_csv(c, header=8)
    DFs[c] = f
  # Plot each contour in the same figure
  plt.close()
  plt.title("Published limits")
  xAxisLabel = None
  yAxisLabel = None
  for key, val in DFs.items():
    # Get axis labels and dataframe keys
    xEntry = None
    yEntry = None
    cols = val.columns
    if "gluinoDD" in key or "gluinoCD1" in key:
      for c in cols:
        if "LSP" in c:
          yEntry = c
          yAxisLabel = r"$m(\tilde{\chi}^0_1)$ [GeV]"
        else:
          xEntry = c
          xAxisLabel = r"$m(\tilde{g})$ [GeV]"
    # Get necessary legend entry
    legendEntry = None
    if "Obs" in key:
      legendEntry = r"Observed " 
    elif "Exp" in key:
      legendEntry = r"Expected "
    if "gluinoDD" in key:
      legendEntry += r"$\tilde{g}\rightarrow q q\tilde{\chi}^0_1$"
    elif "gluinoCD1" in key:
      legendEntry += r"$\tilde{g}\rightarrow q q \tilde{\chi}^{\pm}_1 \rightarrow q q W \tilde{\chi}^0_1$"
    plt.scatter(x=np.array(val[xEntry]), y=np.array(val[yEntry]), marker='.', s=1, label=legendEntry)
  plt.plot([1,2000], [1,2000], linestyle='dashed', color='grey', marker=None)
  plt.xlabel(xAxisLabel)
  plt.ylabel(yAxisLabel)
  plt.xlim([0,2500])
  plt.ylim([0,2000])
  plt.legend(frameon=False, numpoints=3)
  plt.savefig("./limits/"+outNamePrefix+".png", transparent=True, dpi=300)
  plt.savefig("./limits/"+outNamePrefix+".pdf", transparent=True, dpi=250)

if __name__=="__main__":
  publishedLimits(["gluinoDD","gluinoCD1"], "publishedGluinoLimits")
