# SUSY mass regression software

## Event extractor

Extract necessary information from ROOT files and produces datasets.

## Event merger

Combines datasets.

## Kinematic generator

Fast event generator for easy ML training and testing.

## Models

Generates and tests NN model.

## WARNINGs

* To avoid code confusion, do not delete the ```.log``` files which keeps track of packages processes.
