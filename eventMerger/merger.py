import sys
import os
import math
import pandas as pd

#################################################
#
# Arguments
#
#################################################
splitType = "NN"
signalDatasets = []
bkgDatasets = []
nSplits = 1

def saveArgs(**kwargs):
  for a, v in kwargs.items():
    if a == "splitType":
      try:
        global splitType
        splitType = v
      except:
        pass
    elif a == "signalDatasets":
      try:
        global signalDatasets
        signalDatasets = eval(v)
      except:
        pass
    elif a == "bkgDatasets":
      try:
        global bkgDatasets
        bkgDatasets = eval(v)
      except:
        pass
    elif a == "nSplits":
      try:
        global nSplits
        nSplits = int(v)
      except:
        pass
  return

def readArgs():
  print("----- Event merger arguments -----")
  print("splitType: "+splitType)
  print("signalDatasets: "+str(signalDatasets))
  print("bkgDatasets: "+str(bkgDatasets))
  print("nSplits: "+str(nSplits))
  return

def checkArgs():
  print("----- Event merger arguments check -----")
  accepted = True

  # Check splitType argument
  if splitType not in ["NN"]:
    print("Please provide a valid splitType...")
    print("splitType accepted values: (Default) \"NN\"")
    accepted = False
  
  # Check signalDatasets argument
  if searchDatasets(signalDatasets):
    print("Please provide a valid list of signalDatasets...")
    print("See eventExtractor or regressionTesting log file for accepted datasets...")
    accepted = False
  
  # Check bkgDatasets argument
  if searchDatasets(bkgDatasets):
    print("Please provide a valid list of bkgDatasets...")
    print("See eventExtractor log file for accepted datasets...")
    accepted = False

  # Check nSplits argument
  if nSplits < 1:
    print("Please provide a valid nSplits...")
    print("nSplits accepted values: positive integer greater than 0")
    accepted = False
  
  # Accepted values
  if accepted:
    print("Provided arguments are OK... Proceeding with next steps")
  else:
    print("Some arguments are not accepted...")
    print("Exiting program without merging...")
  return accepted


#################################################
#
# Event extractor log
#
#################################################

def searchDatasets(datasets):
  with open("../eventExtractor/extractor.log","r") as f:
    lines = f.readlines()
    for ds in datasets:
      found = False
      for line in lines:
        if "datasetCode" in line:
          dsName = line.replace(":","").strip()
          if dsName == ds:
            found = True
            break
      if found:
        continue
      else:
        return False
  return True


#################################################
#
# Log
#
#################################################

def startLog():
  print("----- Start event merger log file -----")
  
  # If log file does not exist, create one
  if not os.path.isfile("./merger.log"):
    f = open('./merger.log', 'w')
    f.close()
    print("Log file does not exist. Creating new log file...")
  else:
    print("Using existing log file...")

def searchLog():
  print("----- Searching event merger log file -----")

  # Check existing entries
  f = open('./merger.log', 'r')
  # Variables
  mergeCode = -1
  splitTypeMatch = False
  signalDatasetsMatch = False
  bkgDatasetsMatch = False
  nSplitsMatch = False
  match = False
  # Read file
  lines = f.readlines()
  # Dissect lines
  for line in lines:
    code,value = line.replace(" ", "").replace("\n", "").split(":")
    # Check mergeCode
    if code == "mergeCode":
      # First entry
      if mergeCode == -1:
        mergeCode = int(value)
      # Followng entries:
      else:
        # Check if previous datasetCode matches options
        match1 = splitTypeMatch and nSplitsMatch
        match2 = signalDatasetsMatch and bkgDatasetsMatch
        match = match1 and match2
        if match:
          break
        # If previous dataCode does not match options, update datasetCode and reset bools
        else:
          mergeCode = int(value)
          splitTypeMatch = False
          nSplitMatch = False
          signalDatasetsMatch = False
          bkgDatasetsMatch = False
    # Check splitType
    elif code == "splitType":
      if splitType == value: splitTypeMatch = True
    # Check nSplits
    elif code == "nSplits":
      if nSplits == int(value): nSplitsMatch = True
    # Check signalDatasets
    elif code == "signalDatasets":
      if signalDatasets == eval(value): signalDatasetsMatch = True
    # Check bkgDatasets
    elif code == "bkgDatasets":
      if bkgDatasets == eval(value): bkgDatasetsMatch = True
  # Close file
  f.close()
  # Update match in case it is the last entry
  match1 = splitTypeMatch and nSplitsMatch
  match2 = signalDatasetsMatch and bkgDatasetsMatch
  match = match1 and match2
  # Check the matching mergeCode, otherwise create a new one
  if mergeCode == -1:
    # Create new entry
    mergeCode = 0
    match = False
  else:
    # If it does not match, generate a new mergeCode
    if not match: mergeCode += 1

  # Logging
  if match:
    print("Found datatest with exact same conditions...")
    print("Using mergeCode: "+str(mergeCode))
  else:
    print("No test with the same conditions found...")
    print("New mergeCode: "+str(mergeCode))

  # Return output
  return mergeCode, match

def updateLog():
  print("----- Update event merger log file -----")
  f = open("./merger.log", "a")
  f.write("mergeCode: "+str(mergeCode)+"\n")
  f.write("  splitType: "+str(splitType)+"\n")
  f.write("  signalDatasets: "+str(signalDatasets)+"\n")
  f.write("  bkgDatasets: "+str(bkgDatasets)+"\n")
  f.write("  nSplits: "+str(nSplits)+"\n")
  f.close()
  print("Added entry for mergeCode="+str(mergeCode))


#################################################
#
# Event merger
#
#################################################

def merger():
  print("----- Merging events -----")

  # Obtain signal dataframe
  print("Obtaining signal dataframes...")
  signalDFs = []
  for s in signalDatasets:
    if "datasetCode" in s:
      df = pd.read_csv("../eventExtractor/datasets/"+s+".csv")
    elif "regression" in s:
      df = pd.read_csv("../models/datasets/"+s+".csv")
    signalDFs.append(df)
  signalDF = None
  for i in range(len(signalDatasets)):
    if i == 0:
      signalDF = signalDFs[i]
    else:
      signalDF.append(signalDFs[i], ignore_index=True)
  signalDF['signal'] = 1
  signalDF['scaleF'] = 1
  signalDF['scaledCombWeight'] = signalDF['scaleF']*signalDF['combWeight']

  # Obtain bkg dataframe
  print("Obtaining bkg dataframes...")
  bkgDFs = []
  for b in bkgDatasets:
    if "datasetCode" in b:
      df = pd.read_csv("../eventExtractor/datasets/"+b+".csv")
    elif "regression" in b:
      df = pd.read_csv("../models/datasets/"+b+".csv")
    bkgDFs.append(df)
  bkgDF = None
  for i in range(len(bkgDatasets)):
    if i == 0:
      bkgDF = bkgDFs[i]
    else:
      bkgDF = pd.concat([bkgDF, bkgDFs[i]], ignore_index=True)
  bkgDF['signal'] = 0
  bkgDF['scaleF'] = 1
  bkgDF['scaledCombWeight'] = bkgDF['scaleF']*bkgDF['combWeight']
  
  # Combine signal and bkg dataframes
  print("Merging dataframes...")
  combDF = pd.concat([signalDF,bkgDF], ignore_index=True)
  
  # Split signal and bkg dataframes
  # Work with copies to avoid losing the originals
  print("Splitting dataframes...")
  splitSignalDFs = []
  splitBkgDFs= []
  signalDFCopy = signalDF.copy(deep=True)
  bkgDFCopy = bkgDF.copy(deep=True)
  if splitType == "NN":
    # Evenly split balanced samples
    for i in range(nSplits*2):
      # Split signal
      splitSig = signalDFCopy.sample(frac = 1/float(2*nSplits-i))
      signalDFCopy = signalDFCopy.drop(splitSig.index)
      splitSig = splitSig.reset_index(drop=True)
      splitSignalDFs.append(splitSig)
      # Split bkg
      splitBkg = bkgDFCopy.sample(frac = 1/float(2*nSplits-i))
      bkgDFCopy = bkgDFCopy.drop(splitBkg.index)
      splitBkg = splitBkg.reset_index(drop=True)
      splitBkgDFs.append(splitBkg)

  # Scale weights for each background type
  bkgNames = list(set(bkgDF["comment"])) 
  bkgNames.sort()
  bkgTotalCombWeights = []
  for bkgN in bkgNames:
    totalCombWeight = bkgDF[bkgDF["comment"] == bkgN]["combWeight"].sum()
    bkgTotalCombWeights.append(totalCombWeight)
  for df in splitBkgDFs:
    for i in range(len(bkgNames)):
      desDF = df[df["comment"] == bkgNames[i]]
      indexes = desDF.index
      splitCombWeight = desDF["combWeight"].sum()
      scaleF = bkgTotalCombWeights[i]/splitCombWeight
      df.loc[indexes, "scaleF"] = scaleF
      df.loc[indexes, "scaledCombWeight"] = scaleF*df["combWeight"]

  # Scale weights for each signal type
  signalNames = list(set(signalDF["comment"])) 
  signalNames.sort()
  signalTotalCombWeights = []
  for signalN in signalNames:
    totalCombWeight = signalDF[signalDF["comment"] == signalN]["combWeight"].sum()
    signalTotalCombWeights.append(totalCombWeight)
  for df in splitSignalDFs:
    for i in range(len(signalNames)):
      desDF = df[df["comment"] == signalNames[i]]
      indexes = desDF.index
      splitCombWeight = desDF["combWeight"].sum()
      scaleF = signalTotalCombWeights[i]/splitCombWeight
      df.loc[indexes, "scaleF"] = scaleF
      df.loc[indexes, "scaledCombWeight"] = scaleF*df["combWeight"]
  
  # Combine signal and bkg dataframes
  print("Merging splitted dataframes...")
  combSplittedDFs = []
  for i in range(len(splitSignalDFs)):
    newDF = splitSignalDFs[i]
    newDF = pd.concat([newDF, splitBkgDFs[i]], ignore_index=True)
    combSplittedDFs.append(newDF)

  # Create output directory
  currentDirs = os.listdir("./")
  if "merges" not in currentDirs:
    os.system("mkdir merges")
  
  # Save merged dataframes
  print("Saving merged dataframe...")
  combDF.to_csv("./merges/mergeCode{}full.csv".format(mergeCode), index=False)
 
  # Save merged splitted dataframes 
  print("Saving merged splitted dataframes...")
  if splitType == "NN":
    splitCounter = 0
    fileType = ""
    for i in range(nSplits*2):
      if i % 2 == 0:
        fileType = "train"
        splitCounter += 1
      else:
        fileType = "test"
      outputName = "./merges/mergeCode{}{}".format(mergeCode,fileType) 
      if nSplits > 1:
        outputName += "Split{}".format(splitCounter)
      outputName += ".csv"
      combSplittedDFs[i].to_csv(outputName, index=False)
  
  # End of function
  return


#################################################
#
# Merge events
#
#################################################
mergeCode = -1

def main(**kwargs):
  '''
  Mandatory arguments:

    1. splitType:
        How to split merged datasets
        "NN": split datasets in a training and testing sample and
        Default: "NN"
    2. signalDatasets:
        Signal datasets to merge
        Default: None
    3. bkgDatasets:
        Background datasets to merge
        Default: None
    4. nSplits:
        Number of splits to perform
        Default: 1
  '''

  # Save arguments
  saveArgs(**kwargs)

  # Read arguments
  readArgs()
  if not checkArgs():
    return

  # Start log file
  startLog()

  # Search log file for similar mergers
  global mergeCode
  mergeCode, match = searchLog()

  # Extract events
  merger()

  # Update log file
  if not match: updateLog()
  
  # End of event merger
  print("----- END OF EVENT MERGER -----")
  print("Thanks for using our event merger algorithm")
  print("Best,")
  print("Shion Chen and Luis Felipe Gutierrez")

if __name__=="__main__":
  main(**dict(arg.split("=") for arg in sys.argv[1:]))
