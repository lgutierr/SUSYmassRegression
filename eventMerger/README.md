# Event merger

Package that combines a signal and background dataset and splits the combined dataset in various forms.

This package also performs scale factors to account for reduced statistics due to splits.

## Merging datasets

#### Directly running programm
```
python3 merger.py splitType="NN" signalDatasets=["datasetCode0",...] bkgDatasets=["datasetCode1",...] nSplits=2
```
See below for list of arguments

#### Running shell script
```
./generateMerges.sh
```

#### Arguments

##### Allowed arguments

```splitType```
* How to split merged datasets
* "NN": split datasets in a training and testing sample
* Default: "NN"

```signalDatasets```
* Signal datasets to use for merging
* Default: None

``` bkgDatasets```
* Background datasets to merge
* Default: None

```nSplits```
* Number of splits to perform
* Default: 1

## WARNINGs

* To avoid dataset code confusion, do not delete the ```.log``` file which keeps track of the datasets created.
