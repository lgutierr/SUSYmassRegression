import sys
import os
import pandas as pd
import numpy as np
import keras.backend as K
import tensorflow as tf
from tensorflow.keras import layers
from tensorflow.keras.callbacks import EarlyStopping, ModelCheckpoint
from tensorflow.keras.layers import Input, Dense, GRU, Add, Concatenate, BatchNormalization, Conv1D, Lambda, Dot, Flatten
from tensorflow.keras.models import Model
from matplotlib import pyplot as plt
import atlas_mpl_style as ampl
ampl.use_atlas_style()


#################################################
#
# Event merger log
#
#################################################

def searchMergeCode(mergeCode):
  with open("../eventMerger/merger.log","r") as f:
    lines = f.readlines()
    for line in lines:
      if "mergeCode" in line:
        code = int(line.split(":")[1].strip())
        if code == mergeCode:
          return True
  return False


#################################################
#
# Mode
#
#################################################
mode = None

def saveMode(**kwargs):
  for m, v in kwargs.items():
    if m == "mode":
      try:
        global mode
        mode = v
      except:
        pass

def readMode():
  print("----- Classification mode -----")
  print(mode)
  return

def checkMode():
  print("----- Classification mode check -----")
  accepted = True

  # Check mode
  if mode == None:
    print("Please select a mode...")
    accepted = False
  elif mode not in ["testing", "modelling"]:
    print("Value of mode is not accepted...")
    print("mode accepted values: \"testing\", \"modelling\"")
    accepted = False
  
  # Accepted values
  if accepted:
    print("Selected mode OK... Proceeding with next steps")
  else:
    print("Mode not accepted...")
    print("Exiting program...")
  return accepted

#################################################
#
# Testing Arguments
#
#################################################
testCode = -1
mergeCode = 0

def saveTestingArgs(**kwargs):
  for a, v in kwargs.items():
    if a == "modelCode":
      try:
        global modelCode
        modelCode = int(v)
      except:
        pass
    elif a == "mergeCode":
      try:
        global mergeCode
        mergeCode = int(v)
      except:
        pass

def readTestingArgs():
  print("----- Classification testing arguments -----")
  print("modelCode: "+str(modelCode))
  print("mergeCode: "+str(mergeCode))
  return

def checkTestingArgs():
  print("----- Classification testing arguments check -----")
  accepted = True
  
  # Check model code
  if not searchModelCode(modelCode):
    print("Used modelCode does not exist...")
    print("See classification modelling logs for existing modelCode")
    accepted = False

  # Check dataset code
  if not searchMergeCode(mergeCode):
    print("Used mergeCode does not exist...")
    print("See event merger logs for existing mergeCode")
    accepted = False

  # Accepted values
  if accepted:
    print("All testing arguments are OK... Proceeding with next steps")
  else:
    print("Some testing arguments are not accepted...")
    print("Exiting program...")
  return accepted
  

#################################################
#
# Modelling Arguments
#
#################################################
modelCode = -1
mergeCode = None
nnType = "DNN"
regType = "L2"
nnComplexity = 0
learningRate = 0.001
validationSplit = 0.3
nEpochs = 10
batchSize = 50000
particlesCode = 0
particleVarsCode = 0
inputsScale = ""
modellingTests = True

def saveModellingArgs(**kwargs):
  for a, v in kwargs.items():
    if a == "mergeCode":
      try:
        global mergeCode
        mergeCode = int(v)
      except:
        pass
    elif a == "nnType":
      try:
        global nnType
        nnType = v
      except:
        pass
    elif a == "regType":
      try:
        global regType
        regType = v
      except:
        pass
    elif a == "nnComplexity":
      try:
        global nnComplexity
        nnComplexity = int(v)
      except:
        pass
    elif a == "learningRate":
      try:
        global learningRate
        learningRate = float(v)
      except:
        pass
    elif a == "nEpochs":
      try:
        global nEpochs
        nEpochs = int(v)
      except:
        pass
    elif a == "batchSize":
      try:
        global batchSize
        batchSize = int(v)
      except:
        pass
    elif a == "validationSplit":
      try:
        global validationSplit
        validationSplit = float(v)
      except:
        pass
    elif a == "particlesCode":
      try:
        global particlesCode
        particlesCode = int(v)
      except:
        pass
    elif a == "particleVarsCode":
      try:
        global particleVarsCode
        particleVarsCode = int(v)
      except:
        pass
    elif a == "inputsScale":
      try:
        global inputsScale
        inputsScale = v
      except:
        pass
    elif a == "modellingTests":
      try:
        global modellingTests
        modellingTests = eval(v)
      except:
        pass

def readModellingArgs():
  print("----- Classification modelling arguments -----")
  print("mergeCode: "+str(mergeCode))
  print("nnType: "+nnType)
  print("regType: "+regType)
  print("nnComplexity: "+str(nnComplexity))
  print("learningRate: "+str(learningRate))
  print("nEpochs: "+str(nEpochs))
  print("batchSize: "+str(batchSize))
  print("validationSplit: "+str(validationSplit))
  print("particlesCode: "+str(particlesCode))
  print("particleVarsCode: "+str(particleVarsCode))
  print("inputsScale: "+str(inputsScale))
  return

def checkModellingArgs():
  print("----- Classification modelling arguments check -----")
  accepted = True

  # Check kinematic generator code
  if not searchMergeCode(mergeCode):
    print("Used mergeCode does not exist...")
    print("See event merger logs for existing mergeCode")
    accepted = False
  
  # Check neural network type
  if nnType not in ["DNN"]:
    print("Value of nnType is not accepted...")
    print("nnType accepted values: (default) \"DNN\"")
    accepted = False

  # Check regularization type
  if regType not in ["No", "L1", "L2"]:
    print("Value of regType is not accepted...")
    print("regType accepted values: \"No\", \"L1\", or \"L2\"")
    accepted = False

  # Check neural network complexity
  if nnComplexity not in [0]:
    print("Value of nnComplexity is not accepted...")
    print("nnComplexity accepted values: (default) 0")
    accepted = False

  # Check learning rate
  if learningRate < 0:
    print("Value of learningRate is not accepted...")
    print("learningRate accepted values: positive float, (default) 0.001")
    accepted = False
  
  # Check number epochs
  if nEpochs < 0:
    print("Value of nEpochs is not accepted...")
    print("nEpochs accepted values: positive integer, (default) 10")
    accepted = False
  
  # Check batch size
  if batchSize < 0:
    print("Value of batchSize is not accepted...")
    print("batchSize accepted values: positive integer, (default) 10000")
    accepted = False
  
  # Check validation split
  if validationSplit < 0 or validationSplit > 1:
    print("Value of validationSplit is not accepted...")
    print("validationSplit accepted values: positive float in range [0,1], (default) 10000")
    accepted = False
  
  # Check particle variables code
  if particlesCode not in [0,1]:
    print("Value of particlesCode is not accepted...")
    print("particlesCode accepted values: (default) 0, 1, 2")
    accepted = False
  
  # Check particle variables code
  if particleVarsCode not in [0,1,2,3]:
    print("Value of particleVarsCode is not accepted...")
    print("particleVarsCode accepted values: (default) 0, 1, 2")
    accepted = False
  
  # Check inputs scale
  if inputsScale not in ["", "massSplit"]:
    print("Value of inputsScale is not accepted...")
    print("inputsScale accepted values: \"massSplit\", (default) \"\"")
    accepted = False
  
  # Accepted values
  if accepted:
    print("All options are OK... Proceeding with next steps")
  else:
    print("Some options are not accepted...")
    print("Exiting program without model construction...")
  return accepted


#################################################
#
# Testing Log
#
#################################################

def startTestingLog():
  print("----- Start classification testing log file -----")
  
  # If log file does not exist, create one
  if not os.path.isfile("./classificationTesting.log"):
    f = open('./classificationTesting.log', 'w')
    f.close()
    print("Log file does not exist. Creating new log file...")
  else:
    print("Using existing log file...")

def searchTestingLog():
  print("----- Searching classification testing log file -----")
  # Check existing entries
  f = open("./classificationTesting.log", "r")
  # Variables
  testCode = -1
  modelCodeMatch = False
  mergeCodeMatch = False
  match = False
  # Read file
  lines = f.readlines()
  # Dissect lines 
  for line in lines:
    code,value = line.replace(" ","").replace("\n","").split(":")
    # Check testCode
    if code == "testCode":
      # First entry
      if testCode == -1:
        testCode = int(value)
      # Following entries
      else:
        # Check if previous testCode matches options
        match = modelCodeMatch and mergeCodeMatch
        if match:
          break
        # If previous testCode does not match options, update testCode and reset bools
        else:
          testCode = int(value)
          modelCodeMatch = False
          mergeCodeMatch = False
    # Check modelCode
    elif code == "modelCode":
      if modelCode == int(value): modelCodeMatch=True
    # Check mergeCode
    elif code == "mergeCode":
      if mergeCode == int(value): mergeCodeMatch=True
  # Close file
  f.close()
  # Update match in case it is the last entry
  match = modelCodeMatch and mergeCodeMatch
  # Check the matching testCode, otherwise create a new one
  if testCode == -1:
    # Create new entry
    testCode = 0
    match = False
  else:
    # If it does not match generate a new testCode
    if not match: testCode += 1
  
  # Logging 
  if match:
    print("Found test with exact same conditions...")
    print("Using testCode: "+str(testCode))
  else:
    print("No test with the same conditions found...")
    print("New testCode: "+str(testCode))
  
  # Return output
  return testCode, match

def updateTestingLog():
  print("----- Update classification testing log file -----")
  f = open("./classificationTesting.log", "a")
  f.write("testCode: " +str(testCode)+"\n")
  f.write("  modelCode: " +str(modelCode)+"\n")
  f.write("  mergeCode: "+str(mergeCode)+"\n")
  f.close()
  print("Added entry for testCode="+str(testCode))

 
#################################################
#
# Modelling Log
#
#################################################

def startModellingLog():
  print("----- Start classification modelling log file -----")
  
  # If log file does not exist, create one
  if not os.path.isfile("./classificationModelling.log"):
    f = open('./classificationModelling.log', 'w')
    f.close()
    print("Log file does not exist. Creating new log file...")
  else:
    print("Using existing log file...")

def searchModellingLog():
  print("----- Searching classification modelling log file -----")
  # Check existing entries
  f = open("./classificationModelling.log", "r")
  # Variables
  modelCode = -1
  mergeCodeMatch = False
  nnTypeMatch = False
  regTypeMatch = False
  nnComplexityMatch = False
  learningRateMatch = False
  nEpochsMatch = False
  batchSizeMatch = False
  validationSplitMatch = False
  particlesCodeMatch = False
  particleVarsCodeMatch = False
  inputsScaleMatch = False
  match = False
  # Read file
  lines = f.readlines()
  # Dissect lines 
  for line in lines:
    code,value = line.replace(" ","").replace("\n","").split(":")
    # Check modelCode
    if code == "modelCode":
      # First entry
      if modelCode == -1:
        modelCode = int(value)
      # Following entries
      else:
        # Check if previous modelCode matches options
        match1 = mergeCodeMatch and nnTypeMatch and regTypeMatch
        match2 = nnComplexityMatch
        match3 = learningRateMatch and nEpochsMatch and batchSizeMatch and validationSplitMatch
        match4 = particlesCodeMatch and particleVarsCodeMatch and inputsScaleMatch
        match = match1 and match2 and match3 and match4
        if match:
          break
        # If previous modelCode does not match options, update modelCode and reset bools
        else:
          modelCode = int(value)
          mergeCodeMatch = False
          nnTypeMatch = False
          regTypeMatch = False
          nnComplexityMatch = False
          learningRateMatch = False
          nEpochsMatch = False
          batchSizeMatch = False
          validationSplitMatch = False
          particlesCodeMatch = False
          particleVarsCodeMatch = False
          inputsScaleMatch = False
    # Check mergeCode
    elif code == "mergeCode":
      if mergeCode == int(value): mergeCodeMatch=True
    # Check nnType
    elif code == "nnType":
      if nnType == value: nnTypeMatch=True
    # Check regType
    elif code == "regType":
      if regType == value: regTypeMatch=True
    # Check nnComplexity
    elif code == "nnComplexity":
      if nnComplexity == int(value): nnComplexityMatch=True
    # Check learningRate
    elif code == "learningRate":
      if learningRate == float(value): learningRateMatch=True
    # Check nEpochs
    elif code == "nEpochs":
      if nEpochs == int(value): nEpochsMatch=True
    # Check batchSize
    elif code == "batchSize":
      if batchSize == int(value): batchSizeMatch=True
    # Check validationSplit
    elif code == "validationSplit":
      if validationSplit == float(value): validationSplitMatch=True
    # Check particlesCode
    elif code == "particlesCode":
      if particlesCode == int(value): particlesCodeMatch=True
    # Check particleVarsCode
    elif code == "particleVarsCode":
      if particleVarsCode == int(value): particleVarsCodeMatch=True
    # Check inputsScale
    elif code == "inputsScale":
      if inputsScale == value: inputsScaleMatch=True
  # Close file
  f.close()
  # Update match in case it is the last entry
  match1 = mergeCodeMatch and nnTypeMatch and regTypeMatch
  match2 = nnComplexityMatch
  match3 = learningRateMatch and nEpochsMatch and batchSizeMatch and validationSplitMatch
  match4 = particlesCodeMatch and particleVarsCodeMatch and inputsScaleMatch
  match = match1 and match2 and match3 and match4
  # Check the matching modelCode, otherwise create a new one
  if modelCode == -1:
    # Create new entry
    modelCode = 0
    match = False
  else:
    # If it does not match generate a new genCode
    if not match: modelCode += 1
  
  # Logging 
  if match:
    print("Found model with exact same conditions...")
    print("Using modelCode: "+str(modelCode))
  else:
    print("No generation with the same conditions found...")
    print("New modelCode: "+str(modelCode))
  
  # Return output
  return modelCode, match

def searchModelCode(modelCode):
  with open("./classificationModelling.log","r") as f:
    lines = f.readlines()
    for line in lines:
      if "modelCode" in line:
        code = int(line.split(":")[1].strip())
        if code == modelCode:
          return True
  return False

def searchParticlesCode(modelCode):
  global particlesCode
  with open("./classificationModelling.log","r") as f:
    lines = f.readlines()
    foundModelCode = False
    for line in lines:
      if "modelCode" in line:
        code = int(line.split(":")[1].strip())
        if code == modelCode:
          foundModelCode = True
      if foundModelCode:
        if "particlesCode" in line:
          val = int(line.split(":")[1].replace("\n","").strip())
          particlesCode = val
          break

def searchParticleVarsCode(modelCode):
  global particleVarsCode
  with open("./classificationModelling.log","r") as f:
    lines = f.readlines()
    foundModelCode = False
    for line in lines:
      if "modelCode" in line:
        code = int(line.split(":")[1].strip())
        if code == modelCode:
          foundModelCode = True
      if foundModelCode:
        if "particleVarsCode" in line:
          val = int(line.split(":")[1].replace("\n","").strip())
          particleVarsCode = val
          break

def searchInputsScale(modelCode):
  global inputsScale
  with open("./classificationModelling.log","r") as f:
    lines = f.readlines()
    foundModelCode = False
    for line in lines:
      if "modelCode" in line:
        code = int(line.split(":")[1].strip())
        if code == modelCode:
          foundModelCode = True
      if foundModelCode:
        if "inputsScale" in line:
          val = line.split(":")[1].replace("\n","").strip()
          inputsScale = val
          break


def updateModellingLog():
  print("----- Update classification modelling log file -----")
  f = open("./classificationModelling.log", "a")
  f.write("modelCode: " +str(modelCode)+"\n")
  f.write("  mergeCode: " +str(mergeCode)+"\n")
  f.write("  nnType: "+nnType+"\n")
  f.write("  regType: "+regType+"\n")
  f.write("  nnComplexity: "+str(nnComplexity)+"\n")
  f.write("  learningRate: "+str(learningRate)+"\n")
  f.write("  nEpochs: "+str(nEpochs)+"\n")
  f.write("  batchSize: "+str(batchSize)+"\n")
  f.write("  validationSplit: "+str(validationSplit)+"\n")
  f.write("  particlesCode: "+str(particlesCode)+"\n")
  f.write("  particleVarsCode: "+str(particleVarsCode)+"\n")
  f.write("  inputsScale: "+str(inputsScale)+"\n")
  f.close()
  print("Added entry for modelCode="+str(modelCode))


#################################################
#
# Testing
#
#################################################

def modelTester():
  print("----- Testing model -----")

  # Gather training and testing data
  print("Loading data...")
  testDF = pd.read_csv("../eventMerger/merges/mergeCode{}full.csv".format(mergeCode))
  
  # Inputs depend on particleVarsCode
  # Make sure we have the correct particleVarsCode for the given modelCode
  searchParticlesCode(modelCode)
  searchParticleVarsCode(modelCode)
  searchInputsScale(modelCode)

  # Generate inputs and labels matrices
  print("Preparing data...")
  testInputsNames, testInputs, testLabelsNames, testLabels, testWeights, testDFs = dataPreparation(testDF)
  if testInputsNames == None or testLabelsNames == None:
    print('Exiting model testing without testing...')
    return
  
  # Load the model's topography
  print("Loading model...")
  model = tf.keras.models.load_model("./classification/modelCode{}".format(modelCode))
  model.load_weights("./classification/modelCode{}.h5".format(modelCode))
   
  # Create directory to store information
  currentDirs = os.listdir("./")
  if "classification" not in currentDirs:
    os.system("mkdir classification")
  
  # Test model
  print("Testing model...")

  # General testing to ensure loss is similar
  testModel(model, testInputs, testLabels)

  # Make predictions
  outputs = model.predict(testInputs)
    
  # Denormalize ormalize predictions
  predictedColsNames = ["NNScore"]
  predictedDF = pd.DataFrame(outputs, columns=predictedColsNames, index=testDFs[0].index)

  # Update DF with predicted values
  for col in predictedColsNames:
    testDF[col] = predictedDF[col]

  # Output new dataframes
  currentDirs = os.listdir("./")
  if "datasets" not in currentDirs:
    os.system("mkdir datasets")
  outputName = "datasets/classificationTestCode{}Dataset.csv".format(testCode)
  testDF.to_csv(outputName, index=False)


#################################################
#
# Modelling
#
#################################################

def plotLossCurve(epochs, errors):
  """ Plot a curve of loss vs. epoch. """
  plt.figure()
  plt.xlabel("Epoch")
  plt.ylabel("Binary Cross-Entropy")
  plt.plot(epochs, errors[0], label="Training loss")
  plt.plot(epochs, errors[1], label="Validation loss")
  errorsMin = min([errors[0].min(), errors[1].min()])
  errorsMax = max([errors[0].max(), errors[1].max()])
  plt.xlim([0, nEpochs])
  plt.ylim([errorsMin*0.95, errorsMax*1.03])
  plt.text(0.55,0.9, "ATLAS", fontstyle='italic', fontweight='bold', transform=plt.gca().transAxes)
  plt.text(0.665,0.9, "Work In Progress", transform=plt.gca().transAxes)
  plt.text(0.55,0.84, "Classification Model {}".format(modelCode), transform=plt.gca().transAxes)
  plt.legend(loc=(0.59, 0.7), frameon=False)
  plt.savefig("./classification/modelCode{}Loss.png".format(modelCode), transparent=True, dpi=300)
  plt.savefig("./classification/modelCode{}Loss.pdf".format(modelCode), transparent=True)

def dataPreparation(dataset):
  """ Produce training/testing inputs and outputs """

  # Get particles in dataset
  columns = dataset.columns
  if particlesCode == 0:
    particles = ['q1A', 'q2A', 'q3A', 'q4A', 'q1B', 'q2B', 'q3B', 'q4B', 'l1B', 'met']
  elif particlesCode == 1:
    particles = ['q1A', 'q2A', 'q3A', 'q4A', 'q2B', 'l1B', 'met']
  datasetParticles = []
  for p in particles:
    for col in columns:
      if p in col:
        datasetParticles.append(p)
        break

  # Get visible objects variables to use
  particleVars = None
  if particleVarsCode == 0:
    particleVars = ['Pt', 'Eta', 'Phi', 'E']
  elif particleVarsCode == 1:
    particleVars = ['Px', 'Py', 'Pz', 'E']
  elif particleVarsCode == 2:
    particleVars = ['Pt', 'Eta', 'Phi', 'Px', 'Py', 'Pz', 'E']
  elif particleVarsCode == 3:
    particleVars = ["Pt"]
  
  # Check all requested variables exist in dataset
  inputNames = []
  if particleVars != None:
    for p in datasetParticles:
      for var in particleVars:
        inputNames.append(p+var)
        if p+var not in columns:
          print("Requested particle variable does not exist in dataset: "+p+var)
          return None, None, None, None, None, [None]
 
  # Get dataset outputs
  labelName = ['signal']

  # Check all requested outputs exist in dataset
  for l in labelName:
    if l not in columns:
      print("Requested output does not exist in dataset: "+l)
      return None, None, None, None, None, [None]
  
  # Shuffle dataset
  dataset = dataset.reindex(np.random.permutation(dataset.index)) # shuffle the examples!
  
  # Save and remove comments to calculate means
  comments = dataset['comment']
  dataset = dataset.drop(columns=['comment'])

  # Remove signal because we do not want to normalize those
  signals = dataset['signal']
  dataset = dataset.drop(columns=['signal'])

  # Update dataset inputs based on regression mass splittings
  if inputsScale == "massSplit":
    desCols = [x for x in dataset.columns if "Predicted-dM" in x]
    addedCols = None
    for col in desCols:
      if type(addedCols) == type(None):
        addedCols = dataset[col]
      else:
        addedCols += dataset[col]
    for inputN in inputNames:
      dataset[inputN] = dataset[inputN]/addedCols
  
  # Get mean and std for normalization purposes
  # Simple normalization convert values to their Z-scores assuming independent columns
  dsMean = dataset.mean()
  dsStd = dataset.std()
  dsNorm = (dataset - dsMean)/dsStd

  # Make sure columns with NaNs are all turned into 0.
  dsNorm = dsNorm.fillna(0)
  
  # Put comments and signals back
  dataset['comment'] = comments
  dsNorm['comment'] = comments
  dataset['signal'] = signals
  dsNorm['signal'] = signals

  # Get outputs
  label = [dataset[l] for l in labelName] # labels are not normalized
  label = np.asarray(label).T

  # Get inputs
  if nnType == "DNN":
    inputs = [np.array(dsNorm[i]) for i in inputNames]
    inputs = np.asarray(inputs).T

  # Get weights
  # Add them to dataset so they can be saved later on
  sigDF = dataset[dataset["signal"]==1]
  sigWeights = 139*1000*sigDF["eventWeight"]*sigDF["genWeight"]/sigDF["xsec"]
  bkgDF = dataset[dataset["signal"]==0]
  bkgWeights = 139*1000*bkgDF["eventWeight"]*bkgDF["genWeight"]
  dataset.loc[sigWeights.index, "classifierWeight"] = sigWeights
  dataset.loc[bkgWeights.index, "classifierWeight"] = bkgWeights
  weights = dataset["classifierWeight"]
  
  return inputNames, inputs, labelName, label, weights, [dataset, dsMean, dsStd, dsNorm]

def createDNNModel(numInputs, numOutputs):
  """ Create and compile a simple linear classification model. """

  # Most simple models are sequential
  model = tf.keras.models.Sequential()

  # Add input layer
  model.add(tf.keras.layers.InputLayer(input_shape=(numInputs,)))

  # Topology of the model
  kernel_regularizer = None
  if regType == 'L1':
    kernel_regularizer = tf.keras.regularizers.l1(0.0002)
  if regType == 'L2':
    kernel_regularizer = tf.keras.regularizers.l2(0.0002)
  nodes = []
  if nnComplexity == 0:
    nodes = [100, 200, 300, 200, 100, 100, 100]
  for i in range(len(nodes)):
    model.add(tf.keras.layers.Dense(units=nodes[i], 
                                    activation='relu', 
                                    kernel_regularizer=kernel_regularizer, 
                                    name='Hidden'+str(i+1)))

  # Model output layer
  model.add(tf.keras.layers.Dense(units=numOutputs, activation='sigmoid', name='Output'))# Output layer

  # Compile model
  loss = tf.keras.losses.BinaryCrossentropy()
  metric = tf.keras.metrics.BinaryCrossentropy()
  model.compile(optimizer=tf.keras.optimizers.legacy.Adam(learning_rate=learningRate), 
                loss=loss,
                metrics=[metric],
                weighted_metrics=[])

  # Done with model
  return model

def trainModel(model, modelCallbacks, inputs, labels, weights):
  """ Train a model with given loss function """

  # Train model
  history = model.fit(inputs, labels, sample_weight=weights, batch_size=batchSize, epochs=nEpochs, shuffle=True, validation_split=validationSplit, callbacks=modelCallbacks)

  # Track progress of the model
  epochs = history.epoch
  hist = pd.DataFrame(history.history)
  trainError = hist["binary_crossentropy"]
  valError = hist["val_binary_crossentropy"]

  # Done training and tracking model
  return epochs, trainError, valError

def testModel(model, inputs, labels):
  model.evaluate(inputs, labels, batch_size=batchSize)

def modelBuilder():
  print("----- Building model -----")

  # Create directory to store information
  currentDirs = os.listdir("./")
  if "classification" not in currentDirs:
    os.system("mkdir classification")
  if "datasets" not in currentDirs:
    os.system("mkdir datasets")

  # Gather training and testing data
  print("Loading data...")
  trainDF = pd.read_csv("../eventMerger/merges/mergeCode"+str(mergeCode)+"train.csv", low_memory=False)
  testDF = pd.read_csv("../eventMerger/merges/mergeCode"+str(mergeCode)+"test.csv", low_memory=False)
  
  # Generate inputs and labels matrices
  print("Preparing data...")
  trainInputsNames, trainInputs, trainLabelName, trainLabel, trainWeight, trainDFs = dataPreparation(trainDF)
  testInputsNames, testInputs, testLabelName, testLabel, testWeight, testDFs = dataPreparation(testDF)
  if trainInputsNames == None or trainLabelName == None or testInputsNames == None or testLabelName == None:
    print('Exiting model assembly without construction...')
    return

  # Establish the model's topography
  print("Creating model topography...")
  nInputs = len(trainInputsNames)
  nOutputs = len(trainLabelName)
  if nnType == "DNN":
    model = createDNNModel(nInputs, nOutputs)
  
  # Train the model on the normalized training set
  print("Training model...")
  modelCallbacks = [EarlyStopping(min_delta=0.0001, patience=5, restore_best_weights=True), 
                    ModelCheckpoint(filepath="./classification/modelCode{}.h5".format(modelCode), save_weights_only=True,save_best_only=True)]
  epochs, trainError, valError = trainModel(model, modelCallbacks, trainInputs, trainLabel, trainWeight)
  errors = [trainError, valError]

  # Plot loss function
  plotLossCurve(epochs, errors)
  
  # Save model
  print("Saving model...")
  try:
    model.load_weights("./classification/modelCode{}.h5".format(modelCode))
  except:
    print("Model is incomplete... weights file (.h5) not created")
  model.save("./classification/modelCode{}".format(modelCode))
  
  # Output train dataframe
  outputName = "./datasets/classificationModelCode{}Train.csv".format(modelCode)
  trainDF.to_csv(outputName, index=False)

  # Test model
  if modellingTests:
    print("Testing model...")

    # General testing to ensure loss is similar
    testModel(model, testInputs, testLabel)

    # Make predictions
    outputs = model.predict(testInputs)
    predictedColsNames = ["NNScore"]
    predictedDF = pd.DataFrame(outputs, columns=predictedColsNames, index=testDFs[0].index)
    
    # Update DF with predicted values
    for col in predictedColsNames:
      testDF[col] = predictedDF[col]

    # Get weights used for classifier
    weightColsNames = ["classifierWeight"]
    weightsDF = pd.DataFrame(testWeight, columns=weightColsNames, index=testDFs[0].index)
    
    # Update DF with classifier weights
    for col in weightColsNames:
      testDF[col] = weightsDF[col]

    # Output test dataframe
    outputName = "./datasets/classificationModelCode{}Test.csv".format(modelCode)
    testDF.to_csv(outputName, index=False)


#################################################
#
# Build model
#
#################################################

def main(**kwargs):
  '''
  Mandatory mode argument:
    1. mode:
        String that determines if we are going to test an existing model or create a new model
        "testing": Testing mode
        "modelling": Create new model mode
        Default: None

  If mode is testing:

    Mandatory arguments:
      1. modelCode:
          Model to test
      2. mergeCode:
          Merged dataset code corresponding to dataset to use for testing

  If mode is modelling:

    Mandatory model arguments:
      1. mergeCode:
          Merged dataset used for training and testing
    
    Optional model arguments are:
      1. nnType:
          Regression NN architecture to use
          "DNN": Deep neural network
          Default: "DNN"
      2. regType:
          Regularization to use during training of the neural network
          "No": Do not use regularization at various layers of the network
          "L1": Common L1 regularization
          "L2": Common L2 regularization
          Default: "L2"
      3. nnComplexity: 
          Neural network complexity based on number of layers and nodes
          0: 7 hidden layers with 100, 200, 300, 200, 100, 100, 100 nodes respectively
          Default: 0
      4. learningRate:
          Neural network training learning rate
          Default: 0.001
      5. nEpochs:
          How long the neural network is trained based on epochs
          Default: 10
      6. batchSize:
          How big each batch is during the neural network training
          Default: 50000
      7. validationSplit:
          What fraction of the training dataset is used for validation.
          Default: 0.3
      8. particlesCode:
          Code for particles to use as inputs to the NN if they exist.
          0: q1A, q2A, q3A, q4A, q1B, q2B, q3B, q4B, l1B, met
          1: q1A, q2A, q3A, q4A, q2B, l1B, met
          Default: 0
      9. particleVarsCode:
          Code for particle variables to use as inputs to the NN.
          0: pT, eta, phi, E
          1: px, py, pz, E
          2: pT, eta, phi, px, py, pz, E
          3: pT
          Default: 0
      10. inputsScale:
          Variable by which inputs are scaled
          "": No scaling
          "massSplit": Addition of all predicted mass splittings
          Default: ""

    Other arguments:
      1. modellingTests:
          Boolean used for testing newly generated model
          Default: True
  '''

  # Save mode
  saveMode(**kwargs)

  # Read mode
  readMode()
  if not checkMode():
    return

  # Execute tasks depending on the mode
  if mode == "testing":
    # Save testing arguments

    saveTestingArgs(**kwargs)  

    # Read the testing arguments
    readTestingArgs()
    if not checkTestingArgs():
      return
 
    # Start testing log file
    startTestingLog()

    # Search testing log file for similar tests
    global testCode
    testCode, match = searchTestingLog()
  
    # Test model
    modelTester()

    # Update testing log file
    if not match: updateTestingLog()

    # End of regression testing
    print("----- END OF CLASSIFICATION TESTING -----")
    print("Thanks for using our classification testing algorithm")
    print("Best,")
    print("Shion Chen and Luis Felipe Gutierrez")
  
  else:
    
    # Save modelling arguments
    saveModellingArgs(**kwargs)  

    # Read the modelling arguments
    readModellingArgs()
    if not checkModellingArgs():
      return
 
    # Start modelling log file
    startModellingLog()

    # Search modelling log file for similar models
    global modelCode
    modelCode, match = searchModellingLog()
   
    # Generate model
    modelBuilder()
  
    # Update modelling log file
    if not match: updateModellingLog()
  
    # End of regression NN
    print("----- END OF CLASSIFICATION MODELLING -----")
    print("Thanks for using our classification modelling algorithm")
    print("Best,")
    print("Shion Chen and Luis Felipe Gutierrez")


if __name__=="__main__":
  main(**dict(arg.split('=') for arg in sys.argv[1:]))
