# Neural Networks model builder

Package used for the generation and testing of regression and classification neural networks.

## Regression Neural Networks

* ```regression.py``` is used for generating new models or testing existing models.

#### Directly running program

* Testing mode:
```
python3 regression.py mode="testing" modelCode=0 datasetCode=0
```
See below description of allowed arguments and options.

* Modelling mode: 
```
python3 regression.py mode="modelling" genCode=0 [options]
```
See below description of allowed arguments and options.

#### Running shell scripts
* Testing mode: ```./testRegressionModels.sh```
* Modelling mode: ```./generateRegressionModels.sh```


#### Testing mode arguments and options

##### Mandatory arguments
```modelCode```
* Model to test

```datasetCode```
* Event extractor dataset code of dataset to use for testing

#### Modelling mode arguments and options

##### Mandatory model arguments
```genCode```
* Dataset used for training and testing of model.
* Mandatory option.
* See kinematic generator logs for existing genCode.
    
##### Optional model arguments
```nnType```
* Regression NN architecture to use.
* Allowed values: "DNN", "GNN"
* Default: "DNN"

```regType```
* Regularization to use during training of the neural network.
* Allowed values: "No", "L1", "L2"
* Default: "L2"

```lossType```
* Loss function to use for training of the neural network.
* Allowed values: "Absolute", "Squared"
* Default: "Squared"

```nnComplexity```
* Neural network complexity based on number of layers and nodes.
* Allowed values: 0
* Default: 0

```learningRate```
* Neural network training learning rate
* Allowed values: positive float greater than 0
* Default: 0.001

```nEpochs```
* How long the neural network is trained based on epochs
* Allowed values: positive integer greater than 0
* Default: 10

```batchSize```
* How big each batch is during the neural network training
* Allowed values: positive integer greater than 0
* Default: 10000

```validationSplit```
* What fraction of the training dataset is used for validation.
* Allowed values: positive float between 0 and 1
* Default: 0.3

```particlesCode```
* Code for particles to use as inputs to the NN if they exist.
* 0: q1A, q2A, q3A, q4A, q1B, q2B, q3B, q4B, l1B, met 
* 1: q1A, q2A, q3A, q4A, q2B, l1B, met
* Default: 0

```particleVarsCode```
* Code for particle variables to use as inputs to the NN.
* 0: pT, eta, phi, E
* 1: px, py, pz, E
* 2: pT, eta, phi, px, py, pz, E
* 3: pT
* Default: 0

```eventVarsCode```
* Code for event level variables to be used as inputs to the NN
* 0: None
* 1: Event shape variables - S, St, A, mTens1, mTens2, mTens3
* 2: Arbitrary variable - alpha
* 3: Arbitrary variable - alpha2
* Default: 0
    
```GNNTechnique```
* Technique used for feeding the GNN input to final regression stage
* "Summed": For each variable it considers the sum of every particle (particle ordering does not matter)
* "Flattened": Every variable is considered independently (particle ordering matters)
* Default: "Flattened"

##### Other arguments

```modellingTests```
* Boolean used for testing model
* Default: True


## Classification Neural Networks

* ```classification.py``` is used for generating new models or testing existing models.

#### Directly running program
* Testing mode:
```
python3 classification.py mode="testing" modelCode=0 mergeCode=0
```
See below description of allowed arguments and options.

* Modelling mode: 
```
python3 classification.py mode="modelling" mergeCode=0 [options]
```
See below description of allowed arguments and options.

#### Running shell scripts
* Testing mode: ```./testClassificationModels.sh```
* Modelling mode: ```./generateClassificationModels.sh```

#### Testing mode arguments and options

##### Mandatory arguments
```modelCode```
* Model to test

```mergeCode```
* Event merger dataset code of dataset to use for testing

#### Modelling mode arguments and options

##### Mandatory model arguments
```mergeCode```
* Dataset used for training and testing of model.
* Mandatory option.
* See kinematic generator logs for existing genCode.
    
##### Optional model arguments
```nnType```
* Regression NN architecture to use.
* Allowed values: "DNN"
* Default: "DNN"

```regType```
* Regularization to use during training of the neural network.
* Allowed values: "No", "L1", "L2"
* Default: "L2"

```nnComplexity```
* Neural network complexity based on number of layers and nodes.
* Allowed values: 0
* Default: 0

```learningRate```
* Neural network training learning rate
* Allowed values: positive float greater than 0
* Default: 0.001

```nEpochs```
* How long the neural network is trained based on epochs
* Allowed values: positive integer greater than 0
* Default: 10

```batchSize```
* How big each batch is during the neural network training
* Allowed values: positive integer greater than 0
* Default: 50000

```validationSplit```
* What fraction of the training dataset is used for validation.
* Allowed values: positive float between 0 and 1
* Default: 0.3

```particlesCode```
* Code for particles to use as inputs to the NN if they exist.
* 0: q1A, q2A, q3A, q4A, q1B, q2B, q3B, q4B, l1B, met 
* 1: q1A, q2A, q3A, q4A, q2B, l1B, met
* Default: 0

```particleVarsCode```
* Code for particle variables to use as inputs to the NN.
* 0: pT, eta, phi, E
* 1: px, py, pz, E
* 2: pT, eta, phi, px, py, pz, E
* 3: pT
* Default: 0

##### Other arguments

```modellingTests```
* Boolean used for testing model
* Default: True


## WARNINGs

* To avoid model code confusion, do not delete the ```.log``` files which keep track of model requests.
