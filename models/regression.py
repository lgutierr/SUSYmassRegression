import sys
import os
import pandas as pd
import numpy as np
import keras.backend as K
import tensorflow as tf
from tensorflow.keras import layers
from tensorflow.keras.callbacks import EarlyStopping, ModelCheckpoint
from tensorflow.keras.layers import Input, Dense, GRU, Add, Concatenate, BatchNormalization, Conv1D, Lambda, Dot, Flatten
from tensorflow.keras.models import Model
from matplotlib import pyplot as plt
from matplotlib import colors as mpColors
import atlas_mpl_style as ampl
ampl.use_atlas_style()


#################################################
#
# Config
#
#################################################

def searchParticles(particlesCode):
  with open("./config", "r") as f:
    particles = []
    lines = f.readlines()
    found = False
    for line in lines:
      if "particlesCode" in line:
        code = int(line.split(":")[1].strip())
        if code == particlesCode:
          found = True
      elif found:
        particles = eval(line.split(":")[1].strip())
        return particles

def searchParticleVars(particleVarsCode):
  with open("./config", "r") as f:
    particleVars = []
    lines = f.readlines()
    found = False
    for line in lines:
      if "particleVarsCode" in line:
        code = int(line.split(":")[1].strip())
        if code == particleVarsCode:
          found = True
      elif found:
        particleVars = eval(line.split(":")[1].strip())
        return particleVars

def searchEventVars(eventVarsCode):
  with open("./config", "r") as f:
    eventVars = []
    lines = f.readlines()
    found = False
    for line in lines:
      if "eventVarsCode" in line:
        code = int(line.split(":")[1].strip())
        if code == eventVarsCode:
          found = True
      elif found:
        eventVars = eval(line.split(":")[1].strip())
        return eventVars
   

#################################################
#
# Kinematic generator log
#
#################################################

def searchGenCode(genCode):
  with open("../kinematicGenerator/generator.log","r") as f:
    lines = f.readlines()
    for line in lines:
      if "genCode" in line:
        code = int(line.split(":")[1].strip())
        if code == genCode:
          return True
  return False


#################################################
#
# Event extractor log
#
#################################################

def searchDatasetCode(datasetCode):
  with open("../eventExtractor/extractor.log","r") as f:
    lines = f.readlines()
    for line in lines:
      if "datasetCode" in line:
        code = int(line.split(":")[1].strip())
        if code == datasetCode:
          return True
  return False

#################################################
#
# Mode
#
#################################################
mode = None

def saveMode(**kwargs):
  for m, v in kwargs.items():
    if m == "mode":
      try:
        global mode
        mode = v
      except:
        pass

def readMode():
  print("----- Regression mode -----")
  print(mode)
  return

def checkMode():
  print("----- Regression mode check -----")
  accepted = True

  # Check mode
  if mode == None:
    print("Please select a mode...")
    accepted = False
  elif mode not in ["testing", "modelling"]:
    print("Value of mode is not accepted...")
    print("mode accepted values: \"testing\", \"modelling\"")
    accepted = False
  
  # Accepted values
  if accepted:
    print("Selected mode OK... Proceeding with next steps")
  else:
    print("Mode not accepted...")
    print("Exiting program...")
  return accepted


#################################################
#
# Testing Arguments
#
#################################################
testCode = -1
datasetCode = 0

def saveTestingArgs(**kwargs):
  for a, v in kwargs.items():
    if a == "modelCode":
      try:
        global modelCode
        modelCode = int(v)
      except:
        pass
    elif a == "datasetCode":
      try:
        global datasetCode
        datasetCode = int(v)
      except:
        pass

def readTestingArgs():
  print("----- Regression testing arguments -----")
  print("modelCode: "+str(modelCode))
  print("datasetCode: "+str(datasetCode))
  return

def checkTestingArgs():
  print("----- Regression testing arguments check -----")
  accepted = True
  
  # Check model code
  if not searchModelCode(modelCode):
    print("Used modelCode does not exist...")
    print("See regression modelling logs for existing modelCode")
    accepted = False

  # Check dataset code
  if not searchDatasetCode(datasetCode):
    print("Dataset does not exist...")
    print("See event extractor logs for existing datasetCode")
    accepted = False

  # Accepted values
  if accepted:
    print("All testing arguments are OK... Proceeding with next steps")
  else:
    print("Some testing arguments are not accepted...")
    print("Exiting program...")
  return accepted
  

#################################################
#
# Modelling Arguments
#
#################################################
modelCode = -1
genCode = None
nnType = "DNN"
regType = "L2"
lossType = "Squared"
nnComplexity = 0
learningRate = 0.001
validationSplit = 0.3
nEpochs = 10
batchSize = 10000
particlesCode = 0
particleVarsCode = 0
eventVarsCode = 0
GNNTechnique = "Flattened"
modellingTests = True

def saveModellingArgs(**kwargs):
  for a, v in kwargs.items():
    if a == "genCode":
      try:
        global genCode
        genCode = int(v)
      except:
        pass
    elif a == "nnType":
      try:
        global nnType
        nnType = v
      except:
        pass
    elif a == "regType":
      try:
        global regType
        regType = v
      except:
        pass
    elif a == "lossType":
      try:
        global lossType
        lossType = v
      except:
        pass
    elif a == "nnComplexity":
      try:
        global nnComplexity
        nnComplexity = int(v)
      except:
        pass
    elif a == "learningRate":
      try:
        global learningRate
        learningRate = float(v)
      except:
        pass
    elif a == "nEpochs":
      try:
        global nEpochs
        nEpochs = int(v)
      except:
        pass
    elif a == "batchSize":
      try:
        global batchSize
        batchSize = int(v)
      except:
        pass
    elif a == "validationSplit":
      try:
        global validationSplit
        validationSplit = float(v)
      except:
        pass
    elif a == "particlesCode":
      try:
        global particlesCode
        particlesCode = int(v)
      except:
        pass
    elif a == "particleVarsCode":
      try:
        global particleVarsCode
        particleVarsCode = int(v)
      except:
        pass
    elif a == "eventVarsCode":
      try:
        global eventVarsCode
        eventVarsCode = int(v)
      except:
        pass
    elif a == "GNNTechnique":
      try:
        global GNNTechnique
        GNNTechnique = v
      except:
        pass
    elif a == "modellingTests":
      try:
        global modellingTests
        modellingTests = eval(v)
      except:
        pass

def readModellingArgs():
  print("----- Regression modelling arguments -----")
  print("genCode: "+str(genCode))
  print("nnType: "+nnType)
  print("regType: "+regType)
  print("lossType: "+lossType)
  print("nnComplexity: "+str(nnComplexity))
  print("learningRate: "+str(learningRate))
  print("nEpochs: "+str(nEpochs))
  print("batchSize: "+str(batchSize))
  print("validationSplit: "+str(validationSplit))
  print("particlesCode: "+str(particlesCode))
  print("particleVarsCode: "+str(particleVarsCode))
  print("eventVarsCode: "+str(eventVarsCode))
  if nnType == "GNN":
    print("GNNTechnique: "+GNNTechnique)
  return

def checkModellingArgs():
  print("----- Regression modelling arguments check -----")
  accepted = True

  # Check kinematic generator code
  if not searchGenCode(genCode):
    print("Used genCode does not exist...")
    print("See kinematic generator logs for existing genCode")
    accepted = False
  
  # Check neural network type
  if nnType not in ["DNN", "GNN"]:
    print("Value of nnType is not accepted...")
    print("nnType accepted values: (default) \"DNN\", \"GNN\"")
    accepted = False

  # Check regularization type
  if regType not in ["No", "L1", "L2"]:
    print("Value of regType is not accepted...")
    print("regType accepted values: \"No\", \"L1\", or \"L2\"")
    accepted = False

  # Check loss type
  if lossType not in ["Absolute", "Squared"]:
    print("Value of lossType is not accepted...")
    print("lossType accepted values: \"Absolute\", or (default) \"Squared\"")
    accepted = False

  # Check neural network complexity
  if nnComplexity not in [0]:
    print("Value of nnComplexity is not accepted...")
    print("nnComplexity accepted values: (default) 0")
    accepted = False

  # Check learning rate
  if learningRate < 0:
    print("Value of learningRate is not accepted...")
    print("learningRate accepted values: positive float, (default) 0.001")
    accepted = False
  
  # Check number epochs
  if nEpochs < 0:
    print("Value of nEpochs is not accepted...")
    print("nEpochs accepted values: positive integer, (default) 10")
    accepted = False
  
  # Check batch size
  if batchSize < 0:
    print("Value of batchSize is not accepted...")
    print("batchSize accepted values: positive integer, (default) 10000")
    accepted = False
  
  # Check validation split
  if validationSplit < 0 or validationSplit > 1:
    print("Value of validationSplit is not accepted...")
    print("validationSplit accepted values: positive float in range [0,1], (default) 10000")
    accepted = False
  
  # Check particles code
  if particlesCode not in [0,1]:
    print("Value of particlesCode is not accepted...")
    print("particlesCode accepted values: (default) 0, 1")
    accepted = False
  
  # Check particle variables code
  if particleVarsCode not in [0,1,2,3]:
    print("Value of particleVarsCode is not accepted...")
    print("particleVarsCode accepted values: (default) 0, 1, 2, 3")
    accepted = False
  
  # Check event variables code
  if eventVarsCode not in [0,1,2,3]:
    print("Value of eventVarsCode is not accepted...")
    print("eventVarsCode accepted values: (default) 0, 1, 2, 3")
    accepted = False

  # Check GNN technique
  if nnType == "GNN" and GNNTechnique not in ["Summed", "Flattened"]:
    print("Value of GNNTechnique is not accepted...")
    print("GNNTechnique accepted values: \"Summed\", (default) \"Flattened\"")
    accept = False

  # Accepted values
  if accepted:
    print("All options are OK... Proceeding with next steps")
  else:
    print("Some options are not accepted...")
    print("Exiting program without model construction...")
  return accepted

#################################################
#
# Testing Log
#
#################################################

def startTestingLog():
  print("----- Start regression testing log file -----")
  
  # If log file does not exist, create one
  if not os.path.isfile("./regressionTesting.log"):
    f = open('./regressionTesting.log', 'w')
    f.close()
    print("Log file does not exist. Creating new log file...")
  else:
    print("Using existing log file...")

def searchTestingLog():
  print("----- Searching regression testing log file -----")
  # Check existing entries
  f = open("./regressionTesting.log", "r")
  # Variables
  testCode = -1
  modelCodeMatch = False
  datasetCodeMatch = False
  match = False
  # Read file
  lines = f.readlines()
  # Dissect lines 
  for line in lines:
    code,value = line.replace(" ","").replace("\n","").split(":")
    # Check testCode
    if code == "testCode":
      # First entry
      if testCode == -1:
        testCode = int(value)
      # Following entries
      else:
        # Check if previous testCode matches options
        match1 = modelCodeMatch 
        match2 = datasetCodeMatch
        #and resonanceMsMatch
        match = match1 and match2
        if match:
          break
        # If previous testCode does not match options, update testCode and reset bools
        else:
          testCode = int(value)
          modelCodeMatch = False
          datasetCodeMatch = False
    # Check modelCode
    elif code == "modelCode":
      if modelCode == int(value): modelCodeMatch=True
    # Check datasetCode
    elif code == "datasetCode":
      if datasetCode == int(value): datasetCodeMatch=True
  # Close file
  f.close()
  # Update match in case it is the last entry
  match1 = modelCodeMatch 
  match2 = datasetCodeMatch 
  #and resonanceMsMatch
  match = match1 and match2
  # Check the matching testCode, otherwise create a new one
  if testCode == -1:
    # Create new entry
    testCode = 0
    match = False
  else:
    # If it does not match generate a new testCode
    if not match: testCode += 1
  
  # Logging 
  if match:
    print("Found test with exact same conditions...")
    print("Using testCode: "+str(testCode))
  else:
    print("No test with the same conditions found...")
    print("New testCode: "+str(testCode))
  
  # Return output
  return testCode, match

def updateTestingLog():
  print("----- Update regression testing log file -----")
  f = open("./regressionTesting.log", "a")
  f.write("testCode: " +str(testCode)+"\n")
  f.write("  modelCode: " +str(modelCode)+"\n")
  f.write("  datasetCode: "+str(datasetCode)+"\n")
  f.close()
  print("Added entry for testCode="+str(testCode))

 
#################################################
#
# Modelling Log
#
#################################################

def startModellingLog():
  print("----- Start regression modelling log file -----")
  
  # If log file does not exist, create one
  if not os.path.isfile("./regressionModelling.log"):
    f = open('./regressionModelling.log', 'w')
    f.close()
    print("Log file does not exist. Creating new log file...")
  else:
    print("Using existing log file...")

def searchModellingLog():
  print("----- Searching regression modelling log file -----")
  # Check existing entries
  f = open("./regressionModelling.log", "r")
  # Variables
  modelCode = -1
  genCodeMatch = False
  nnTypeMatch = False
  regTypeMatch = False
  lossTypeMatch = False
  nnComplexityMatch = False
  learningRateMatch = False
  nEpochsMatch = False
  batchSizeMatch = False
  validationSplitMatch = False
  particlesCodeMatch = False
  particleVarsCodeMatch = False
  eventVarsCodeMatch = False
  GNNTechniqueMatch = False
  match = False
  # Read file
  lines = f.readlines()
  # Dissect lines 
  for line in lines:
    code,value = line.replace(" ","").replace("\n","").split(":")
    # Check modelCode
    if code == "modelCode":
      # First entry
      if modelCode == -1:
        modelCode = int(value)
      # Following entries
      else:
        # Check if previous modelCode matches options
        match1 = genCodeMatch and nnTypeMatch and regTypeMatch
        match2 = lossTypeMatch and nnComplexityMatch
        match3 = learningRateMatch and nEpochsMatch and batchSizeMatch and validationSplitMatch
        match4 = particlesCodeMatch and particleVarsCodeMatch and eventVarsCodeMatch
        if nnType == "GNN":
          match = match1 and match2 and match3 and match4 and GNNTechniqueMatch
        else:
          match = match1 and match2 and match3 and match4
        if match:
          break
        # If previous modelCode does not match options, update genCode and reset bools
        else:
          modelCode = int(value)
          genCodeMatch = False
          nnTypeMatch = False
          regTypeMatch = False
          lossTypeMatch = False
          nnComplexityMatch = False
          learningRateMatch = False
          nEpochsMatch = False
          batchSizeMatch = False
          validationSplitMatch = False
          particlesCodeMatch = False
          particleVarsCodeMatch = False
          eventVarsCodeMatch = False
          GNNTechniqueMatch = False
    # Check genCode
    elif code == "genCode":
      if genCode == int(value): genCodeMatch=True
    # Check nnType
    elif code == "nnType":
      if nnType == value: nnTypeMatch=True
    # Check regType
    elif code == "regType":
      if regType == value: regTypeMatch=True
    # Check lossType
    elif code == "lossType":
      if lossType == value: lossTypeMatch=True
    # Check nnComplexity
    elif code == "nnComplexity":
      if nnComplexity == int(value): nnComplexityMatch=True
    # Check learningRate
    elif code == "learningRate":
      if learningRate == float(value): learningRateMatch=True
    # Check nEpochs
    elif code == "nEpochs":
      if nEpochs == int(value): nEpochsMatch=True
    # Check batchSize
    elif code == "batchSize":
      if batchSize == int(value): batchSizeMatch=True
    # Check validationSplit
    elif code == "validationSplit":
      if validationSplit == float(value): validationSplitMatch=True
    # Check particlesCode
    elif code == "particlesCode":
      if particlesCode == int(value): particlesCodeMatch=True
    # Check particleVarsCode
    elif code == "particleVarsCode":
      if particleVarsCode == int(value): particleVarsCodeMatch=True
    # Check eventVarsCode
    elif code == "eventVarsCode":
      if eventVarsCode == int(value): eventVarsCodeMatch=True
    # Check GNNTechnique
    elif code == "GNNTechnique":
      if GNNTechnique == value: GNNTechniqueMatch=True
  # Close file
  f.close()
  # Update match in case it is the last entry
  match1 = genCodeMatch and nnTypeMatch and regTypeMatch
  match2 = lossTypeMatch and nnComplexityMatch
  match3 = learningRateMatch and nEpochsMatch and batchSizeMatch and validationSplitMatch
  match4 = particlesCodeMatch and particleVarsCodeMatch and eventVarsCodeMatch
  if nnType == "GNN":
    match = match1 and match2 and match3 and match4 and GNNTechniqueMatch
  else:
    match = match1 and match2 and match3 and match4
  # Check the matching modelCode, otherwise create a new one
  if modelCode == -1:
    # Create new entry
    modelCode = 0
    match = False
  else:
    # If it does not match generate a new genCode
    if not match: modelCode += 1
  
  # Logging 
  if match:
    print("Found model with exact same conditions...")
    print("Using modelCode: "+str(modelCode))
  else:
    print("No generation with the same conditions found...")
    print("New modelCode: "+str(modelCode))
  
  # Return output
  return modelCode, match

def searchModelCode(modelCode):
  with open("./regressionModelling.log","r") as f:
    lines = f.readlines()
    for line in lines:
      if "modelCode" in line:
        code = int(line.split(":")[1].strip())
        if code == modelCode:
          return True
  return False

def searchParticlesCode(modelCode):
  global particlesCode
  with open("./regressionModelling.log","r") as f:
    lines = f.readlines()
    foundModelCode = False
    for line in lines:
      if "modelCode" in line:
        code = int(line.split(":")[1].strip())
        if code == modelCode:
          foundModelCode = True
      if foundModelCode:
        if "particlesCode" in line:
          val = int(line.split(":")[1].replace("\n","").strip())
          particlesCode = val
          break

def searchParticleVarsCode(modelCode):
  global particleVarsCode
  with open("./regressionModelling.log","r") as f:
    lines = f.readlines()
    foundModelCode = False
    for line in lines:
      if "modelCode" in line:
        code = int(line.split(":")[1].strip())
        if code == modelCode:
          foundModelCode = True
      if foundModelCode:
        if "particleVarsCode" in line:
          val = int(line.split(":")[1].replace("\n","").strip())
          particleVarsCode = val
          break

def updateModellingLog():
  print("----- Update regression modelling log file -----")
  f = open("./regressionModelling.log", "a")
  f.write("modelCode: " +str(modelCode)+"\n")
  f.write("  genCode: " +str(genCode)+"\n")
  f.write("  nnType: "+nnType+"\n")
  f.write("  regType: "+regType+"\n")
  f.write("  lossType: "+lossType+"\n")
  f.write("  nnComplexity: "+str(nnComplexity)+"\n")
  f.write("  learningRate: "+str(learningRate)+"\n")
  f.write("  nEpochs: "+str(nEpochs)+"\n")
  f.write("  batchSize: "+str(batchSize)+"\n")
  f.write("  validationSplit: "+str(validationSplit)+"\n")
  f.write("  particlesCode: "+str(particlesCode)+"\n")
  f.write("  particleVarsCode: "+str(particleVarsCode)+"\n")
  f.write("  eventVarsCode: "+str(eventVarsCode)+"\n")
  if nnType == "GNN":
    f.write("  GNNTechnique: "+str(GNNTechnique)+"\n")
  f.close()
  print("Added entry for modelCode="+str(modelCode))


#################################################
#
# Testing
#
#################################################

def modelTester():
  print("----- Testing model -----")
  
  # Create directory to store information
  currentDirs = os.listdir("./")
  if "regression" not in currentDirs:
    os.system("mkdir regression")
  if "datasets" not in currentDirs:
    os.system("mkdir datasets")

  # Gather training and testing data
  print("Loading data...")
  testDF = pd.read_csv("../eventExtractor/datasets/datasetCode{}.csv".format(datasetCode))
  
  # Inputs depend on particlesCode and particleVarsCode
  # Make sure we have the correct particlesCode and particleVarsCode for the given modelCode
  searchParticlesCode(modelCode)
  searchParticleVarsCode(modelCode)

  # Generate inputs and labels matrices
  print("Preparing data...")
  testInputsNames, testInputs, testLabelsNames, testLabels, testDFs = dataPreparation(testDF)
  if testInputsNames == None or testLabelsNames == None:
    print('Exiting model testing without testing...')
    return
  
  # Load the model's topography
  print("Loading model...")
  model = tf.keras.models.load_model("./regression/modelCode{}".format(modelCode))
  model.load_weights("./regression/modelCode{}.h5".format(modelCode))
  
  # Test model
  print("Testing model...")

  # General testing to ensure loss is similar
  testModel(model, testInputs, testLabels)

  # Make predictions
  outputs = model.predict(testInputs)
  predictedColsNames = ["Predicted-"+n for n in testLabelsNames]
  predictedDF = pd.DataFrame(outputs, columns=predictedColsNames, index=testDFs[0].index)

  # Update DF with predicted values
  for col in predictedColsNames:
    testDF[col] = predictedDF[col]

  # Output new dataframes
  outputName = "datasets/regressionTestCode{}.csv".format(testCode)
  testDF.to_csv(outputName, index=False)


#################################################
#
# Modelling
#
#################################################

def dataPreparation(dataset):
  """ Produce training/testing inputs and outputs """

  # Get particles in dataset
  columns = dataset.columns
  particles = searchParticles(particlesCode)
  datasetParticles = []
  for p in particles:
    for col in columns:
      if p in col:
        datasetParticles.append(p)
        break

  # Get visible objects variables to use
  particleVars = searchParticleVars(particleVarsCode)
  
  # Get event level variables
  eventVars = searchEventVars(eventVarsCode)
  
  # Check all requested variables exist in dataset
  inputNames = []
  if particleVars != None:
    for p in datasetParticles:
      for var in particleVars:
        inputNames.append(p+var)
        if p+var not in columns:
          print("Requested particle variable does not exist in dataset: "+p+var)
          return None, None, None, None, [None]
  if eventVars != [None]:
    for var in eventVars:
      inputNames.append(var)
      if var not in columns:
        print("Requested event variable does not exist in dataset: "+var)
        return None, None, None, None, [None]

  # Get dataset outputs
  outputs = ['dM1', 'dM2']
  labelNames = []
  for o in outputs:
    for col in columns:
      if o in col:
        labelNames.append(o+"_1")
        break

  # Check all requested outputs exist in dataset
  if labelNames != []:
    for l in labelNames:
      if l not in columns:
        print("Requested output does not exist in dataset: "+l)
        return None, None, None, None, [None]
  
  # Shuffle dataset
  dataset = dataset.reindex(np.random.permutation(dataset.index)) # shuffle the examples!

  # Save and remove comments to calculate means
  try:
    comments = dataset['comment']
    dataset = dataset.drop(columns=['comment'])
  except:
    pass
  
  # Get mean and std for normalization purposes
  # Simple normalization convert values to their Z-scores assuming independent columns
  dsMean = dataset.mean()
  dsStd = dataset.std()
  dsNorm = (dataset - dsMean)/dsStd
  
  # Make sure columns with NaNs are all turned into 0.
  dsNorm = dsNorm.fillna(0)
  
  # Put comments back
  try:
    dataset['comment'] = comments
    dsNorm['comment'] = comments
  except:
    pass

  # Get outputs
  labels = [dataset[l] for l in labelNames] # labels are not normalized
  labels = np.asarray(labels).T

  # Get inputs
  if nnType == "DNN":
    inputs = [np.array(dsNorm[i]) for i in inputNames]
    inputs = np.asarray(inputs).T
  elif nnType == "GNN":
    inputs = []
    for i in range(len(particleVars)):
      row = []
      # Regular particles with all the variables
      for j in range(len(datasetParticles)):
        row.append(dsNorm[datasetParticles[j]+particleVars[i]])
      inputs.append(row)
    inputs = np.transpose(inputs, axes=(2,1,0))

  return inputNames, inputs, labelNames, labels, [dataset, dsMean, dsStd, dsNorm]

def createDNNModel(numInputs, numOutputs):
  """ Create and compile a simple linear regression model. """

  # Most simple models are sequential
  model = tf.keras.models.Sequential()

  # Add input layer
  model.add(tf.keras.layers.InputLayer(input_shape=(numInputs,)))

  # Topology of the model
  kernel_regularizer = None
  if regType == 'L1':
    kernel_regularizer = tf.keras.regularizers.l1(0.0002)
  if regType == 'L2':
    kernel_regularizer = tf.keras.regularizers.l2(0.0002)
  nodes = []
  if nnComplexity == 0:
    nodes = [100, 200, 300, 200, 100, 100, 100]
  for i in range(len(nodes)):
    model.add(tf.keras.layers.Dense(units=nodes[i], 
                                    activation='relu', 
                                    kernel_regularizer=kernel_regularizer, 
                                    name='Hidden'+str(i+1)))

  # Model output layer
  model.add(tf.keras.layers.Dense(units=numOutputs, name='Output'))# Output layer

  # Compile model
  loss = None
  metric = None
  if lossType == 'Squared':
    loss = 'mean_squared_error'
    metric = tf.keras.metrics.MeanSquaredError()
  elif lossType == 'Absolute':
    loss = 'mean_absolute_error'
    metric = tf.keras.metrics.MeanAbsoluteError()
  model.compile(optimizer=tf.keras.optimizers.legacy.Adam(learning_rate=learningRate), 
                loss=loss,
                metrics=[metric])

  # Done with model
  return model

def interactionMatrix(nParticles):
  """ Define the receiving and sending interaction matrix. """
  
  # Number of objects and edges
  Ne = nParticles * (nParticles - 1) # Number of edges
  No = nParticles                    # Number of objects/particles

  # Receiving matrix
  RR = []
  for i in range(No):
    row = []
    for j in range(Ne):
      if j in range(i*(No - 1), (i+1)*(No-1)):
        row.append(1.0)
      else:
        row.append(0.0)
    RR.append(row)
  RR = np.array(RR)
  RR = np.float32(RR)
  RRT = np.transpose(RR)

  # Sending matrix
  RST = []
  for i in range(No):
    for j in range(No):
      row = []
      for k in range(No):
        if k == j:
          row.append(1.0)
        else:
          row.append(0.0)
      RST.append(row)
  rowsToRemove = []
  for i in range(No):
    rowsToRemove.append(i*(No+1))
  RST = np.array(RST)
  RST = np.float32(RST)
  RST = np.delete(RST, rowsToRemove,0)
  RS = np.transpose(RST)
  
  # Return all matrices
  return RR, RRT, RS, RST

def createGNNModel(nParticles, nVars, nOutputs):
  """ Create GNN model topology obtained from JEDI-net """
  # Define the input matrix
  I = Input(shape=(nParticles, nVars), name="i")
  # Get interaction matrices
  RR, RRT, RS, RST = interactionMatrix(nParticles)
  # Get B matrix
  IdotRR = Lambda(lambda tensor: tf.transpose(tf.tensordot(tf.transpose(tensor, perm=(0, 2, 1)), RR, axes=[[2], [0]]),
                                            perm=(0, 2, 1)), name="idotRR")(I)
  IdotRS = Lambda(lambda tensor: tf.transpose(tf.tensordot(tf.transpose(tensor, perm=(0, 2, 1)), RS, axes=[[2], [0]]),
                                            perm=(0, 2, 1)), name="idotRS")(I)
  B = Lambda(lambda tensorList: tf.concat((tensorList[0], tensorList[1]), axis=2), name="b")([IdotRR, IdotRS])
  # Reduce dimension of B using convolutions
  convOneParticle = Conv1D(80, kernel_size=1, activation="relu", name="convOneParticle")(B)
  convTwoParticle = Conv1D(50, kernel_size=1, activation="relu", name="convTwoParticle")(convOneParticle)
  convThreeParticle = Conv1D(30, kernel_size=1, activation="relu", name="convThreeParticle")(convTwoParticle)
  # Get E matrix
  E = BatchNormalization(momentum=0.6, name="e")(convThreeParticle)
  # Get E-bar matrix
  EBar = Lambda(lambda tensor: tf.transpose(tf.tensordot(tf.transpose(tensor, perm=(0, 2, 1)), RRT, axes=[[2], [0]]),
                                            perm=(0, 2, 1)), name="eBar")(E)
  # Get combined matrix C
  C = Lambda(lambda listOfTensors: tf.concat((listOfTensors[0], listOfTensors[1]), axis=2), name="c")(
    [I, EBar])
  # Reduce dimension of C using convolutions
  convPredictOne = Conv1D(80, kernel_size=1, activation="relu", name="convPredictOne")(C)
  convPredictTwo = Conv1D(50, kernel_size=1, activation="relu", name="convPredictTwo")(convPredictOne)
  O = Conv1D(24, kernel_size=1, activation="relu", name="o")(convPredictTwo)
  # Calculate input to final regression neural network
  OBar = None
  if GNNTechnique == "Summed":
    OBar = Lambda(lambda tensor: K.sum(tensor, axis=1), name="oBar")(O)
  elif GNNTechnique == "Flattened":
    OBar = Flatten()(O)
  # Final regression task
  kernel_regularizer = None
  if regType == 'L1':
    kernel_regularizer = tf.keras.regularizers.l1(0.0002)
  if regType == 'L2':
    kernel_regularizer = tf.keras.regularizers.l1(0.0002)
  fRegOne = Dense(300, activation="relu", kernel_regularizer=kernel_regularizer, name="fRegOne")(OBar)
  fRegTwo = Dense(400, activation="relu", kernel_regularizer=kernel_regularizer, name="fRegTwo")(fRegOne)
  fRegThree = Dense(500, activation="relu", kernel_regularizer=kernel_regularizer, name="fRegThree")(fRegTwo)
  fRegFour = Dense(400, activation="relu", kernel_regularizer=kernel_regularizer, name="fRegFour")(fRegThree)
  fRegFive = Dense(300, activation="relu", kernel_regularizer=kernel_regularizer, name="fRegFive")(fRegFour)
  fRegSix = Dense(200, activation="relu", kernel_regularizer=kernel_regularizer, name="fRegSix")(fRegFive)
  fRegSeven = Dense(100, activation="relu", kernel_regularizer=kernel_regularizer, name="fRegSeven")(fRegSix)
  output = Dense(nOutputs, name="output")(fRegSeven)
  # Define model
  model = Model(inputs=[I], outputs=[output])
  # Compile model
  loss = None
  metric = None
  if lossType == 'Squared':
    loss = 'mean_squared_error'
    metric = tf.keras.metrics.MeanSquaredError()
  elif lossType == 'Absolute':
    loss = 'mean_absolute_error'
    metric = tf.keras.metrics.MeanAbsoluteError()
  #model.compile(optimizer='rmsprop', loss=loss, metrics=[metric])
  model.compile(optimizer=tf.keras.optimizers.legacy.Adam(learning_rate=0.001), loss=loss, metrics=[metric])
  return model 

def trainModel(model, modelCallbacks, inputs, labels):
  """ Train a model with given loss function """
  history = model.fit(inputs, labels, batch_size=batchSize, epochs=nEpochs, shuffle=True, validation_split=validationSplit, callbacks=modelCallbacks)
  # Track progress of the model
  epochs = history.epoch
  hist = pd.DataFrame(history.history)
  trainError = None
  valError = None
  if lossType == 'Squared':
    trainError = hist["mean_squared_error"]
    valError = hist["val_mean_squared_error"]
  elif lossType == 'Absolute':
    trainError = hist["mean_absolute_error"]
    valError = hist["val_mean_absolute_error"]
  # Done training and tracking model
  return epochs, trainError, valError

def testModel(model, inputs, labels):
  model.evaluate(inputs, labels, batch_size=batchSize)

def modelBuilder():
  print("----- Building model -----")
  
  # Create directory to store information
  currentDirs = os.listdir("./")
  if "regression" not in currentDirs:
    os.system("mkdir regression")
  if "datasets" not in currentDirs:
    os.system("mkdir datasets")

  # Gather training and testing data
  print("Loading data...")
  trainDF = pd.read_csv("../kinematicGenerator/data/genCode"+str(genCode)+"train.csv")
  testDF = pd.read_csv("../kinematicGenerator/data/genCode"+str(genCode)+"test.csv")

  # Generate inputs and labels matrices
  print("Preparing data...")
  trainInputsNames, trainInputs, trainLabelsNames, trainLabels, trainDFs = dataPreparation(trainDF)
  testInputsNames, testInputs, testLabelsNames, testLabels, testDFs = dataPreparation(testDF)
  if trainInputsNames == None or trainLabelsNames == None or testInputsNames == None or testLabelsNames == None:
    print('Exiting model assembly without construction...')
    return

  # Establish the model's topography
  print("Creating model topography...")
  nInputs = len(trainInputsNames)
  nOutputs = len(trainLabelsNames)
  if nnType == "DNN":
    model = createDNNModel(nInputs, nOutputs)
  elif nnType == "GNN":
    particles = searchParticles(particlesCode) 
    particleVars = searchParticleVars(particleVarsCode)
    nParticles = len(particles)
    nVars = len(particleVars)
    model = createGNNModel(nParticles, nVars, nOutputs)

  # Train the model on the normalized training set
  print("Training model...")
  modelCallbacks = [EarlyStopping(min_delta=0.0001, patience=5, restore_best_weights=True), 
                    ModelCheckpoint(filepath="./regression/modelCode{}.h5".format(modelCode), save_weights_only=True,save_best_only=True)]
  epochs, trainError, valError = trainModel(model, modelCallbacks, trainInputs, trainLabels)
  errors = [trainError, valError]

  # Plot loss function
  plotLossCurve(epochs, errors)
  
  # Save model
  print("Saving model...")
  try:
    model.load_weights("./regression/modelCode{}.h5".format(modelCode))
  except:
    print("Model is incomplete... weights file (.h5) not created")
  model.save("./regression/modelCode{}".format(modelCode))
  
  # Save train dataframe
  outputName = "./datasets/regressionModelCode{}Train.csv".format(modelCode)
  trainDF.to_csv(outputName, index=False)

  # Test model
  if modellingTests:
    print("Testing model...")
    
    # General testing to ensure loss is similar
    testModel(model, testInputs, testLabels)

    # Make predictions
    outputs = model.predict(testInputs)
    predictedColsNames = ["Predicted-"+n for n in testLabelsNames]
    predictedDF = pd.DataFrame(outputs, columns=predictedColsNames, index=testDFs[0].index)

    # Update DF with predicted values
    for col in predictedColsNames:
      testDF[col] = predictedDF[col]

    # Output test dataframe
    outputName = "./datasets/regressionModelCode{}Test.csv".format(modelCode)
    testDF.to_csv(outputName, index=False)
  

#################################################
#
# Plots
#
#################################################

def plotLossCurve(epochs, errors):
  """ Plot a curve of loss vs. epoch. """
  
  # Start fresh plot
  plt.close()
  plt.figure()

  # Plot data
  plt.plot(epochs, errors[0], label="Training loss")
  plt.plot(epochs, errors[1], label="Validation loss")

  # Format x and y axis
  plt.xlabel("Epoch")
  plt.ylabel("Mean "+lossType+" Error")
  errorsMin = min([errors[0].min(), errors[1].min()])
  errorsMax = max([errors[0].max(), errors[1].max()])
  plt.xlim([0, nEpochs])
  plt.ylim([errorsMin*0.95, errorsMax*1.03])
  
  # Add labels
  plt.text(0.6,0.9, "ATLAS", fontstyle='italic', fontweight='bold', transform=plt.gca().transAxes)
  plt.text(0.715,0.9, "Work In Progress", transform=plt.gca().transAxes)
  plt.text(0.6,0.84, "Regression Model {}".format(modelCode), transform=plt.gca().transAxes)

  # Add legend
  plt.legend(loc=(0.59, 0.7), frameon=False)

  # Save plot
  formats = ["png", "pdf"]
  for f in formats:
    plt.savefig("./regression/modelCode{}Loss.{}".format(modelCode,f), transparent=True, dpi=200)


#################################################
#
# Build model
#
#################################################

def main(**kwargs):
  '''
  Mandatory mode argument:
    1. mode:
        String that determines if we are going to test an existing model or create a new model
        "testing": Testing mode
        "modelling": Create new model mode
        Default: None

  If mode is testing:

    Mandatory arguments:
      1. modelCode:
          Model to test
      2. datasetCode:
          Dataset code corresponding to dataset to use for testing

  If mode is modelling:

    Mandatory model arguments:
      1. genCode:
          Dataset used for training and testing
    
    Optional model arguments are:
      1. nnType:
          Regression NN architecture to use
          "DNN": Deep neural network
          "GNN": Graph neural network
          Default: "DNN"
      2. regType:
          Regularization to use during training of the neural network
          "No": Do not use regularization at various layers of the network
          "L1": Common L1 regularization
          "L2": Common L2 regularization
          Default: "L2"
      3. lossType:
          Loss function to use for training of the neural network
          "Absolute": Mean absolute error
          "Squared": Mean squared error
          Default: "Squared"
      4. nnComplexity: 
          Neural network complexity based on number of layers and nodes
          0: 7 hidden layers with 100, 200, 300, 200, 100, 100, 100 nodes respectively
          Default: 0
      5. learningRate:
          Neural network training learning rate
          Default: 0.001
      6. nEpochs:
          How long the neural network is trained based on epochs
          Default: 10
      7. batchSize:
          How big each batch is during the neural network training
          Default: 10000
      8. validationSplit:
          What fraction of the training dataset is used for validation.
          Default: 0.3
      9. particlesCode:
          Which particles to use as inputs to the NN
          0: q1A, q2A, q3A, q4A, q1B, q2B, q3B, q4B, l1B, met
          1: q1A, q2A, q3A, q4A, q2B, l1B, met
      10. particleVarsCode:
          Code for particle variables to use as inputs to the NN.
          0: pT, eta, phi, E
          1: px, py, pz, E
          2: pT, eta, phi, px, py, pz, E
          3: pT
          Default: 0
      11. eventVarsCode:
          Code for event level variables to be used as inputs to the NN
          0: None
          1: Event shape variables - S, St, A, mTens1, mTens2, mTens3
          2: Arbitrary variable - alpha
          3: Arbitrary variable - alpha2
          Default: 0
      12. GNNTechnique:
          Technique used for feeding the GNN input to final regression stage
          "Summed": For each variable it considers the sum of every particle (particle ordering does not matter)
          "Flattened": Every variable is considered independently (particle ordering matters)
          Default: "Flattened"

    Other arguments:
      1. modellingTests:
          Boolean used for testing newly generated model
          Default: True
  '''

  # Save mode
  saveMode(**kwargs)

  # Read mode
  readMode()
  if not checkMode():
    return

  # Execute tasks depending on the mode
  if mode == "testing":
    
    # Save testing arguments
    saveTestingArgs(**kwargs)  

    # Read the testing arguments
    readTestingArgs()
    if not checkTestingArgs():
      return
 
    # Start testing log file
    startTestingLog()

    # Search testing log file for similar tests
    global testCode
    testCode, match = searchTestingLog()
  
    # Test model
    modelTester()

    # Update testing log file
    if not match: updateTestingLog()

    # End of regression testing
    print("----- END OF REGRESSION TESTING -----")
    print("Thanks for using our regression testing algorithm")
    print("Best,")
    print("Shion Chen and Luis Felipe Gutierrez")
  
  else:
    
    # Save modelling arguments
    saveModellingArgs(**kwargs)  

    # Read the modelling arguments
    readModellingArgs()
    if not checkModellingArgs():
      return
 
    # Start modelling log file
    startModellingLog()

    # Search modelling log file for similar models
    global modelCode
    modelCode, match = searchModellingLog()
    
    # Generate model
    modelBuilder()
  
    # Update modelling log file
    if not match: updateModellingLog()
  
    # End of regression NN
    print("----- END OF REGRESSION MODELLING -----")
    print("Thanks for using our regression modelling algorithm")
    print("Best,")
    print("Shion Chen and Luis Felipe Gutierrez")


if __name__=="__main__":
  main(**dict(arg.split('=') for arg in sys.argv[1:]))
