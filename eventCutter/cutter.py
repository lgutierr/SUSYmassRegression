import sys
import os
import math
import pandas as pd
import numpy as np
from matplotlib import pyplot as plt
import atlas_mpl_style as ampl
ampl.use_atlas_style()


#################################################
#
# Arguments
#
#################################################
path = ""
variables = []
thresholdUp = []
thresholdDown = []

def saveArgs(**kwargs):
  for a, v in kwargs.items():
    if a == "path":
      try:
        global path
        path = v
      except:
        pass
    elif a == "variables":
      try:
        global variables
        variables = eval(v)
      except:
        pass
    elif a == "thresholdUp":
      try:
        global thresholdUp
        thresholdUp = eval(v)
      except:
        pass
    elif a == "thresholdDown":
      try:
        global thresholdDown
        thresholdDown = eval(v)
      except:
        pass
  return

def readArgs():
  print("----- Event cutter arguments -----")
  print("path: "+path)
  print("variables: "+str(variables))
  print("thresholdUp: "+str(thresholdUp))
  print("thresholdDown: "+str(thresholdDown))
  return

def checkArgs():
  print("----- Event cutter arguments check -----")
  accepted = True
  
  # Check path argument
  if not os.path.isfile(path):
    print("Please provide valid path to dataset to apply cuts on...")
    accepted = False

  # Check variables argument
  if len(variables) == 0:
    print("Please provide valid variables to cut on...")
    accepted = False
  
  # Check thresholdUp argument
  if len(variables) != len(thresholdUp):
    print("Please provide a valid list of thresholdUp...")
    accepted = False
  
  # Check thresholdDown argument
  if len(variables) != len(thresholdDown):
    print("Please provide a valid list of thresholdDown...")
    accepted = False
  
  # Accepted values
  if accepted:
    print("Provided arguments are OK... Proceeding with next steps")
  else:
    print("Some arguments are not accepted...")
    print("Exiting program without cutting...")
  return accepted


#################################################
#
# Log
#
#################################################

def startLog():
  print("----- Start event cutter log file -----")
  
  # If log file does not exist, create one
  if not os.path.isfile("./cutter.log"):
    f = open('./cutter.log', 'w')
    f.close()
    print("Log file does not exist. Creating new log file...")
  else:
    print("Using existing log file...")

def searchLog():
  print("----- Searching event cutter log file -----")

  # Check existing entries
  f = open('./cutter.log', 'r')
  # Variables
  cutCode = -1
  pathMatch = False
  variablesMatch = False
  thresholdUpMatch = False
  thresholdDownMatch = False
  match = False
  # Read file
  lines = f.readlines()
  # Dissect lines
  for line in lines:
    code,value = line.replace(" ", "").replace("\n", "").split(":")
    # Check cutCode
    if code == "cutCode":
      # First entry
      if cutCode == -1:
        cutCode = int(value)
      # Followng entries:
      else:
        # Check if previous cutCode matches options
        match1 = pathMatch and variablesMatch
        match2 = thresholdUpMatch and thresholdDownMatch
        match = match1 and match2
        if match:
          break
        # If previous dataCode does not match options, update datasetCode and reset bools
        else:
          cutCode = int(value)
          pathMatch = False
          variablesMatch = False
          thresholdUpMatch = False
          thresholdDownMatch = False
    # Check path
    elif code == "path":
      if path == value: pathMatch = True
    # Check variables
    elif code == "variables":
      if variables == eval(value): variablesMatch = True
    # Check thresholdUp
    elif code == "thresholdUp":
      if thresholdUp == eval(value): thresholdUpMatch = True
    # Check thresholdDown
    elif code == "thresholdDown":
      if thresholdDown == eval(value): thresholdDownMatch = True
  # Close file
  f.close()
  # Update match in case it is the last entry
  match1 = pathMatch and variablesMatch
  match2 = thresholdUpMatch and thresholdDownMatch
  match = match1 and match2
  # Check the matching cutCode, otherwise create a new one
  if cutCode == -1:
    # Create new entry
    cutCode = 0
    match = False
  else:
    # If it does not match, generate a new mergeCode
    if not match: cutCode += 1

  # Logging
  if match:
    print("Found cuts with exact same conditions...")
    print("Using cutCode: "+str(cutCode))
  else:
    print("No test with the same conditions found...")
    print("New cutCode: "+str(cutCode))

  # Return output
  return cutCode, match

def updateLog():
  print("----- Update event cutter log file -----")
  f = open("./cutter.log", "a")
  f.write("cutCode: "+str(cutCode)+"\n")
  f.write("  path: "+str(path)+"\n")
  f.write("  variables: "+str(variables)+"\n")
  f.write("  thresholdUp: "+str(thresholdUp)+"\n")
  f.write("  thresholdDown: "+str(thresholdDown)+"\n")
  f.close()
  print("Added entry for cutCode="+str(cutCode))


#################################################
#
# Event cutter
#
#################################################

def cutter():
  print("----- Cutting events -----")

  # Obtain dataframe
  print("Obtaining dataframe...")
  df = pd.read_csv(path)

  # Filter dataframe
  print("Filtering dataframe...")
  filtDF = None
  for i in range(len(variables)):
    var = variables[i]
    thUp = thresholdUp[i]
    thDown = thresholdDown[i]
    if type(filtDF) == type(None):
      filtDF = df[(df[var] > thUp) & (df[var] < thDown)]
    else:
      filtDF = filtDF[(filtDF[var] > thUp) & (filtDF[var] < thDown)]

  # Create output directory
  currentDirs = os.listdir("./")
  if "datasets" not in currentDirs:
    os.system("mkdir datasets")
  
  # Save filtered dataframes
  print("Saving filtered dataframe...")
  filtDF.to_csv("./datasets/cutCode{}.csv".format(cutCode), index=False)

  # End of function
  return


#################################################
#
# Cut events
#
#################################################
cutCode = -1

def main(**kwargs):
  '''
  Mandatory arguments:
    1. path:
      Path to dataset to apply cuts on.
    2. variables:
      List of variables to apply a cut on.
    3. thresholdUp:
      List with the same dimension as variables with the low cut to apply.
    4. thresholdDown:
      List with the same dimension as variable with the high cut to apply.
  '''

  # Save arguments
  saveArgs(**kwargs)

  # Read arguments
  readArgs()
  if not checkArgs():
    return

  # Start log file
  startLog()

  # Search log file for similar mergers
  global cutCode
  cutCode, match = searchLog()

  # Extract events
  cutter()

  # Update log file
  if not match: updateLog()

  # End of event merger
  print("----- END OF EVENT CUTTER -----")
  print("Thanks for using our event cutter algorithm")
  print("Best,")
  print("Shion Chen and Luis Felipe Gutierrez")

if __name__=="__main__":
  main(**dict(arg.split("=") for arg in sys.argv[1:]))
