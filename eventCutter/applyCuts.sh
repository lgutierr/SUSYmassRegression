python3 cutter.py\
  path="/Users/felipegutierrez/Documents/CERN/ATLAS/work/SUSY/massRegression/software/models/datasets/classificationModelCode0Test.csv"\
  variables=[\"NNScore\"]\
  thresholdUp=[0.9]\
  thresholdDown=[1.01]\
  plots=[\"NNScoreMassDependency\"]

python3 cutter.py\
  path="/Users/felipegutierrez/Documents/CERN/ATLAS/work/SUSY/massRegression/software/models/datasets/classificationModelCode1Test.csv"\
  variables=[\"NNScore\"]\
  thresholdUp=[0.9]\
  thresholdDown=[1.01]\
  plots=[\"NNScoreMassDependency\"]
