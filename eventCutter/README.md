# Event cutter

Package that applies cuts on a dataset.

## Cutting on datasets

#### Directly running programm
```
python3 cutter.py path="PATH/TO/EXISTING/DATASET" variables=["NNScore",...] thresholdUp=["0.99",...] thresholdDown=["1.01",...]
```
See below for list of arguments.

#### Running shell script
```
./applyCuts.sh
```

#### Arguments

##### Mandatory arguments
```path```
* Path to dataset to apply cuts on.

```variables```
* List of variables to apply a cut on.

```thresholdUp```
* List with the same dimension as variables with the low cut to apply.

```thresholdDown```
* List with the same dimension as variable with the high cut to apply.


## WARNINGs

* To avoid cuts code confusion, do not delete the ```.log``` file which keeps track of the cuts and filtered datasets created.
